import json
from collections import OrderedDict
from functools import partial

from django.dispatch import receiver
from django.db.models import signals

from front.models import Deployment as FrontDeployment
from .models import Deployment


_is_open_boundary = lambda type_: type_.lower() == 'open'

@receiver(signals.post_save, sender=FrontDeployment)
def fill_deployment_config2(sender, instance, **kwargs):
    if not instance.active:
        return

    try:
        x_min, y_min, x_max, y_max = instance.step2['extent']
    except ValueError:
        x_min, y_min, x_max, y_max = instance.step2['extent'][1]

    x_min_, x_max_ = max(-180., x_min), min(180., x_max)
    y_min_, y_max_ = max(-90., y_min), min(90., y_max)

    # boundaries
    boundaries_geojson = json.loads(instance.step2['boundaries']['geojson'])

    open_boundaries_lengths = dict()
    for feature in boundaries_geojson['features']:

        feature_properties = feature['properties']

        if not _is_open_boundary(feature_properties['type']):
            continue

        boundary_code = feature_properties['code']
        boundary_length = len(feature['geometry']['coordinates'])

        open_boundaries_lengths[boundary_code] = boundary_length

    # forcings
    def boundary_forcings_item(bnd):
        name = bnd['bid']

        return name, dict(
            kind = bnd['ftype'],
            forcings = bnd['fmodel'],
            node_count = open_boundaries_lengths[name],
        )

    boundaries_forcings = instance.step3.get('forcings') or tuple()

    boundaries = OrderedDict( map(boundary_forcings_item, boundaries_forcings) )

    param_in = instance.step5.get('params') or dict()

    vgrid_info = instance.step2.get('vgrid_info')
    vertical_levels_dict = lambda: dict(
        s_levels = vgrid_info['s_levels'],
        z_levels = vgrid_info['z_levels'],
    )

    try:
        wwminput_nml = instance.step5['wwm']['params']
    except KeyError:
        wwminput_nml = dict()

    config = dict(
        run_type = instance.step1.get('run_type'),
        period_hours = instance.model_version_period.run_period,
        timestep_seconds = int(param_in['dt']),

        bbox = ( (x_min_, y_min_), (x_max_, y_max_) ),
        boundaries = boundaries,
        boundaries_order = tuple( boundaries.keys() ),
        boundaries_geojson = boundaries_geojson,
        vertical_levels = vertical_levels_dict() if vgrid_info else dict(),

        ocean_elev_forcing = instance.step3.get('oceanmodel'),
        ocean_salt_temp_forcing = instance.step3.get('oceanmodel_temp_salt'),
        ocean_waves_forcing = instance.step3.get('oceanmodel_waves'),
        atmospheric_forcing = instance.step3.get('atmmodel'),

        param_in = param_in,
        wwminput_nml = wwminput_nml,
    )

    updating_fields = dict(
        config = config,
    )
    Deployment.objects.update_or_create(parent=instance,
                                                       defaults=updating_fields)
