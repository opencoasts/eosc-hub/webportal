import operator
from datetime import datetime, timedelta
from collections import abc
from django.utils.translation import gettext as _, override
from functools import lru_cache, partial
from itertools import chain
from pathlib import Path
from pprint import pformat
from urllib.request import urlopen

from datamodel.models import Deployment, DeploymentRun, ForcingSource, \
                                                     DeploymentRunOutputFilepath
from front.utils import notify_users as notify_users_by_email
from wizard.utils import get_auxfile_bk as get_deployment_auxfile_text
from .ncwms import ncWMS

from simulation.core.entities import Step, Layer
from simulation.core.mixins.file_handler import FileHandler
from simulation.schism.common import ForcingSeriesKind
from simulation.schism.steps.base import SCHISMStep


class OPENCoastSMappings:

    from simulation.schism.common import SimulationKind, ConfigurationKind, \
                                        BoundaryKind, ForcingKind, ForcingSource

    simulation_kind_map = dict(
        basic2d = SimulationKind.BAROTROPIC,
        basic3d = SimulationKind.BAROCLINIC,
        waves2d = SimulationKind.BAROTROPIC_WAVES,
    )

    file_type_map = dict(
        albedo = 'albedo.gr3',
        diffmin = 'diffmin.gr3',
        diffmax = 'diffmax.gr3',
        drag = 'drag.gr3',
        hgrid_gr3 = 'hgrid.gr3',
        hgrid_ll = 'hgrid.ll',
        manning = 'manning.gr3',
        salinity = 'salt.ic',
        temperature = 'temp.ic',
        vgrid_in = 'vgrid.in',
        watertype = 'watertype.gr3',
        windrot = 'windrot_geo2proj.gr3',
    )
    simulation_kind_auxfiles_map = {
        SimulationKind.BAROTROPIC: ('manning', 'windrot'),
        SimulationKind.BAROTROPIC_WAVES: ('manning', 'windrot'),
        SimulationKind.BAROCLINIC: ('albedo', 'diffmin', 'diffmax', 'drag',
                             'salinity', 'temperature', 'watertype', 'windrot'),
    }

    configuration_defaults_map = {
        ConfigurationKind.PARAM_IN: {
            SimulationKind.BAROTROPIC: dict(
                dt = 90.,
                hvis_coef0 = .025,
            ),
            SimulationKind.BAROTROPIC_WAVES: dict(
                dt = 30.,
                nstep_wwm = 10,
            ),
            SimulationKind.BAROCLINIC: dict(
                dt = 30.,
                indvel = 0,
                ihorcon = 0,
                hvis_coef0 = .02,
                ishapiro = 1,
                shapiro = .5,
                h0 = .01,
                thetai = .6,
                nstep_wwm = 0,
            ),
        },

        ConfigurationKind.WWMINPUT_NML: {
            SimulationKind.BAROTROPIC_WAVES: dict(
                PROC_DELTC = 300.,
                ENGS_BRHD = .78,
            ),
        }
    }

    boundary_kind_map = dict(
        ocean = BoundaryKind.OCEAN,
        river = BoundaryKind.RIVER,
    )

    forcing_kind_map = dict(
        elevation = ForcingKind.ELEVATION,
        circulation = ForcingKind.ELEVATION,
        temperature = ForcingKind.TEMPERATURE,
        salinity = ForcingKind.SALINITY,
        flow = ForcingKind.FLOW,
    )

    ocean_elev_forcings_map = {
        'prism2017': ForcingSource.PRISM_2017,
        'fes2014': ForcingSource.FES_2014,
        'cmems-global': ForcingSource.CMEMS_GLOBAL,
        'cmems-ibi': ForcingSource.CMEMS_IBI,
    }
    ocean_salt_temp_forcings_map = {
        'cmems-global': ForcingSource.CMEMS_GLOBAL,
        'cmems-ibi': ForcingSource.CMEMS_IBI,
    }
    atmospheric_forcings_map = {
        'noaa-gfs': ForcingSource.NOAA_GFS_0P25,
        'meteofr-arpege_ea': ForcingSource.METEOFR_ARGPEGE_EA,
        'noforcing': ForcingSource.NO_FORCING,
    }


class DjangoDeployment:

    deployment_id = Step.Input()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.deployment = Deployment.objects.get(id=self.deployment_id)
        self.deployment_front = self.deployment.deployment
        self.deployment_wiff = self.deployment.wiff

    def _publish_deployment_run_files(self, date_time, files, kind):
        try:
            deployment_run = self.deployment.deploymentrun_set.get(
                run_datetime = date_time,
            )
        except DeploymentRun.DoesNotExist:
            self.logger.info('DeploymentRun record not found: '
                                                  "file(s) won't be published!")
            return False

        for file in files:
            entry = DeploymentRunOutputFilepath.objects.create(
                deployment_run = deployment_run,
                kind = kind,
                filepath = file,
            )
            self.logger.info('%s file published!', entry)

        return True

    def _publish_files(self):

        def publish_if_exists(file_pattern, kind):
            files = self.path.glob(file_pattern)

            if files:
                self._publish_deployment_run_files(self.date_time, files, kind)
            else:
                self.logger.info('No file(s) in %s!', file_pattern)

        for file in self.files_to_publish:
            publish_if_exists(*file)

    email_templates_dirpath = Path('wiff/emails')

    def _notify_by_email(self, addresses, subject, run_date, template_filename):
        # addresses += ('jrogeiro@lnec.pt',) # testing purposes
        context = dict(
            deployment = self.deployment,
            run_date = run_date,
        )
        templates_filepaths_ = lambda filename: (
            self.email_templates_dirpath / f'{filename}.txt',
            self.email_templates_dirpath / f'{filename}.html',
        )

        notify_users_by_email( addresses, subject, context,
                                      *templates_filepaths_(template_filename) )

        self.logger.info('Email sent: addresses=%s subject=%s template=%s',
                                    addresses, repr(subject), template_filename)

    def _notify_author_by_email(self, subject, run_date, template_filename,
                                                          extra_addresses=None):
        extra_addresses_ = extra_addresses or tuple()
        addresses = (self.deployment_front.user.email, *extra_addresses_)

        with override('en'):
            self._notify_by_email(addresses, _(subject), run_date,
                                                              template_filename)


class Announce(Layer, DjangoDeployment):

    path = Step.Input()
    date_time = Step.Input()
    publish = Step.Input(default=True)
    notification = Step.Input(default=True)
    begin_timestamp = Step.Input(datetime.now)
    admin_emails = Step.Input(default=tuple)

    _steps_failed = Step.Input(optional=True)

    def execute(self):
        self._create_or_update_run_record()

    def _create_or_update_run_record(self):
        record, created = DeploymentRun.objects.update_or_create(
            deployment = self.deployment,
            run_datetime = self.date_time,

            defaults = dict(
                state = DeploymentRun.State.STARTED,
                begin = self.begin_timestamp,
                end = None,
            )
        )

        action = 'Created' if created else 'Updated'
        self.logger.info('%s DeploymentRun record #%i.', action, record.id)

    def finalize(self):
        self._finalize_run_record()

        if self.publish:
            self._publish_files()

        if self._steps_failed and self.notifications:
            self._notify_admin_by_email('Falha na execução do sistema',
                                    self.date_time.date(), 'deployment_failure')

    files_to_publish = (
        ('logs.tar.xz', DeploymentRunOutputFilepath.Kind.LOGS),
        ('confs.tar.xz', DeploymentRunOutputFilepath.Kind.CONFS),
        ('forcings.tar.xz', DeploymentRunOutputFilepath.Kind.INPUTS),
    )

    def _finalize_run_record(self):
        try:
            deployment_run = DeploymentRun.objects.get(
                deployment = self.deployment,
                run_datetime = self.date_time,
            )
        except DeploymentRun.DoesNotExist:
            self.logger.info('DeploymentRun record not found!')
            return

        run_state = DeploymentRun.State.FAILED if self._steps_failed \
                                               else DeploymentRun.State.FINISHED

        deployment_run.state = run_state
        deployment_run.end = datetime.now()
        deployment_run.save()

        self.logger.info('Updated DeploymentRun record #%i.', deployment_run.id)

    def _notify_admin_by_email(self, subject, run_date, template_filename,
                                                          extra_addresses=None):
        extra_addresses_ = extra_addresses or tuple()
        addresses = tuple(self.admin_emails + extra_addresses_)

        self._notify_by_email(addresses, subject, run_date, template_filename)


from shutil import rmtree


class Prepare(SCHISMStep, OPENCoastSMappings, DjangoDeployment):

    series_root = SCHISMStep.Input(optional=True)
    begin_timestamp = SCHISMStep.Input(datetime.now)

    simulation_kind = SCHISMStep.Output()
    param_in = SCHISMStep.Output()
    wwminput_nml = SCHISMStep.Output()
    vertical_levels_count = SCHISMStep.Output()
    boundaries = SCHISMStep.Output()
    forcings = SCHISMStep.Output()
    bbox = SCHISMStep.Output()
    is_cartesian = SCHISMStep.Output()

    _lru_cache_size = 8

    def execute(self):
        self._resolve_simulation_kind()
        self._resolve_configuration_files()
        self._resolve_vertical_levels()
        self._resolve_boundaries()
        self._resolve_forcings()
        self._resolve_wave_boundaries()
        self._resolve_static_files()

    default_simulation_kind = OPENCoastSMappings.SimulationKind.BAROTROPIC,

    def _resolve_simulation_kind(self):
        run_type = self.deployment_wiff.config.get('run_type')

        if run_type:
            simulation_kind = self.simulation_kind_map[run_type]
            self.logger.info( 'simulation kind: %r', simulation_kind.upper() )
        else:
            simulation_kind = self.default_simulation_kind
            self.logger.warning('simulation kind not set, defaults to: %r',
                                                                simulation_kind)

        self.simulation_kind = simulation_kind

    def _resolve_configuration_files(self):

        def coarse_values_type(defined, defaults):
            coarsed = dict()
            for key, value in defined.items():
                default_value = defaults.get(key)
                default_type = type(default_value)
                coarsed[key] = \
                         value if default_value is None else default_type(value)

            return coarsed

        def get_configuration_for_(conf_kind):
            kinds_map = self.configuration_defaults_map.get(conf_kind) or dict()
            kind_defaults = kinds_map.get(self.simulation_kind) or dict()

            deploy_config = self.deployment_wiff.config.get(conf_kind) or dict()
            coarsed_deploy_config = coarse_values_type(deploy_config,
                                                                  kind_defaults)

            return dict(kind_defaults, **coarsed_deploy_config)

        self.param_in = get_configuration_for_('param_in')
        self.timestep = timedelta( seconds=int(self.param_in['dt']) )
        self.wwminput_nml = get_configuration_for_('wwminput_nml')

    def _resolve_vertical_levels(self):
        vertical_levels = self.deployment_wiff.config.get('vertical_levels')
        if vertical_levels:
            s_levels = vertical_levels['s_levels']
            z_levels = vertical_levels['z_levels']

            self.vertical_levels_count = len(s_levels) + len(z_levels) - 1

    def _resolve_boundaries(self):
        config = self.deployment_wiff.config
        date_time = self.date_time

        simulation_kind = self.simulation_kind
        config_boundaries = config['boundaries']

        boundary_kind_ = lambda cfg_bnd_kind: \
                                            self.boundary_kind_map[cfg_bnd_kind]
        river_boundaries__forcings = {
            name: self._handle_config_river_forcings(cfg_bnd['forcings'])
                for name, cfg_bnd in config_boundaries.items()
                   if boundary_kind_(cfg_bnd['kind']) == self.BoundaryKind.RIVER
        }
        self.logger.debug("river boundary forcings map:\n%s",
                                           pformat(river_boundaries__forcings) )

        resolved_boundaries = list()
        ordered_config_boundaries = ( (bnd_name, config_boundaries[bnd_name])
                                    for bnd_name in config['boundaries_order'] )
        for bnd_name, cfg_bnd in ordered_config_boundaries:

            boundary_kind = boundary_kind_(cfg_bnd['kind'])

            boundary_details = dict(
                kind = boundary_kind,
                node_count = cfg_bnd['node_count'],
            )

            if boundary_kind == self.BoundaryKind.RIVER:

                forcing_kinds = self.SimulationKind.FORCINGS_MAP[
                                                 simulation_kind][boundary_kind]

                for forcing_kind in forcing_kinds:

                    forcing_kind_details_ = lambda bnd_name: \
                              river_boundaries__forcings[bnd_name][forcing_kind]
                    forcing_kind_details = forcing_kind_details_(bnd_name)

                    forcing_kind_ratio = forcing_kind_details.get('ratio') or 1.

                    forcing_kind_reference = forcing_kind_details.get(
                                                                    'reference')
                    if forcing_kind_reference:
                        forcing_kind_details = forcing_kind_details_(
                                                         forcing_kind_reference)

                    boundary_details[forcing_kind] = \
                        self._handle_river_forcing_series_kind(
                            forcing_kind_details, date_time, forcing_kind_ratio)

            resolved_boundaries.append(boundary_details)

        self.boundaries = resolved_boundaries
        self.logger.info("the following boundaries are defined:\n%s",
                                                  pformat(resolved_boundaries) )

    def _handle_config_river_forcings(self, forcings):

        forcing_spec = lambda kind, **kwargs: dict(kind=kind, **kwargs)

        opposite_series = lambda series: tuple(
                                        map(partial(operator.mul, -1), series) )

        # flow_forcing_spec = lambda flow_series: {
        #     ForcingKind.FLOW: forcing_spec( ForcingSeriesKind.ANNUAL,
        #                                  series=opposite_series(flow_series) )
        #     }

        # forcings_are_only_flow = not isinstance(forcings, abc.Mapping)
        # if forcings_are_only_flow:
        #     return {
        #         self.ForcingKind.FLOW:
        #             forcing_spec(self.ForcingSeriesKind.ANNUAL,
        #                                       series=opposite_series(forcings) )
        #     }

        is_flow_series = lambda fkind: fkind == self.ForcingKind.FLOW

        monthly_value_series = lambda value, fkind: forcing_spec(
            ForcingSeriesKind.ANNUAL,
            series = opposite_series(value) if is_flow_series(fkind) else value,
        )

        annual_value_series = lambda value, fkind: monthly_value_series(
                                                              [value]*12, fkind)

        url_series = lambda value, _: forcing_spec(
                           ForcingSeriesKind.TH_SERVICE_URL, url=value.strip() )

        percent_series = lambda value, _: forcing_spec(ForcingSeriesKind.RATIO,
                             ratio=value['percent']/100, reference=value['bid'])

        series_type_handlers_map = dict(
            monthly = monthly_value_series,
            annual = annual_value_series,
            url = url_series,
            percent = percent_series,
        )
        series_type_handler = lambda details, fkind: \
            series_type_handlers_map[ details['type'] ](details['value'], fkind)

        fkind_map = self.forcing_kind_map
        return {
            fkind_map[fkind]: series_type_handler(details, fkind_map[fkind])
                for fkind, details in forcings.items()
        }

    def _handle_river_forcing_series_kind(self, details, date_time, ratio):
        series_kind_handlers_map = {
            ForcingSeriesKind.ANNUAL: lambda: self._monthly_value(
                               yearly_series=details['series'], date=date_time),
            ForcingSeriesKind.TH_SERVICE_URL: lambda: self._th_service_value(
                                    url_service=details['url'], date=date_time),
        }
        forcing_value = series_kind_handlers_map[ details['kind'] ]

        return forcing_value() * ratio

    # develop further to use at least the simulation period dates, instead
    # of using only the beginning period date
    @staticmethod
    def _monthly_value(yearly_series, date):
        flux_month_index = date.month - 1
        return yearly_series[flux_month_index]

    @lru_cache(maxsize=_lru_cache_size)
    def _th_service_value(self, url_service, date):
        dated_url_service = date.strftime(url_service)

        self.logger.debug('trying to fetch forcing from %r', dated_url_service)
        bytes_data = urlopen(dated_url_service).read()
        series_lines = bytes_data.decode().strip().splitlines()

        step_pair = lambda ts, v: ( int(ts), -float(v) )
        timestamps, values = zip( *( step_pair( *line.strip().split() )
                                                    for line in series_lines ) )

        return sum(values) / len(values)

    def _resolve_forcings(self):
        map_forcing_ = lambda mapping, forcing: \
                                 mapping[ self.deployment_wiff.config[forcing] ]

        forcings = dict(
            ocean_elev = map_forcing_(self.ocean_elev_forcings_map,
                                                          'ocean_elev_forcing'),
            atmospheric = map_forcing_(self.atmospheric_forcings_map,
                                                         'atmospheric_forcing'),
        )

        if self.simulation_kind == self.SimulationKind.BAROCLINIC:
            forcings['ocean_salt_temp'] = map_forcing_(
                   self.ocean_salt_temp_forcings_map, 'ocean_salt_temp_forcing')

        self.forcings = forcings

        deployment_bbox = self.deployment_wiff.config.get('bbox')
        if deployment_bbox:
            (x_min, y_min), (x_max, y_max) = deployment_bbox
            self.bbox = dict(
                lon = (x_min, x_max),
                lat = (y_min, y_max),
            )

    wave_simulation_kinds = (
        OPENCoastSMappings.SimulationKind.BAROTROPIC_WAVES,
    )

    def _resolve_wave_boundaries(self):
        if self.simulation_kind not in self.wave_simulation_kinds:
            return

        boundaries = self.deployment_wiff.config['boundaries']
        has_boundary_forcing = lambda details: ForcingSource.objects.filter(
                   name=details['forcings']['waves']['value'].strip() ).exists()
        is_wave_boundary = lambda details: 'waves' in details['forcings'] and \
                                                   has_boundary_forcing(details)

        wave_boundary_names = tuple( name
            for name, details in boundaries.items() if is_wave_boundary(details)
        )
        self.logger.debug("wave boundaries: %s", wave_boundary_names)

        index_from_ = lambda name: int( name.split('-')[1] )
        wave_boundary_indexes = tuple( map(index_from_, wave_boundary_names) )

        boundaries_features = \
                   self.deployment_wiff.config['boundaries_geojson']['features']
        points_from_ = lambda feature: feature['geometry']['coordinates']
        is_wave_boundary_ = lambda feature: \
                            feature['properties']['code'] in wave_boundary_names

        wave_boundaries_points = tuple( points_from_(feature)
              for feature in boundaries_features if is_wave_boundary_(feature) )

        self.logger.debug("wave boundaries points: %s", wave_boundaries_points)

        wave_forcings = dict(
            wave_boundaries = wave_boundary_indexes,
            wave_boundary_points =
                           tuple( chain.from_iterable(wave_boundaries_points) ),
        )
        self.forcings.update(wave_forcings)

    def _resolve_static_files(self):
        if self.series_root:
            series_static_path = self.series_root / 'static'

            if not series_static_path.exists():
                series_static_path.mkdir(parents=True)
                try:
                    self._download_static_files(series_static_path)
                except: # clean remaning static folder
                    rmtree(series_static_path, ignore_errors=True)
                    raise

            for static_file_path in series_static_path.iterdir():
                run_static_file_path = self.path / static_file_path.name
                self.link_file(static_file_path, run_static_file_path)

        else:
            self._download_static_files(self.path)

        hgrid_gr3_path = self.path / 'hgrid.gr3'
        hgrid_ll_path = hgrid_gr3_path.with_suffix('.ll')
        hgrid_cartesian = False
        try:
            self.link_file(hgrid_ll_path, hgrid_gr3_path)
        except FileExistsError:
            hgrid_cartesian = True
            self.logger.info('using CARTESIAN coordinate system!')

        self.is_cartesian = hgrid_cartesian

    def _download_static_files(self, path):
        self._download_deployment_files(path)
        self._download_deployment_auxfiles(path)

    def _download_deployment_files(self, path):

        file_type_entries = lambda name: self.deployment.files.filter(
                                                           file_type__name=name)

        for entry in file_type_entries('hgrid') | file_type_entries('vgrid'):

            entry_type_name = f'{entry.file_type.name}_{entry.file_type.format}'
            entry_path = path / self.file_type_map[entry_type_name]

            self.logger.debug("downloading %r file as %s", entry_type_name,
                                                                     entry_path)
            entry_path.write_bytes(entry.file)
            self.logger.info("downloaded %r file as %s", entry_type_name,
                                                                     entry_path)

    def _download_deployment_auxfiles(self, path):

        for auxfile in self.simulation_kind_auxfiles_map[self.simulation_kind]:

            auxfile_path = path / self.file_type_map[auxfile]
            self.logger.debug("downloading %r file as %s", auxfile,
                                                                   auxfile_path)

            auxfile_text = get_deployment_auxfile_text(self.deployment.id,
                                                                        auxfile)
            auxfile_path.write_text(auxfile_text)
            self.logger.info("downloaded %r file as %s", auxfile, auxfile_path)


class Update(Step, FileHandler, DjangoDeployment):

    path = Step.Input()
    date_time = Step.Input()
    publish_outputs = Step.Input(default=True)
#    sixties_paths = Step.Input(default=tuple)
#    netcdf_paths = Step.Input(default=tuple)
#    netcdf_ugrid_paths = Step.Input(default=tuple)
    ncwms_name = Step.Input(optional=True)
    ncwms_deployment_location = Step.Input(optional=True)
    hotstart_path = Step.Input(optional=True) # workaround
    notifications = Step.Input(default=True)

    def execute(self):
        if self.publish_outputs:
            self._publish_files()
            self._update_ncwms_dataset()

        if self.notifications:
            self._notify_user()

        self._update_last_run()

    files_to_publish = (
        ('outputs/?_*.6?*', DeploymentRunOutputFilepath.Kind.RESULTS),
        ('outputs/?_all-ugrid.nc', DeploymentRunOutputFilepath.Kind.RESULTS),
        ('outputs/*_hotstart.in*', DeploymentRunOutputFilepath.Kind.INPUTS),
        ('outputs/wwm_hotstart.nc*', DeploymentRunOutputFilepath.Kind.INPUTS),
    )

    ncwms_folder = '_ncwms'
    ncwms_nc_suffix = '_all-ugrid.nc'

    def _update_ncwms_dataset(self):
        if not self.ncwms_name:
            self.logger.info('no ncWMS server defined!')
            return

        ncwms_server = ncWMS.servers.get(self.ncwms_name)
        if not ncwms_server:
            self.logger.warning('invalid ncWMS server name %r!',
                                                                self.ncwms_name)
            return

        self.logger.info('ncWMS server name %r', self.ncwms_name)

        self._build_ncwms_dataset_folder()

        ncwms_nc_glob = f'{self.ncwms_folder}/*{self.ncwms_nc_suffix}'
        ncwms_nc_paths = tuple( self.path.glob(ncwms_nc_glob) )
        if not ncwms_nc_paths:
            self.logger.info('no netcdf ugrid path(s) exposed!')
            return

        self.logger.info('netcdf ugrid paths: %s', ncwms_nc_paths)

        logger_msg = lambda action: ( f'%s {action} @ %s', self.deployment,
                                                               self.ncwms_name )

        # TODO: if location has changed it should be replaced instead of updated
        refresh = ncwms_server.deployment_update(self.deployment)
        if refresh:
            self.logger.info( *logger_msg('refreshed') )

        else:
            if not self.ncwms_deployment_location:
                self.logger.info('no ncWMS deployment location set!')
                return

            ncwms_location = f'{self.ncwms_deployment_location}/{ncwms_nc_glob}'
            add = ncwms_server.deployment_add(self.deployment, ncwms_location)
            if add:
                self.logger.info( *logger_msg('added') )
            else:
                self.logger.info( *logger_msg('not added') )

    ncwms_previous_nc_prefix = 1
    ncwms_current_nc_prefixes = (1, 2)

    def _build_ncwms_dataset_folder(self):
        ncwms_path = self.create_folder(self.ncwms_folder)

        dataset_nc_paths = list()
        if self.hotstart_path:
            previous_outputs_path = self.hotstart_path.parent
            previous_nc_name = \
                        f'{self.ncwms_previous_nc_prefix}{self.ncwms_nc_suffix}'
            dataset_nc_paths.append(previous_outputs_path / previous_nc_name)

        for current_nc_prefix in self.ncwms_current_nc_prefixes:
            current_nc_name = f'{current_nc_prefix}{self.ncwms_nc_suffix}'
            dataset_nc_paths.append(Path('outputs') / current_nc_name)

        for nc_link_prefix, nc_path in enumerate(dataset_nc_paths):
            nc_link = ncwms_path / f'{nc_link_prefix}{self.ncwms_nc_suffix}'
            if nc_path.exists():
                self.link_file(nc_path, nc_link)

    def _notify_user(self):
        run_date = self.date_time.date()
        end_date = self.deployment.end_date

        is_first_run = not self.deployment.last_run_date
        is_expiring = (end_date - run_date) == timedelta(days=7)
        is_expired = run_date == end_date

        if is_first_run:
            subject = 'Início de novo sistema'
            self._notify_author_by_email(subject, run_date,
                                                           'deployment_1st_run')

        elif is_expiring:
            subject = 'Sistema quase a expirar'
            self._notify_author_by_email(subject, run_date,
                                                          'deployment_expiring')

        elif is_expired:
            subject = 'Sistema expirou'
            self._notify_author_by_email(subject, run_date,
                                                           'deployment_expired')

        else:
            return

        self.logger.info("Deployment's author notified.")

    def _update_last_run(self):
        run_date = self.date_time.date()

        self.deployment.last_run_date = run_date
        self.deployment.save(update_fields=['last_run_date'])
        self.logger.info("%s's last run date updated to: %s",
                                                      self.deployment, run_date)


class Finalize(Step, DjangoDeployment):

    path = Step.Input()
    date_time = Step.Input()
    publish = Step.Input(default=True)
    notification = Step.Input(default=True)

    _steps_failed = Step.Input(optional=True)

    def execute(self):
        self._finalize_run_record()

        if self.publish:
            self._publish_files()

        if self._steps_failed and self.notifications:
            self._notify_admin_by_email('Falha na execução do sistema',
                                    self.date_time.date(), 'deployment_failure')

    files_to_publish = (
        ('logs.tar.xz', DeploymentRunOutputFilepath.Kind.LOGS),
        ('confs.tar.xz', DeploymentRunOutputFilepath.Kind.CONFS),
        ('forcings.tar.xz', DeploymentRunOutputFilepath.Kind.INPUTS),
    )

    _publish_files = Update._publish_files

    def _finalize_run_record(self):
        try:
            deployment_run = DeploymentRun.objects.get(
                deployment = self.deployment,
                run_datetime = self.date_time,
            )
        except DeploymentRun.DoesNotExist:
            self.logger.info('DeploymentRun record not found!')
            return

        run_state = DeploymentRun.State.FAILED if self._steps_failed \
                                               else DeploymentRun.State.FINISHED

        deployment_run.state = run_state
        deployment_run.end = datetime.now()
        deployment_run.save()

        self.logger.info('Updated DeploymentRun record #%i.', deployment_run.id)
