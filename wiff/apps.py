from django.apps import AppConfig


class WiffConfig(AppConfig):
    name = 'wiff'
    verbose_name = 'WIFF'

    def ready(self):
        from . import signals