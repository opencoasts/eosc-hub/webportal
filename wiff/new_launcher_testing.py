import argparse
import logging
import pickle
from datetime import datetime, date, time, timedelta
from django.conf import settings
from functools import partial
from pathlib import Path
from time import sleep

if __name__ == '__main__':
    import django
    django.setup()

from datamodel.models import Deployment
from wiff.new_launcher import SCHISM, logger, LOGGING_FMT, lookup_setting, \
                                                             lookup_setting_list

logger = logging.getLogger(__name__)

def setup_argparser():
    cmd = argparse.ArgumentParser(
        description = 'OPENCoastS SCHISM testing launcher'
    )

    cmd.add_argument('simulation_kind',
        choices = ('ww3', 'schism'),
        help = "kind of simulation",
    )

    cmd.add_argument('-i', '--interactive',
        action = 'store_true',
        dest = 'interactive',
        help = "ask for confirmation before execute each step",
    )

    cmd.add_argument('-v', '--verbose',
        action = 'store_true',
        help='print everything to terminal',
    )

    cmd_mode = cmd.add_subparsers()

    cmd_new = cmd_mode.add_parser('new',
        help = 'new simulation',
    )

    cmd_new.add_argument('deployment_id',
        help = "deployment ID",
    )

    cmd_new.add_argument('-p', '--path',
        dest = 'root_path',
        metavar = 'PATH',
        type = Path,
        default = Path.cwd(),
        help = "simulation root path",
    )

    cmd_new.add_argument('-o', '--offset',
        dest = 'offset_days',
        metavar = 'DAYS',
        type = int,
        default = 0,
        help = "run date offset (days: positive=past, negative=future)",
    )

    cmd_new_compute = cmd_new.add_mutually_exclusive_group()

    cmd_new_compute.add_argument('-m', '--mpi_procs',
        metavar = 'COUNT',
        type = int,
        default = 0,
        nargs = '?',
        help = "run locally using MPI, process count may be set",
    )

    cmd_new_compute.add_argument('-s', '--sproxy',
        action = 'store_true',
        help = "use sproxy to offload the simulation run",
    )

    cmd_resume = cmd_mode.add_parser('resume',
        help = 'resume simulation',
    )

    cmd_resume.add_argument('context',
        metavar = 'FILE',
        help = "initial context to use, usually a _state.pickle file",
    )

    return cmd

if __name__ == '__main__':
    cmd_args = setup_argparser().parse_args()

    log_lvl = 'DEBUG' if cmd_args.verbose else 'INFO'
    logging.basicConfig(level=log_lvl, format=LOGGING_FMT)

    logger.debug('cmd_args: %s', cmd_args)

    root_path = Path(cmd_args.root_path).resolve()
    dir_path_ = lambda dir: root_path / dir

    deployment = Deployment.objects.get(id=cmd_args.deployment_id)
    run_datetime = ( datetime.combine( date.today(), time(0) ) -
                                          timedelta(days=cmd_args.offset_days) )
    deployments_path = Path( lookup_setting('DEPLOYMENTS_PATH') or
                                                       dir_path_('deployment') )

    with_sproxy = partial(
        SCHISM.with_sproxy_compute,
        url = lookup_setting('SPROXY_URL'),
        job_service = lookup_setting('SPROXY_JOB_SERVICE'),
        blob_service = lookup_setting('SPROXY_BLOB_SERVICE'),
        label_prefix = lookup_setting('SPROXY_LABEL_PREFIX'),
    )
    with_local_mpi = partial(
        SCHISM.with_local_mpi_compute,
        mpirun_args = lookup_setting_list('MPIRUN_ARGS'),
    )

    launcher = with_sproxy if cmd_args.sproxy else with_local_mpi

    template = launcher(
        simulation_name = 'schism-testing',

        root_path = deployments_path,
        bin_path = Path( lookup_setting('SCHISM_BIN_PATH') ),
        static_path = Path( lookup_setting('SCHISM_STATIC_PATH') ),
        ww3_bin_path = Path( lookup_setting('WW3_BIN_PATH') ),
        ww3_static_path = Path( lookup_setting('WW3_STATIC_PATH') ),

        fes2014_exec = lookup_setting('FES2014_PATH'),
        motuclient_exec = lookup_setting('MOTUCLIENT_PATH'),
        motuclient_config = lookup_setting('MOTUCLIENT_CONFIG'),

        ncwms_name = lookup_setting('NCWMS_NAME'),
        admin_emails = lookup_setting_list('ADMINS'),
        mpirun_args = lookup_setting_list('MPIRUN_ARGS'),

        ww3_prepared_path_template =
            '{root}/{series}/%Y/%m/%d/00/{simulation}/_latest'.format(
                root = deployments_path,
                series = 'waves-na',
                simulation = 'ww3',
            )
    ).setup_simulation_template(cmd_args.deployment_id)
    template.kwargs['step_context']['date_time'] = run_datetime

    template.render().run()

    logging.shutdown()
