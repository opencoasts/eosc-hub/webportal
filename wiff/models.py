from django.db.models import CASCADE, Model, OneToOneField, FilePathField
from django.contrib.postgres.fields import JSONField

import datamodel.models as dm_models


class Deployment(Model):

    # rename to 'front'
    parent = OneToOneField(dm_models.Deployment, on_delete=CASCADE,
                                            related_name='wiff', editable=False)
    # datamodel = OneToOneField(dm_models.Deployment, on_delete=CASCADE,
    #                                         related_name='wiff', editable=False)

    config = JSONField(blank=True, null=True)

    def __str__(self):
        return str(self.parent)
        # return str(self.datamodel)
