import argparse
import logging
import multiprocessing.pool
import os
from collections.abc import Sequence
from datetime import datetime, date, time, timedelta
from django.conf import settings
from functools import partial
from pathlib import Path
from time import sleep

if __name__ == '__main__':
    import django
    django.setup()

from ProjOpenCoastS.setup import _ENV_PREFIX
from datamodel.models import Deployment
from wiff import steps as schism_steps_oc

from core.utils import QueuedExecution
from simulation.core.entities import Series, Simulation
from simulation.core.steps.local_workplace import LocalWorkplace0
from simulation.schism import steps as schism_steps
from simulation.ww3 import steps as ww3_steps

START_WAIT = timedelta(seconds=5)
SERIES_INTERVAL = timedelta(days=1)

LOGGING_FILE_LEVEL = logging.DEBUG
LOGGING_CONSOLE_LEVEL = logging.DEBUG

LOGGING_FMT = '%(asctime)s %(levelname)s' \
                  ' %(name)s # %(module)s.%(funcName)s:%(lineno)s "%(message)s"'

logger = logging.getLogger(__name__)


class Base:

    settings_map = dict()

    def __init__(self, **kwargs):
        #self.setting = dict( self._parse_settings(**kwargs) )
        for setting, value in self._parse_settings(**kwargs):
            if hasattr(self, setting) or hasattr(type(self), setting):
                raise ValueError(f'{setting} is already defined!')
            else:
                setattr(self, setting, value)

    def _parse_settings(self, **kwargs):
        for setting, default in self.settings_map.items():
            value = kwargs.pop(setting, default)

            if value is None:
                raise ValueError(f'{setting} must be defined!')

            yield (setting, value)

        if kwargs:
            remain_args_str = ', '.join( kwargs.keys() )
            raise ValueError(f'Unknown arguments: {remain_args_str}')


class WW3Main(Base):

    settings_map = dict(
        series_name = 'waves-na',
        simulation_name = 'ww3',

        root_path = '',
        bin_path = 'bin/ww3',
        static_path = 'static/ww3',
        mpirun_args = (),
    )

    def setup_simulation_template(self):
        return Simulation.template(self.simulation_name,
            step_context = dict(
                bin_path = self.bin_path,
                static_path = self.static_path,
            ),

            step_templates = (
                LocalWorkplace0.template('workplace',
                    root_path = self.root_path,
                    # initial_files = (
                    # ),
                ),
                ww3_steps.Prepare.template('prepare',
                ),
                ww3_steps.Forcings.template('forcings',
                    extent = dict(
                        lat=(0., 75.),
                        lon=(-98., 13.),
                    ),
                ),
                ww3_steps.Compute.template('compute',
                    mpirun_args = self.mpirun_args,
                ),
                ww3_steps.Outputs.template('outputs',
                    output_field = True,
                ),
            )
        )

    def setup_forecast_template(self):
        return Series.template(self.series_name,
            interval=timedelta(hours=24),
            simulation_context = dict(
            ),

            simulation_templates=(
                self.setup_simulation_template(),
            ),
        )

    def launch_simulation(self, run_date, **kwargs):
        logger.debug(' >> launch_simulation(%s, %s)', run_date, kwargs)

        try:
            logger.info(' ### STARTED SIMULATION: main WW3 @ %s ###', run_date)
            begin_dt = datetime.now()

            self.setup_forecast_template().render(
                begin = run_date,
                end = run_date,
            ).simulate()

            duration = datetime.now() - begin_dt
            logger.info(' ### FINISHED SIMULATION: main WW3 @ %s, took %s ###',
                                                             run_date, duration)

        except:
            logger.exception(
                  ' ### ERROR: something went wrong while running main WW3 ###')


class SCHISM(Base):

    settings_map = dict(
        series_name_template = 'd{id}',
        simulation_name = 'schism',

        root_path = '',
        ncwms_name = 'default',
        ncmws_dir_template = '{series_name}/_latest/{simulation_name}/_latest',
        admin_emails = (),

        ww3_prepared_path_template = None,
        compute_template = None,
        mpirun_args = (),

        bin_path = 'bin/schism',
        static_path = 'static/schism',
        ww3_bin_path = 'bin/ww3',
        ww3_static_path = 'static/ww3',

        fes2014_exec = 'fes2014.sh',
        motuclient_exec = 'motuclient',
        motuclient_config = 'motuclient.ini',

        concurrent_wait = timedelta(minutes=1),
        series_interval = timedelta(days=1),
        recover_intervals = 0,
        force_recover = False,
    )

    @classmethod
    def with_local_mpi_compute(cls, mpirun_args, **kwargs):
        return cls(
                compute_template =
                    schism_steps.Compute.template('compute',
                        mpirun_args = mpirun_args,
                    ),
                mpirun_args = mpirun_args,
                **kwargs
            )

    @classmethod
    def with_sproxy_compute(cls, url, blob_service, job_service,
                                                     label_prefix='', **kwargs):
        return cls(
                compute_template =
                    schism_steps.SProxyCompute.template('compute',
                        url = url,
                        blob_service = blob_service,
                        job_service = job_service,
                        label_prefix = label_prefix,
                    ),
                **kwargs
            )

    def _series_name(self, deployment_id):
        return self.series_name_template.format(id=deployment_id)

    def setup_simulation_template(self, deployment_id):
        return Simulation.template(self.simulation_name,
            step_context = dict(
                bin_path = self.bin_path,
                static_path = self.static_path,
                deployment_id = deployment_id,
            ),

            step_templates = (
                LocalWorkplace0.template('workplace',
                    root_path = self.root_path,
                    initial_files = (
                    ),
                ),
                schism_steps_oc.Announce.template('announce_oc',
                    admin_emails = self.admin_emails,
                ),
                schism_steps_oc.Prepare.template('prepare_oc',
                    deployment_id = deployment_id,
                ),
                schism_steps.Prepare.template('prepare',
                ),
                schism_steps.PrepareWWM.template('prepare_wwm',
                    ww3_bin_path = self.ww3_bin_path,
                    ww3_static_path = self.ww3_static_path,
                    ww3_prepared_path_template = 
                                                self.ww3_prepared_path_template,
                    ww3_mpirun_args = self.mpirun_args,
                ),
                schism_steps.Forcings.template('forcings',
                    fes2014_exec = self.fes2014_exec,
                    motuclient_exec = self.motuclient_exec,
                    motuclient_config = self.motuclient_config,
                ),
                schism_steps.Configure.template('configs',
                ),
                self.compute_template,
                schism_steps.Combine.template('combine',
                ),
                schism_steps.Cleanup.template('cleanup',
                ),
                schism_steps_oc.Update.template('update_oc',
                    ncwms_name = self.ncwms_name,
                    ncwms_deployment_location = self.ncmws_dir_template.format(
                        series_name = self._series_name(deployment_id),
                        simulation_name = self.simulation_name,
                    ),
                ),
                # schism_steps_oc.Finalize.template('finalize_oc',
                #     admin_emails = self.admin_emails,
                # ),
            )
        )

    # try to pass deployment_id by series' template context
    def setup_forecast_template(self, deployment_id):
        return Series.template(self._series_name(deployment_id),
            interval = timedelta(hours=24),

            simulation_context = dict(
            ),

            simulation_templates = (
                self.setup_simulation_template(deployment_id),
            )
        )

    def launch_deployment_simulations(self, deployment, end_run_datetime):
        logger.debug(' >> launch_deployment_simulations(%s, %s)', deployment,
                                                               end_run_datetime)

        try:
            recover_interval = self.series_interval * self.recover_intervals
            begin_run_datetime = end_run_datetime - recover_interval

            last_run_date = deployment.last_run_date
            if last_run_date:
                last_run_datetime = datetime.combine(last_run_date, time(0) )
                too_late_to_resume = last_run_datetime < begin_run_datetime

                if not (too_late_to_resume or self.force_recover):
                    begin_run_datetime = last_run_datetime

            if begin_run_datetime > end_run_datetime:
                logger.info(' ### SIMULATION SERIES IS UP TO DATE: %s @ %s ###',
                                                   deployment, end_run_datetime)
                return

            logger.info(' ### STARTED SIMULATION SERIES: %s [%s -> %s] ###',
                               deployment, begin_run_datetime, end_run_datetime)
            begin_dt = datetime.now()

            self.setup_forecast_template(deployment.id).render(
                begin = begin_run_datetime,
                end = end_run_datetime,
            ).simulate()

            duration = datetime.now() - begin_dt
            logger.info(' ### FINISHED SIMULATION SERIES: %s, took %s ###',
                                                           deployment, duration)

        except:
            logger.exception(
                ' ### ERROR: something went wrong while running %s ###',
                                                                     deployment)

    # include timmings
    def launch_simulations(self, run_datetime, pool_size=1, **kwargs):
        logger.debug('run date: %s', run_date)

        deployment_set = (
            Deployment.objects
                .filter(deleted = False)
                .filter(active = True)
                .filter(begin_date__lte = run_datetime)
                .filter(end_date__gte = run_datetime)
                .order_by('id')
        )
        logger.info('deployment set: %s', deployment_set)

        concurrent = pool_size > 1

        if concurrent:
            from simulation.mixins.process_handler import ProcessHandler
            ProcessHandler.mpirun_queue.set_concurrency(pool_size)
            QueuedExecution.enable()

        # extra_workers allows to run non queued code on such workers
        # eg: while heavy computation (via mpirun) are limited by pool_size,
        # extra workers will getting ready (fetching forcings and stuff
        # like that) to start crunching as soon as resources become free
        extra_workers = 1
        with multiprocessing.pool.ThreadPool(pool_size+extra_workers) as pool:

            for deployment in deployment_set:

                args = (deployment, run_datetime)

                logger.debug(' > apply_async(%s, %s, %s)',
                               self.launch_deployment_simulations, args, kwargs)
                pool.apply_async(self.launch_deployment_simulations, args,
                                                                         kwargs)

                #pool.wait(self.concurrent_wait.total_seconds() )
                if concurrent:
                    sleep( self.concurrent_wait.total_seconds() )

            pool.close()
            pool.join()


def setup_logging(log_path, console_verbose=False, base_logger=logger,
                console_lvl=LOGGING_CONSOLE_LEVEL, file_lvl=LOGGING_FILE_LEVEL):

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.NOTSET)
    logger_formatter = logging.Formatter(fmt=LOGGING_FMT)

    # outputs just launcher log records to console
    console_output = logging.StreamHandler()
    console_output.setLevel(console_lvl)
    console_output.setFormatter(logger_formatter)

    console_logger = root_logger if console_verbose else base_logger
    console_logger.addHandler(console_output)

    # logs everything to weekly rotating file
    file_log_path = log_path / 'launcher'
    file_log = logging.handlers.TimedRotatingFileHandler(file_log_path,
                                                      when='W1', backupCount=10)
    file_log.setLevel(file_lvl)
    file_log.setFormatter(logger_formatter)
    root_logger.addHandler(file_log)

def setup_argparser():
    cmd_parser = argparse.ArgumentParser(
        description = 'OPENCoastS launcher'
    )

    cmd_parser_models = cmd_parser.add_mutually_exclusive_group()

    cmd_parser_models.add_argument('-WW3', '--only_ww3',
        action = 'store_true',
        help = "run only main WW3 simulation",
    )

    cmd_parser_models.add_argument('-SCHISM', '--only_schism',
        action = 'store_true',
        help = "run only SCHISM simulations",
    )

    cmd_parser.add_argument('-p', '--path',
        dest = 'root_path',
        metavar = 'PATH',
        type = Path,
        default = Path.cwd(),
        help = "forecasting root path",
    )

    cmd_parser.add_argument('-o', '--offset',
        dest = 'offset_days',
        metavar = 'DAYS',
        type = int,
        default = 0,
        help = "run date offset (days: positive=past, negative=future)",
    )

    cmd_parser.add_argument('-r', '--recover',
        dest = 'recover_days',
        metavar = 'DAYS',
        type = int,
        default = 0,
        help = "recover period (days)",
    )

    cmd_parser.add_argument('-f', '--force',
        action = 'store_true',
        help = 'force recover period',
    )

    cmd_parser.add_argument('-c', '--concurrency',
        metavar = 'COUNT',
        type = int,
        default = 1,
        help = "concurrent deployments",
    )

    cmd_parser_execution = cmd_parser.add_mutually_exclusive_group()

    # cmd_parser_execution.add_argument('-m', '--mpirun_args',
    #     metavar = 'ARGS',
    #     nargs = '+',
    #     help = "run locally using MPI, mpirun arguments may defined")

    cmd_parser_execution.add_argument('-s', '--sproxy',
        action = 'store_true',
        help = "use sproxy to offload the simulation run",
    )

    cmd_parser.add_argument('-w', '--no_wait',
        action = 'store_false',
        dest = 'wait',
        help = 'execute immediately rather than wait 5 seconds',
    )

    # cmd_parser.add_argument('-d', '--debug',
    #     action = 'store_true',
    #     help = 'set debug logging level')

    cmd_parser.add_argument('-v', '--verbose',
        action = 'store_true',
        help = 'print everything to terminal',
    )

    return cmd_parser

def logged_lookup_(msg, obj, *args):
    logger.debug(msg, obj, *args)
    return obj

lookup_env = lambda name, default=None: os.getenv(_ENV_PREFIX+name, default)
lookup_django = lambda name, default=None: getattr(settings, name, default) 
lookup_setting = lambda name: logged_lookup_(f'{name} = %s',
                                       lookup_django(name) or lookup_env(name) )
lookup_setting_list = lambda name, env_sep=' ': logged_lookup_(f'{name} = %s',
    lookup_django(name) or tuple( lookup_env(name, default='').split(env_sep) )
)

if __name__ == '__main__':
    cmd_args = setup_argparser().parse_args()

    root_path = Path(cmd_args.root_path).resolve()
    dir_path_ = lambda dir: root_path / dir

    logs_path = Path( lookup_setting('LOGS_PATH') or dir_path_('logs') )
    setup_logging(logs_path, console_verbose=cmd_args.verbose)

    logger.debug('cmd_args: %s', cmd_args)

    run_date = ( datetime.combine( date.today(), time(0) ) -
                                          timedelta(days=cmd_args.offset_days) )
    deployments_path = Path( lookup_setting('DEPLOYMENTS_PATH') or
                                                       dir_path_('deployment') )

    if cmd_args.wait:
        wait_secs = START_WAIT.total_seconds()
        print(f"Starting in {int(wait_secs)} seconds (Ctrl + C to stop)!")
        sleep(wait_secs)

    ww3_main = WW3Main(
        root_path = deployments_path,
        bin_path = Path( lookup_setting('WW3_BIN_PATH') ),
        static_path = Path( lookup_setting('WW3_STATIC_PATH') ),
        mpirun_args = lookup_setting_list('WW3_MPIRUN_ARGS'),
    )

    if not cmd_args.only_schism:
        ww3_main.launch_simulation(run_date)

    if not cmd_args.only_ww3:
        schism_with_sproxy = partial(
            SCHISM.with_sproxy_compute,
            url = lookup_setting('SPROXY_URL'),
            job_service = lookup_setting('SPROXY_JOB_SERVICE'),
            blob_service = lookup_setting('SPROXY_BLOB_SERVICE'),
            label_prefix = lookup_setting('SPROXY_LABEL_PREFIX'),
        )
        schism_with_local_mpi = partial(
            SCHISM.with_local_mpi_compute,
            mpirun_args = lookup_setting_list('MPIRUN_ARGS'),
        )

        schism_launcher = schism_with_sproxy if cmd_args.sproxy \
                                                      else schism_with_local_mpi

        schism_launcher(
            root_path = deployments_path,
            bin_path = Path( lookup_setting('SCHISM_BIN_PATH') ),
            static_path = Path( lookup_setting('SCHISM_STATIC_PATH') ),
            ww3_bin_path = Path( lookup_setting('WW3_BIN_PATH') ),
            ww3_static_path = Path( lookup_setting('WW3_STATIC_PATH') ),

            fes2014_exec = lookup_setting('FES2014_PATH'),
            motuclient_exec = lookup_setting('MOTUCLIENT_PATH'),
            motuclient_config = lookup_setting('MOTUCLIENT_CONFIG'),

            ncwms_name = lookup_setting('NCWMS_NAME'),
            admin_emails = lookup_setting_list('ADMINS'),
            mpirun_args = lookup_setting_list('MPIRUN_ARGS'),

            force_recover = cmd_args.force,
            series_interval = SERIES_INTERVAL,
            recover_intervals = cmd_args.recover_days,
            ww3_prepared_path_template =
                '{root}/{series}/%Y/%m/%d/00/{simulation}/_latest'.format(
                    root = deployments_path,
                    series = ww3_main.series_name,
                    simulation = ww3_main.simulation_name,
                )
            
        ).launch_simulations(run_date, pool_size=cmd_args.concurrency)

    logging.shutdown()
