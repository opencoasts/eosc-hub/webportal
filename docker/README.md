# Deploy with Docker

> **Warning**: default passwords SHOULD be changed in production systems!

## Setup
The following commands must be executed inside the folder containing the
`docker-compose.yml` file or specify its location by including the `-f` or
`--file` argument (use `-h` or `--help` flag to know how):

    # build images
    docker-compose build

    # start services in the background (-d)
    docker-compose up -d

    # check containers status and logs
    docker-compose ps
    docker-compose logs

    # create database tables
    docker-compose exec webportal python -m manage migrate

    # load database required content
    docker-compose exec webportal python -m manage loaddata datamodel front

    # create administrator account
    docker-compose exec webportal python -m manage createsuperuser

    # change web portal (django) settings via setup tool
    docker-compose exec webportal python -m ProjOpenCoastS.setup --help

    # reload web portal (served by gunicorn) settings
    docker-compose kill -s HUP webportal

    # stop and remove containers, networks, images, and volumes
    docker-compose down


## Dockerfiles

**Base** and **Web Portal** hold the web application code base.  
**PostGIS** and **pgAdmin4** add customizations by wrapping well established
Docker Hub images.

### Base (`Dockerfile`)
Based on the _python:3.6-buster_ image, this file defines an image providing
the software platform (dependencies) and the web application (at `/project`)
without configurations.

### Web Portal (`Dockerfile-webportal`)
Based on _opencoasts/django:0_ image, built from the base file, provides
the missing configurations and an entry point to serve the web application on
port 8000. 
_gunicorn_ is the web server and since its execution is defined via
an `ENTRYPOINT` it is possible to pass it extra arguments (eg: `--workers=2`)
in `docker run` and `docker create`.  
The following arguments (with defaults) allow some build customization:
 - `postgis_host = postgis`
 - `postgis_port = 5432`
 - `postgis_user = oc_user`
 - `postgis_password = password`
 - `postgis_database = opencoasts`
 - `deployments_path = /__deployments__`

### PostGIS (`Dockerfile-postgis`)
Based on _postgis/postgis:13-3.0_ image, this file defines init scripts and
defaults.

### pgAdmin4 (`Dockerfile-pgadmin`)
Based on the _dpage/pgadmin4:4_ image, this file just defines the `postgis`
server to be included in pgAdmin's list of servers.


## Docker Compose (`docker-compose.yml`)

### Services

#### `webportal`:
    build:
      context: ./
    image: opencoasts/webportal:0
    command: --workers=2
    ports:
      - 8000:8000
    volumes:
      - type: bind
        source: ./data
        target: /_deployments
        read_only: true
    restart: always
    depends_on:
      - postgis

#### `postgis`:
    build:
      context: docker/postgis/
    image: opencoasts/postgis:0
    volumes:
      - type: volume
        source: postgis_data
        target: /var/lib/postgresql/data
    restart: always

#### `pgadmin`:
    build:
      context: docker/pgadmin/
    image: opencoasts/pgadmin:0
    environment:
      PGADMIN_DEFAULT_EMAIL: admin@local
      PGADMIN_DEFAULT_PASSWORD: password
    restart: always
    depends_on:
      - postgis

#### `ncwms`:
    build:
      context: docker/ncwms/
      dockerfile: Dockerfile
    image: opencoasts/ncwms:0
    ports:
      - 8080:8080
    volumes:
      - type: bind
        source: ./data
        target: /_data
        read_only: true
    restart: always

### Networks
Currently no custom configurations.

### Volumes
  - `postgis_data`
