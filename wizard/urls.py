from django.conf.urls import url
from .views import *
from django.conf.urls.static import static


urlpatterns = [
    url(r'^$', open_wizard, name='open_wizard'),

    url(r'^step1/$', templates[0]['function'], name='step1'),
    url(r'^step1/disclaimer/$', accept_disclaimer, name='accept_disclaimer'),
    url(r'^step1/restart/$', templates[0]['restart'], name='step1_restart'),
    url(r'^step1/get_run_periods/(?P<mv_pk>[0-9]+)/$', get_run_periods, name='get_run_periods'),
    url(r'^step2/$', templates[1]['function'], name='step2'),
    url(r'^step2/restart/$', templates[1]['restart'], name='step2_restart'),
    url(r'^step2/validate_file/$', validate_file, name='step2_validate_file'),
    url(r'^step3/$', templates[2]['function'], name='step3'),
    url(r'^step3/restart/$', templates[2]['restart'], name='step3_restart'),
    url(r'^step4/$', templates[3]['function'], name='step4'),
    url(r'^step4/restart/$', templates[3]['restart'], name='step4_restart'),
    url(r'^step5/$', templates[4]['function'], name='step5'),
    url(r'^step5/restart/$', templates[4]['restart'], name='step5_restart'),
    url(r'^step6/$', templates[5]['function'], name='step6'),
    url(r'^step6/restart/$', templates[5]['restart'], name='step6_restart'),
    url(r'^step7/$', templates[6]['function'], name='step7'),
    # url(r'^save_steps/$', save_steps, name='save_steps'),
    # url(r'^save_step/(?P<step>[0-9]+)/$', save_step, name='save_step'),
    url(r'^save_steps/(?P<step>[0-9]+)/$', save_steps, name='save_steps'),
]
