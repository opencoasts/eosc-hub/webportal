import decimal

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from django.forms.widgets import RadioSelect

from django.db.models import Q

from itertools import chain

import datamodel.models as dm_models
import front.models as front_models

from wizard.utils import member_run_3d


F_TYPE = (
    ('ocean', _('Oceânica')),
    ('river', _('Fluvial')),
)

STAT_TYPE = (
    ('C', _('Comparação')),
    ('V', _('Virtual')),
)

PAR_TYPE = (
    ('D', _('Parâmetros predefinidos')),
    ('C', _('Customizar parâmetros')),
)

AF_TYPE = (
    ('C', _('Customizar valor')),
    ('U', _('Carregar ficheiro')),
)

RUN_TYPE = (
    ('basic2d', _('2D Barotrópico')),
    ('basic3d', _('3D Baroclínico')),
    ('waves2d', _('2D Ondas e correntes')),
    # ('waves3d', _('3D Ondas e correntes')),
)

RUN_TYPE_map = dict(RUN_TYPE)


class SelectWithDisabled(RadioSelect):
    """
    Subclass of Django's field widget that allows disabling options.
    To disable an option, pass a dict instead of a string for its label,
    of the form: {'label': 'option label', 'disabled': True}
    """

    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None):
        disabled = False
        if isinstance(label, dict):
            label, disabled = label['label'], label['disabled']
        option_dict = super(SelectWithDisabled, self).create_option(name, value, label, selected, index, subindex=subindex, attrs=attrs)
        if disabled:
            option_dict['attrs']['disabled'] = 'disabled'
            option_dict['attrs']['title'] = _('Não tem permissões para esta opção')
        return option_dict


class ModelForm(forms.Form):
    # Fill ModelVersions and ModelVersionRunPeriod form
    def __init__(self, *args, **kwargs):
        initial = kwargs.pop('initial', None)
        user = kwargs.pop('user', None)

        super(ModelForm, self).__init__(*args, **kwargs)

        # Restrict ModelVersions to 'schism' or 'self' models, for now
        query_model = dm_models.Model.objects.filter(reference__in=['schism', 'selfe'])
        query_modelversion = dm_models.ModelVersion.objects.filter(child__public=True, model__in=query_model)

        query_modelversionperiod, disable_inputs, mv_pk, run_period, run_type = None, False, None, None, 'basic2d'
        if initial:
            if 'model' in initial:
                disable_inputs = True   # Once model exists, no longer able to change inputs since they affect other steps

                # Limits ModelVersionRunPeriods to already selected ModelVersion
                mv_pk = initial['model']
                query_modelversionperiod = dm_models.ModelVersionRunPeriod.objects.filter(model_version__pk=mv_pk).order_by('run_period')

            if 'run_period' in initial:
                run_period = initial['run_period']

            if 'run_type' in initial:
                run_type = initial['run_type']

        # If user has no 3D permissions
        rt_choices = []
        has3D = user and member_run_3d(user)
        for rt_key, rt_label in RUN_TYPE_map.items():
            label = dict(label=rt_label, disabled=False)

            # disable 3D for users without such permission
            if not has3D and '3d' in rt_key:
                label['disabled'] = True

            rt_choices.append((rt_key, label))

        self.fields['run_type'] = forms.ChoiceField(choices=rt_choices, widget=SelectWithDisabled, label=_('Selecione tipo de corrida'), disabled=disable_inputs, initial=run_type)

        self.fields['model'] = forms.ModelChoiceField(queryset=query_modelversion, widget=forms.Select, label=_('Selecione um modelo (*)'), disabled=disable_inputs, initial=mv_pk)

        rp_choices = []
        if query_modelversionperiod:
            rp_choices = [(choice.pk, str(choice.run_period) + 'h') for choice in query_modelversionperiod]

        rp_choices.insert(0, ('', '---------'))
        #self.fields['run_period'] = forms.ChoiceField(choices=rp_choices, widget=forms.Select, label=_('Selecione um período (*)'), disabled=rp_disabled, initial=selvalue, help_text=_('A selecão do período condiciona a disponibilidade de forçamentos para as Condições de Fronteira'))
        self.fields['run_period'] = forms.ChoiceField(choices=rp_choices, widget=forms.Select, label=_('Selecione um período (*)'), disabled=disable_inputs, initial=run_period)


class DataInputForm(forms.Form):
    def __init__(self, *args, **kwargs):
        run_type = kwargs.pop('run_type', None)
        super(DataInputForm, self).__init__(*args, **kwargs)

        query1 = front_models.CRS.objects.all()
        #temporary
        if run_type and 'waves' in run_type:
            query1 = front_models.CRS.objects.filter(name__contains = 'UTM')

        query2 = front_models.CRSV.objects.all()

        # Create fields
        self.fields['file'] = forms.FileField(required=True, label=_('Selecione uma malha horizontal (*)'))

        if run_type and '3d' in run_type:
            self.fields['file_3d'] = forms.FileField(required=True, label=_('Selecione uma malha vertical (*)'))

        self.fields['crs_list'] = forms.ModelChoiceField(required=False, queryset=query1, widget=forms.Select, label=_('Sistema de Referência de Coordenadas da Malha'))
        self.fields['crs'] = forms.CharField(required=True, label=_('ou introduza um código EPSG (*)'))
        self.fields['crsv_list'] = forms.ModelChoiceField(required=False, queryset=query2, widget=forms.Select, label=_('Referêncial Vertical da Malha'))
        self.fields['crsv'] = forms.FloatField(required=True, label=_('ou introduza um valor em metros (*)'))
        self.fields['calcdt'] = forms.BooleanField(required=False, initial=False, widget=forms.CheckboxInput, label=_('Calcular sugestão para o passo de cálculo (dt)'), help_text=_('Pode aumentar significativamente o tempo de processamento.'))
        self.fields['sat'] = forms.BooleanField(required=False, initial=False, widget=forms.CheckboxInput, label=_('Obter imagens de satélite para a malha definida:'), help_text=_('As imagens de satélite só estarão disponíveis ao final do dia de hoje.'))
        # self.fields['ipre'] = forms.BooleanField(required=False, initial=False, disabled=True, widget=forms.CheckboxInput, label=_('Verificar consistência e validade da Malha'), help_text=_('Pode aumentar significativamente o tempo de processamento.'))

        # Add widget attributes
        self.fields['crs'].widget.attrs = {"pattern": '[0-9]{4,8}', "title": _('Código de 4 a 8 dígitos')}
        self.fields['crsv'].widget.attrs = {"step": '0.01'}
        #self.fields['ipre'].widget.attrs = {"title": _('Disponível brevemente')}


class ForcingChoiceForm(forms.Form):
    # TODO: in the future replace ftype fixed choice by QuerySet BoundaryKind with category = 'wat'
    # TODO: Also change from RadioSelect to Select and replace fixed code in step_front template
    def __init__(self, *args, **kwargs):
        # Workaround condition models to selected runperiod
        run_type = kwargs.pop('run_type', None)
        bids = kwargs.pop('bids', None)
        super(ForcingChoiceForm, self).__init__(*args, **kwargs)

        self.fields['ftype'] = forms.ChoiceField(choices=F_TYPE, widget=forms.RadioSelect, label=_('Selecione o tipo de fronteira'), initial='O', required=False)
        self.fields['forcings'] = forms.CharField(widget=forms.HiddenInput(), label='', required=False)
        self.fields['forcings'].widget.attrs = {"class": 'hidden'}

        months = [_('Jan'), _('Fev'), _('Mar'), _('Abr'), _('Mai'), _('Jun'), _('Jul'), _('Ago'), _('Set'), _('Out'), _('Nov'), _('Dez'), '']
        # Add flux monthly fields
        for i in range(13):
            fieldname = 'flux' + str(i+1)
            # Add help text to last monthly field
            if i == 11:
                self.fields[fieldname] = forms.FloatField(required=False, label='', help_text=_(
                    "Nota: valores positivos são de entrada no domínio; valores negativos são de saída do domínio"))
            else:
                self.fields[fieldname] = forms.FloatField(required=False, label='')
            self.fields[fieldname].widget.attrs = {"placeHolder": months[i] + ' [m3/s]', "step": '0.001'}

        # Added this requirement on Feb2019
        self.fields['url'] = forms.URLField(label=_('Fonte de Previsão de fluxos'), required=False,
                                            help_text=_("Url para recolha de dados de previsão de fluxo (aceita urls dinâmicos)"))
        self.fields['url'].widget.attrs = {"class": 'long', "placeHolder": 'http://'}

        self.fields['bid_perc'] = forms.FloatField(required=False, label=_('Percentagem'))
        self.fields['bid_perc'].widget.attrs = {"step": '0.01', 'min': 0.01, "placeHolder": '%'}
        bids_choices = [(x, x) for x in bids]
        bids_choices.insert(0, ('', '---------'))
        self.fields['bids'] = forms.ChoiceField(choices=bids_choices, widget=forms.Select, label=_('de'), required=False,
                                                    help_text=_("Atenção: a fronteira selecionada também terá de ser definida como fluvial"))

        if run_type and '3d' in run_type:
            # Add temperature and salinity monthly fields
            for i in range(13):
                fieldname1 = 'temp' + str(i + 1)
                self.fields[fieldname1] = forms.FloatField(required=False, label='')
                self.fields[fieldname1].widget.attrs = {"placeHolder": months[i] + ' [ºC]', "step": '0.01', 'min': 0, 'max': 40}

                fieldname2 = 'salt' + str(i + 1)
                self.fields[fieldname2] = forms.FloatField(required=False, label='')
                self.fields[fieldname2].widget.attrs = {"placeHolder": months[i] + ' [PSU]', "step": '0.01', 'min': 0, 'max': 42}


class ForcingAtmModelForm(forms.Form):
    def __init__(self, *args, **kwargs):
        run_type = kwargs.pop('run_type', None)
        super(ForcingAtmModelForm, self).__init__(*args, **kwargs)

        # TODO: change this query must depend on selected modelversion
        # If 3D only provide GFS forcings
        if run_type and '3d' in run_type:
            query_atm = front_models.ForcingSource.objects.filter(parent__reference='noaa-gfs')
        else:
            query_atm = front_models.ForcingSource.objects.filter(forcing_kind__category='atm').filter(active=True)
        atmmodel_choices = [(x.parent_reference(), x.parent_name()) for x in query_atm]
        # Adds noforcing option manually as 2nd choice
        atmmodel_choices.insert(0, ('noforcing', 'No forcing'))
        atmmodel_choices.insert(0, ('', '---------'))
        self.fields['atmmodel'] = forms.ChoiceField(choices=atmmodel_choices, widget=forms.Select, label=_('Forçamento Atmosférico'), required=True)
        self.fields['atmmodel'].widget.attrs = {"class": 'long'}


class ForcingOceanModelForm(forms.Form):
    def __init__(self, *args, **kwargs):
        runperiod_pk = kwargs.pop('runperiod_pk', None)
        run_type = kwargs.pop('run_type', None)
        super(ForcingOceanModelForm, self).__init__(*args, **kwargs)

        # TODO: change this query must depend on selected modelversion
        query_ocean_all = front_models.ForcingSource.objects.filter(forcing_kind__category='wat').filter(active=True)
        query_ocean = query_ocean_all.exclude(parent__reference__contains='ww3')

        # TODO: stabilize this workaround - condition models to selected runperiod
        try:
            runperiod = dm_models.ModelVersionRunPeriod.objects.get(id=runperiod_pk)
            if runperiod.run_period > 48:
                # Exclude prism2017 for run periods over 48 hours
                query_ocean = query_ocean.exclude(parent__reference='prism2017')
        except Exception:
            pass

        oceanmodel_choices = [(x.parent_reference(), x.parent_name()) for x in query_ocean]
        oceanmodel_choices.insert(0, ('', '---------'))
        self.fields['oceanmodel'] = forms.ChoiceField(choices=oceanmodel_choices, widget=forms.Select, label=_('Forçamento para Circulação a aplicar a todas as fronteiras oceânicas'), required=False)
        self.fields['oceanmodel'].widget.attrs = {"class": 'long'}

        if run_type:
            if 'waves' in run_type:
                query_ocean_waves = query_ocean_all.filter(parent__reference__contains='ww3')
                oceanmodel_waves_choices = [(x.parent_reference(), x.parent_name()) for x in query_ocean_waves]
                # Adds noforcing option manually as 2nd choice
                oceanmodel_waves_choices.insert(0, ('noforcing', 'No forcing'))
                oceanmodel_waves_choices.insert(0, ('', '---------'))
                self.fields['oceanmodel_waves'] = forms.ChoiceField(choices=oceanmodel_waves_choices, widget=forms.Select,
                                                                    label=_('Forçamento para Ondas a aplicar a todas as fronteiras oceânicas'),
                                                                    required=False, help_text=_("Fronteiras oceânicas fora da região do forçamento não serão consideradas."))
                self.fields['oceanmodel_waves'].widget.attrs = {"class": 'long'}

            if '3d' in run_type:
                # Exclude fes2014 for 3D run types
                query_ocean = query_ocean.exclude(parent__reference='fes2014')

                # TODO: add category 'wat_3d' to model in order to limit water forcings to 3D run types
                query_ocean_3d = query_ocean.filter(parent__reference__contains='cmems')
                oceanmodel_3d_choices = [(x.parent_reference(), x.parent_name()) for x in query_ocean_3d]
                oceanmodel_3d_choices.insert(0, ('', '---------'))
                self.fields['oceanmodel_temp_salt'] = forms.ChoiceField(choices=oceanmodel_3d_choices, widget=forms.Select,
                                                              label=_('Forçamento para Temperatura e Salinidade a aplicar a todas as fronteiras oceânicas'),
                                                              required=False)
                self.fields['oceanmodel_temp_salt'].widget.attrs = {"class": 'long'}
                # Commented separate forcings for Temperature ans Salinity, for this version use only one forcing for both
                # self.fields['oceanmodel_temp'] = forms.ChoiceField(choices=oceanmodel_3d_choices, widget=forms.Select,
                #                                               label=_('Forçamento para Temperaturas a aplicar a todas as fronteiras oceânicas'),
                #                                               required=False)
                # self.fields['oceanmodel_temp'].widget.attrs = {"class": 'long'}
                # self.fields['oceanmodel_salt'] = forms.ChoiceField(choices=oceanmodel_3d_choices, widget=forms.Select,
                #                                                    label=_('Forçamento para Salinidades a aplicar a todas as fronteiras oceânicas'),
                #                                                    required=False)
                # self.fields['oceanmodel_salt'].widget.attrs = {"class": 'long'}


class StationForm(forms.Form):
    lat = forms.FloatField(required=False, label=_('Latitude (*)'))
    lon = forms.FloatField(required=False, label=_('Longitude (*)'), help_text=_('(coordenadas decimais)'))
    name = forms.CharField(required=False, label=_('Nome (*)'))
    stype = forms.ChoiceField(choices=STAT_TYPE, widget=forms.RadioSelect, label=_('Selecione o tipo de estação'), initial='C', required=False)
    cstation = forms.ChoiceField(required=False, label=_('Comparar com Estação (latitude, longitude)'))

    stations = forms.CharField(widget=forms.HiddenInput(), label='', required=False)

    def __init__(self, *args, **kwargs):
        stations = kwargs.pop('stations', None)
        super(StationForm, self).__init__(*args, **kwargs)
        self.fields['lat'].widget.attrs = {"step": '0.000001'}
        self.fields['lon'].widget.attrs = {"step": '0.000001'}
        self.fields['name'].widget.attrs = {"class": 'long'}
        self.fields['stype'].widget.attrs = {"onChange": 'toggleFields();'}
        self.fields['cstation'].widget.attrs = {"class": 'long grp_C'}
        if stations:
            self.fields['cstation'].choices = stations


class ParChoiceForm(forms.Form):
    def __init__(self, *args, **kwargs):
        run_type = kwargs.pop('run_type', None)
        super(ParChoiceForm, self).__init__(*args, **kwargs)

        choices = PAR_TYPE
        # TODO: in a near future we allow param.in upload and parse
        #if run_type and '3d' in run_type:
        #    choices = PAR_TYPE + (('U', _('Carregar ficheiro')),)

        self.fields['parchoice'] = forms.ChoiceField(choices=choices, widget=forms.RadioSelect, label=_('Selecione uma das opções'), initial='D')
        self.fields['parchoice'].widget.attrs = {"onChange": 'toggleForms();'}


class WWMChoiceForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(WWMChoiceForm, self).__init__(*args, **kwargs)

        choices = PAR_TYPE
        self.fields['wwmchoice'] = forms.ChoiceField(choices=choices, widget=forms.RadioSelect, label=_('Selecione uma das opções'), initial='D')
        #self.fields['wwmchoice'].widget.attrs = {"onChange": 'toggleForms_wwm();'}


class ParFormset(forms.Form):
    # Fill form from parameters table
    def __init__(self, *args, **kwargs):
        run_type = kwargs.pop('run_type', None)
        sel_model_version = kwargs.pop('sel_model_version', None)
        file_type = kwargs.pop('file_type', None)
        values = kwargs.pop('values', None)
        dt_val = kwargs.pop('dt_val', None)
        _calc_dt = kwargs.pop('_calc_dt', None) # if dt is calculated on step2
        super(ParFormset, self).__init__(*args, **kwargs)

        def order_by(dic):
            try:
                return float(dic['order_by'])
            except Exception:
                return 0

        if sel_model_version:
            modelparameter, modelversionparameter, modelversionparameterwave, modelversionparameter3d = [], [], [], []

            if not file_type or file_type == 'params':
                # Get model from model_version
                model = dm_models.ModelVersion.objects.get(pk=sel_model_version).model

                # Get Base Model Parameters (active fields)
                modelparameter_queryset = front_models.ModelParameter.objects.filter(parent__model=model, active=True)
                if modelparameter_queryset:
                    modelparameter = modelparameter_queryset.values(
                        'category', 'par_type', 'readonly', 'min_value', 'max_value', 'choices', 'ruleset', 'order_by',
                        'parent__default_value', 'parent__description', 'parent__parameter__name', 'parent__notes'
                    )

                # Get Base Model Version Parameters (active fields)
                modelversionparameter_queryset = front_models.ModelVersionParameter.objects.filter(parent__model_version=sel_model_version, active=True)
                if modelversionparameter_queryset:
                    modelversionparameter = modelversionparameter_queryset.values(
                        'category', 'par_type', 'readonly', 'min_value', 'max_value', 'choices', 'ruleset', 'order_by',
                        'parent__default_value', 'parent__description', 'parent__parameter__name', 'parent__notes'
                    )

                if 'waves' in run_type:
                    # Get Base Model Version Parameters Waves (active fields)
                    modelversionparameterwave_queryset = front_models.ModelVersionParameterWave.objects.filter(
                        parent__model_version=sel_model_version, active=True, file_type='param')
                    if modelversionparameterwave_queryset:
                        modelversionparameterwave = modelversionparameterwave_queryset.values(
                            'category', 'par_type', 'readonly', 'min_value', 'max_value', 'choices', 'ruleset',
                            'order_by', 'parent__default_value', 'parent__description', 'parent__parameter__name', 'parent__notes'
                        )

                if '3d' in run_type:
                    # Get Base Model Version Parameters 3D (active fields)
                    modelversionparameter3d_queryset = front_models.ModelVersionParameter3D.objects.filter(parent__model_version=sel_model_version, active=True)
                    if modelversionparameter3d_queryset:
                        modelversionparameter3d = modelversionparameter3d_queryset.values(
                            'category', 'par_type', 'readonly', 'min_value', 'max_value', 'choices', 'ruleset', 'order_by',
                            'parent__default_value', 'parent__description', 'parent__parameter__name', 'parent__notes'
                        )
            else:
                # Get Base Model Version Parameters 3D (active fields)
                modelversionparameterwave_queryset = front_models.ModelVersionParameterWave.objects.filter(
                    parent__model_version=sel_model_version, active=True, file_type='wwminput')
                if modelversionparameterwave_queryset:
                    modelversionparameterwave = modelversionparameterwave_queryset.values(
                        'category', 'par_type', 'readonly', 'min_value', 'max_value', 'choices', 'ruleset',
                        'order_by', 'parent__default_value', 'parent__description', 'parent__parameter__name',
                        'parent__notes'
                    )
            #puts the default value of PROC_DELTC compatible with calculated dt
            for po in modelversionparameterwave:
                if po['parent__parameter__name'] == 'PROC_DELTC' and dt_val:
                    po['parent__default_value'] = int(dt_val)*10
                if po['parent__parameter__name'] == 'nstep_wwm' and _calc_dt:
                    po['parent__default_value'] = 10 #PROCDELTC/dt = 10 in this case

            pars_list = sorted(chain(modelparameter, modelversionparameter, modelversionparameter3d, modelversionparameterwave), key=order_by)
            # Get ordered union parameters
            if pars_list:
                # Create form field from QuerySet
                for row in pars_list:
                    # Rename dictionary attributes
                    row['name'] = row.pop('parent__parameter__name')
                    row['default'] = row.pop('parent__default_value')
                    row['description'] = row.pop('parent__description')
                    row['notes'] = row.pop('parent__notes')

                    # Condition fields according to step 2 selection
                    if (row['name'] == 'ics' and values['ics'] == 2) or (
                            row['name'] == 'cpp_lat' and 'cpp_lat' not in values):
                        # If WGS84 don't add parameters to form
                        continue

                    dynfield = {}
                    widgetatts = {}
                    options = self.get_options(row)


                    # Condition fields according to step 4 selection
                    if values:
                        if row['name'] in values:
                            row['default'] = values[row['name']]

                    # Create fields
                    dynfield[row['name']] = getattr(self, "create_field_for_" + str(
                        dict(front_models.FIELDTYPE)[row['par_type']]).lower())(row, options, widgetatts)

                    # Add JQuery events
                    if row['ruleset']:
                        widgetatts["data-ruleset"] = row['ruleset']

                    # Add the category of the field
                    widgetatts["data-category"] = row['category']

                    # Attach all gathered widget attributes
                    dynfield[row['name']].widget.attrs = widgetatts
                    self.fields.update(dynfield)

    def get_options(self, field):
        options = {}
        options['label'] = field['description'] + " (" + field['name'] + ")" if field['description'] else field['name']
        options['help_text'] = field['notes']
        options['required'] = True  # All parameters must have a default value
        options['disabled'] = bool(field['readonly'])
        return options

    def create_field_for_text(self, field, options, widgetatts):
        options['initial'] = str(field['default']) if field['default'] else None
        options['max_length'] = 50
        return forms.CharField(**options)

    def create_field_for_integer(self, field, options, widgetatts):
        options['initial'] = int(field['default']) if field['default'] else None
        options['min_value'] = int(field['min_value']) if field['min_value'] else None
        options['max_value'] = int(field['max_value']) if field['max_value'] else None
        widgetatts["min"] = int(field['min_value']) if str(field['min_value']) != 'None' else '-999999999'
        widgetatts["max"] = int(field['max_value']) if str(field['max_value']) != 'None' else '999999999'
        widgetatts["type"] = "number"
        return forms.IntegerField(**options)

    def create_field_for_float(self, field, options, widgetatts):
        # Can't use float because use_L10N is True (useful for user interaction)
        # and this activates localization on float but html/javascript only recognizes EN format
        options['initial'] = str(field['default']).replace(',', '.') if field['default'] else None
        options['min_value'] = str(field['min_value']).replace(',', '.') if field['min_value'] else None
        options['max_value'] = str(field['max_value']).replace(',', '.') if field['max_value'] else None
        widgetatts["min"] = str(field['min_value']).replace(',', '.') if str(field['min_value']) != 'None' else '-999999999.0'
        widgetatts["max"] = str(field['max_value']).replace(',', '.') if str(field['max_value']) != 'None' else '999999999.0'
        widgetatts["type"] = "number"
        # Set precision to step
        d = decimal.Decimal(field['default'])
        dec = d.as_tuple().exponent * -1
        if dec > 0:
            widgetatts["step"] = "0.%s" % (format(1, '0' + str(dec)))
        return forms.FloatField(**options)

    def create_field_for_choice(self, field, options, widgetatts):
        options['initial'] = int(field['default']) if type(field['default']) is int else str(field['default'])
        options['choices'] = [(c.split(": ")[0], c.split(": ")[0] + " | " + c.split(": ")[1].replace(";", "")) for c in field['choices'].split("\r\n")]
        return forms.ChoiceField(widget=forms.RadioSelect, **options)

    def create_field_for_list(self, field, options, widgetatts):
        options['initial'] = int(field['default']) if type(field['default']) is int else str(field['default'])
        options['choices'] = [(c.split(": ")[0], c.split(": ")[0] + " | " + c.split(": ")[1].replace(";", "")) for c in field.choices.split("\r\n")]
        return forms.ChoiceField(**options)

    def create_field_for_boolean(self, field, options, widgetatts):
        #options['initial'] = True if (field['default'] == 'true' or field['default'] == '1') else False
        #options['required'] = False
        #return forms.BooleanField(widget=forms.CheckboxInput, **options)
        # IMPORTANT - replaced checkbox for radiobox - unchecked checkboxes aren't passed to form POST
        #print('form', field['default'])
        options['initial'] = int(field['default'])
        options['choices'] = [(1, 'on'), (0, 'off')]
        return forms.ChoiceField(widget=forms.RadioSelect, **options)

    def create_field_for_array(self, field, options, widgetatts):
        defvalue = str(field['default']) if field['default'] else None
        if defvalue:
            options['initial'] = defvalue
            deflen = defvalue.replace(' ', '').split('|')
            widgetatts["data-array"] = len(deflen)
        return forms.CharField(**options)


class ParUploadForm(forms.Form):
    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial', None)
        super(ParUploadForm, self).__init__(*args, **kwargs)

        self.fields['file'] = forms.FileField(required=True, label=_('Selecione um ficheiro (*)'))
        if initial:
            self.fields['file_aux'] = forms.CharField(required=False, label=_('Ficheiro'), max_length=200)
            self.fields['file_aux'].initial = initial['file']
            self.fields['file_aux'].widget.attrs = {"class": 'noedit', "readonly": ''}


class AuxFilesFormset(forms.Form):
    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial', None)
        super(AuxFilesFormset, self).__init__(*args, **kwargs)

        self.fields['name'] = forms.CharField(required=False, label='', max_length=200, widget=forms.HiddenInput())
        self.fields['label'] = forms.CharField(required=False, label='', max_length=200, widget=forms.HiddenInput())

        if initial and 'options' in initial:
            readonly = False
            if 'readonly' in initial['options']:
                # Change label of disabled form and do not create other fields
                change_label = "{0} ({1})".format(initial['label'], _('Indisponível'))
                self.initial['label'] = change_label
                readonly = True

            if 'choice' in initial:
                self.fields['choice'] = forms.ChoiceField(choices=AF_TYPE, widget=forms.RadioSelect, label=_('Selecione uma das opções'), initial='C', disabled=readonly)
                self.fields['choice'].widget.attrs = {"onChange": 'toggleForms(this);'}

                widgetatts = {}
                if 'constant_type' in initial['options']:
                    self.fields['constant'] = getattr(self, "create_field_for_" + initial['options']['constant_type'])(initial, widgetatts)
                    self.fields['constant'].label = _('Constante')
                    self.fields['constant'].widget.attrs = widgetatts
                else:
                    self.fields['constant'] = forms.FloatField(required=False, label=_('Constante'), disabled=readonly)

                if readonly:
                    # Add hidden constant to be used instead of disabled constant field
                    self.fields['constant_hdn'] = forms.FloatField(widget=forms.HiddenInput(), label='', initial=initial['constant'])

                # if a file already was uploaded
                if 'file' in initial:
                    self.fields['file_aux'] = forms.CharField(required=False, label=_('Ficheiro'), max_length=200)
                    self.fields['file_aux'].initial = initial['file']
                    self.fields['file_aux'].widget.attrs = {"class": 'noedit', "readonly": ''}
                    self.fields['file'] = forms.FileField(required=False, label=_('Alterar ficheiro'))
                else:
                    self.fields['file'] = forms.FileField(required=False, label=_('Selecione um ficheiro'))

        else:
            self.fields['constant'] = forms.FloatField(required=False, label=_('Defina uma Constante'))
            self.fields['file'] = forms.FileField(required=False, label=_('Selecione um ficheiro'))

    def create_field_for_integer(self, initial, widgetatts):
        options = {}
        if 'min_value' in initial['options']:
            options['min_value'] = widgetatts["min"] = int(initial['options']['min_value'])
        if 'max_value' in initial['options']:
            options['max_value'] = widgetatts["max"] = int(initial['options']['max_value'])

        if 'constant' in initial:
            # Set initial value
            if initial['constant']:
                options['initial'] = str(initial['constant']).replace(',', '.')
            else:
                if 'min_value' in options and 'max_value' in options:
                    widgetatts["placeHolder"] = "[{0}-{1}]".format(options['min_value'], options['max_value'])

        if 'readonly' in initial['options']:
            options['disabled'] = initial['options']['readonly']
        widgetatts["type"] = "number"
        #widgetatts["required"] = ""
        return forms.IntegerField(**options)

    def create_field_for_float(self, initial, widgetatts):
        # Can't use float because use_L10N is True (useful for user interaction)
        # and this activates localization on float but html/javascript only recognizes EN format
        options = {}
        if 'min_value' in initial['options']:
            options['min_value'] = widgetatts["min"] = str(initial['options']['min_value']).replace(',', '.')
        if 'max_value' in initial['options']:
            options['max_value'] = widgetatts["max"] = str(initial['options']['max_value']).replace(',', '.')

        if 'constant' in initial:
            # Set initial value
            options['initial'] = initial['constant'] if initial['constant'] else None
            # Set precision to step
            dec = len(str(initial['constant']).split('.')[1]) if len(str(initial['constant']).split('.')) > 1 else 1
            if dec == 0:
                dec = 1
        else:
            if 'min_value' in options and 'max_value' in options:
                dec = len(str(options['min_value']).split('.')[1]) if len(str(options['min_value']).split('.')) > 1 else 1
                if dec == 0:
                    dec = 1
                widgetatts["placeHolder"] = "[{0}-{1}]".format(options['min_value'], options['max_value'])

                #Fix to make Albedo with 2 decimal cases
                if 'Albedo' in initial['name']:
                    dec = 2
                    widgetatts["placeHolder"] = "[{0:.2f}-{1:.2f}]".format(float(options['min_value']), float(options['max_value']))
            else:
                dec = 1

        if 'readonly' in initial['options']:
            options['disabled'] = initial['options']['readonly']
        widgetatts["step"] = "0.%s" % (format(1, '0' + str(dec)))
        widgetatts["type"] = "number"
        #widgetatts["required"] = ""
        return forms.FloatField(**options)


class DeployForm(forms.Form):
    name = forms.CharField(max_length=100, label=_('Nome (*)'), required=False)
    description = forms.CharField(max_length=200, label=_('Descrição'), required=False, widget=forms.Textarea())

    def __init__(self, *args, **kwargs):
        super(DeployForm, self).__init__(*args, **kwargs)

        self.fields['name'].widget.attrs = {"class": 'long'}
        self.fields['description'].widget.attrs = {"class": 'long'}

    def clean_name(self):
        tmp_name = self.cleaned_data['name']

        # validate file entry
        if not tmp_name:
            raise ValidationError(_('Campo obrigatório.'))

        return tmp_name


class ExtendDeployForm(forms.Form):
    #to_date = forms.DateField(label=_('Estender até'), initial=timezone.now(), input_formats=('%Y-%m-%d'))
    to_date = forms.DateField(label=_('Estender até'))
    motive = forms.CharField(max_length=250, label=_('Motivo'), widget=forms.Textarea())
    deploy_id = forms.IntegerField(widget=forms.HiddenInput(), label='', required=False)

    def __init__(self, *args, **kwargs):
        super(ExtendDeployForm, self).__init__(*args, **kwargs)

        self.fields['motive'].widget.attrs = {"class": 'long'}

