import json
import requests

from django.conf import settings
from datetime import timedelta

from io import TextIOWrapper

from django.contrib.sessions.models import Session

from django.utils.translation import ugettext_lazy as _, string_concat, override
from django.utils import timezone

from django.contrib.gis.geos import Point, LineString, LinearRing, Polygon, MultiPolygon, GeometryCollection

from django.template.loader import get_template
from django.utils.safestring import mark_safe

from geojson import Feature, Point as geojson_Point
from shapely.ops import linemerge

from datamodel import models as dm_models
from front import models as front_models
from front.utils import isWGS84, notify_error, file2bytes

from .forecast_models import schism_v540 as schism

logger = settings.LOGGER.getChild('wizard')


STATION_TYPE = (
    ('O', _('Observação')),
    ('C', _('Comparação')),
    ('V', _('Virtual')),
)

FORCING_TYPE = (
    ('circulation', _('Circulação')),
    ('flow', _('Fluxo')),
    ('temperature', _('Temperatura')),
    ('salinity', _('Salinidade')),
    ('waves', _('Ondas')),
)


def get_state(step, active, begin_date, end_date, runperiodhours, last_run_date):
    # Function that returns deployment's state
    # State -1: Active with error and expiring
    # State 0: Active with error
    # State 1: Active expiring
    # State 2: Active
    # State 3. Configured
    # State 4: In configuration
    # State 5: Deactivated
    # State 6: Deactivated - no activation possible
    # State 7: Expired Active
    # State 8: Expired Deactivated
    state = None

    if not active and not begin_date:
        # In configuration process
        if step < 6:
            state = 4
        elif step == 6 or (step == 7 and not begin_date):
            state = 3
    else:
        today = timezone.now().date()
        expire_label_date = end_date - timedelta(days=7)  # TODO - fixed days but this should be adjustable
        error_date = today - timedelta(days=1)

        if active:
            # Active
            state = 2
            # Extra labels
            if expire_label_date <= today <= end_date:
                # Expiring
                state = 1
            if last_run_date and last_run_date < error_date:
                # Deployment with errors
                state = 0
            if last_run_date and last_run_date < error_date and expire_label_date <= today <= end_date:
                # Deployment with errors and expiring
                state = -1
            if today > (begin_date + timedelta(days=1)) and not last_run_date:
                # Deployment should have ran already for the first time, so post error
                state = 0
        else:
            # Deactivated
            state = 5

            # Commented on 26/09/2018 - Team decided to remove this restriction
            #can_activate_date = today - timedelta(hours=runperiodhours)
            #if last_run_date and last_run_date < can_activate_date:
            #    state = 6

        # Overriding any state if end_date has passed
        if today > end_date:
            # Expired
            if active:
                state = 7
            else:
                state = 8

    return state


def render_state(state, step, pk):
    # Function that returns state label and action buttons according to state
    if state == 3 or state == 4:
        # Deployment in a Configuration Stage
        actions_html = """
           <button type='button' class='btn btn-primary btn-xs' data-action='open' data-id='{0}' data-toggle='tooltip' data-placement='right' title='{1}'>
             <span class='glyphicon glyphicon-cog'></span>
           </button>
           <button type='button' class='btn btn-danger btn-xs' data-action='delete' data-id='{0}' data-toggle='tooltip' data-placement='right' title='{2}'>
             <span class='glyphicon glyphicon-remove'></span>
           </button>
           """.format(pk, _('Abrir Sistema'), _('Apagar Sistema'))

        if state == 4:
            # In configuration
            strval = '<span class="label label-primary">{0} {1}</span>'.format(_('Passo'), step+1)
        else:
            # Configured
            strval = '<span class="label label-primary">{0}</span>'.format(_('Configurado'))
            actions_html += """
               <button type='button' class='btn btn-primary btn-xs' data-action='print' data-id='{0}' data-toggle='tooltip' data-placement='right' title='{1}'>
                 <span class='glyphicon glyphicon-print'></span>
               </button>""".format(pk, _('Imprimir Configuração'))
    else:
        # Disabling actions
        disable_activate, disable_deactivate, disable_delete, disable_extend = '', '', '', ''

        if -1 <= state <= 2:
            # Active
            strval = '<span class="label {0}">{1}</span>'.format("label-success", _('Ativo'))
            disable_delete = "disabled='disabled'"

            # Extra labels
            if state == 1 or state == -1:
                # Expiring
                strval += '<br/><span class="label label-warning">{0}</span>'.format(_('A expirar'))

            if state == 0 or state == -1:
                # Deployment with errors
                err_msg = str(_('Verifique se existem ficheiros de log para este sistema no Visualizador.'))
                strval += '<br/><span class="label label-danger" title="{1}"><a href="{2}">{0}</a></span>'.format(_('Erro'), err_msg, '/viewer')
                disable_extend = "disabled='disabled'"

        elif state == 7 or state == 8:
            # Expired
            strval = '<span class="label label-default basic-btns">{0}</span>'.format(_('Expirado'))
            disable_activate = "disabled='disabled'"
            disable_deactivate = "disabled='disabled'"
            disable_extend = "disabled='disabled'"
        else:
            # Deactivated
            strval = '<span class="label {0}">{1}</span>'.format("label-default", _('Desativado'))
            disable_extend = "disabled='disabled'"

            if state == 6:
                # Deactivated, activation no longer possible
                disable_activate = "disabled='disabled'"

        actions_html = """
           <button type='button' class='btn btn-primary btn-xs' data-action='view' data-id='{0}' data-toggle='tooltip' data-placement='right' title='{1}'>
             <span class='glyphicon glyphicon-eye-open'></span>
           </button>
           <button type='button' class='btn btn-danger btn-xs' data-action='delete' data-id='{0}' {5} data-toggle='tooltip' data-placement='right' title='{2}'>
             <span class='glyphicon glyphicon-remove'></span>
           </button>
           <button type='button' class='btn btn-primary btn-xs' data-action='print' data-id='{0}' data-toggle='tooltip' data-placement='right' title='{3}'>
             <span class='glyphicon glyphicon-print'></span>
           </button>
           <button type='button' class='btn btn-primary btn-xs' data-action='clone' data-id='{0}' data-toggle='tooltip' data-placement='right' title='{4}'>
             <span class='glyphicon glyphicon-random'></span>
           </button>
           """.format(pk, _('Ver Configuração'), _('Apagar Sistema'), _('Imprimir Configuração'), _('Clonar Sistema'), disable_delete)

        if -1 <= state <= 2 or state == 7:
            actions_html += """
           <button type='button' class='btn btn-warning btn-xs' data-action='deactivate' data-id='{0}' {2} data-toggle='tooltip' data-placement='right' title='{1}'>
             <span class='glyphicon glyphicon-pause'></span>
           </button>""".format(pk, _('Desativar Sistema'), disable_deactivate)
        elif 5 <= state <= 6 or state == 8:
            actions_html += """
           <button type='button' class='btn btn-warning btn-xs' data-action='reactivate' data-id='{0}' {2} data-toggle='tooltip' data-placement='right' title='{1}'>
             <span class='glyphicon glyphicon-play'></span>
           </button>""".format(pk, _('Reativar Sistema'), disable_activate)

        actions_html += """
           <button type='button' class='btn btn-primary btn-xs' data-action='extend' data-id='{0}' {2} data-toggle='tooltip' data-placement='right' title='{1}'>
             <span class='glyphicon glyphicon-calendar'></span>
           </button>""".format(pk, _('Estender Período'), disable_extend)

    return strval, actions_html


def new_deploy_possible(user):
    # Validate if user can create new system
    deploy_maxnr = user.profile.deploy_maxnr
    today = timezone.now().date()

    # Count active, non deleted and non expired Deployments
    deploy_count = front_models.Deployment.objects.filter(user=user).filter(active=True).filter(deleted=False).filter(end_date__gte=today).count()

    if deploy_count >= deploy_maxnr:
        return False
    else:
        return True


def validate_bboxes(boundaries, selcrs):
    # Validates if boundaries are inside the official Bounding Boxes
    boundariesinbbox, msg, minpercentage = True, None, 50
    boundary_objs = json.loads(boundaries)

    try:
        # Build Boundaries Geometry Collection Polygon
        allgeoms = []
        for obj in boundary_objs['features']:
            allgeoms.append(LineString(obj['geometry']['coordinates']))
        geom_coll = GeometryCollection(allgeoms, srid=4326).convex_hull

        # Get Global BBox Geometry
        bboxobj = front_models.BBox.objects.filter(bbox_type=front_models.BBox.BOXTYPE_GLOBAL).values('polygon')[0]['polygon']
        '''
        # Validate if percentage of All Boundaries Extent is inside Global BBox
        if minpercentage:
            intersection = geom_coll.intersection(bboxobj)

            # Prepare to compute areas in meters (WGS84 Web Mercator)
            geom_coll.transform(3857)
            intersection.transform(3857)

            # If area inside BBox is less than minpercentage
            if round(intersection.area * 100 / geom_coll.area) < minpercentage:
                boundariesinbbox = False
        else:
            boundariesinbbox = geom_coll.within(bboxobj)
        '''
        # Validate if All Boundaries Extent is inside Global BBox
        boundariesinbbox = geom_coll.within(bboxobj)  # Must be 100% inside Global BBox
        if not boundariesinbbox:
            return False, str(string_concat(_('A malha tem de estar dentro da zona global de atuação! Verifique se o Sistema de Referência "'), selcrs, _('" é o correto.')))

    except Exception:
        logger.exception('Error validating if grid inside bbox')

        boundariesinbbox = False
        msg = _('Ocorreu um erro! Por favor tente mais tarde.')

    return boundariesinbbox, msg


def validate_outerboundaries(boundaries):
    closedouterboundaries, msg = True, None

    try:
        boundary_objs = json.loads(boundaries)
        geos_poly, lines = Polygon(), []
        for obj in boundary_objs['features']:
            line = LineString(obj['geometry']['coordinates'])
            if not line.closed:
                lines.append(line)

        # Added this condition - it is now possible to load a grid with islands only (28/09/2018))
        if len(lines) > 0:
            mergedlines = linemerge(lines)
            geos_poly.exterior_ring = LinearRing(tuple(mergedlines.coords))

    except Exception:
        logger.exception('Error validating outer boundaries connected')

        closedouterboundaries = False
        msg = _('A malha apresenta erros de geometria. Verifique se as fronteiras exteriores estão ligadas.')

    return closedouterboundaries, msg


def valid_aux_file(tmp_file, hgrid_nrelems, hgrid_nrnodes):
    # Get number of elements and nodes of file, if they do not match current grid file return false
    match, nrelems, nrnodes, mustconvert = False, 0, 0, False
    try:
        for chunk in tmp_file.chunks():
            lines = chunk.splitlines()
            # Get number of elements and nodes
            nrelems = int(lines[1].split()[0])
            nrnodes = int(lines[1].split()[1])

            # Check if WGS84
            first_node = lines[2].split()
            if not isWGS84(first_node[0], first_node[1]):
                mustconvert = True

            break

        if hgrid_nrelems == nrelems and hgrid_nrnodes == nrnodes:
            match = True

    except Exception:
        logger.exception('Error matching auxiliary file to hgrid')

    return match, mustconvert


def calc_maxfilesize(max_nodes):
    # Calculate file maximum size regarding maximum number of nodes limitation
    # bits counting with number of max_nodes
    max_bits_size = (max_nodes * 3) * 64 + (max_nodes + max_nodes * 2 * 5) * 32
    # Add to bits size it's half (to count with spaces/tabs in file) and convert to bytes
    max_bytes_size = (max_bits_size + max_bits_size / 2) / 8

    return max_bytes_size


def get_key(dic, value):
    for key in dic:
        if dic[key] == value:
            return key


def time_to_rate(user):
    # Check if its time to ask user for a rate of the service
    deps = front_models.Deployment.objects.filter(user=user).filter(active=True)

    if deps.count() == 0:
        # User has not submitted any deployments yet
        return None
    else:
        ratings = front_models.UserProfileRating.objects.filter(userprofile=user.profile)

        if ratings.count() == 0:
            # No ratings yet
            # Get first submitted deployment
            dep = deps[:1].get()

            # Check if deploy has 2 weeks of forecast results
            today = timezone.now().date()
            begin_run_date = dep.begin_run_date

            if begin_run_date:
                working_time = begin_run_date + timedelta(days=14)

                if today >= working_time:
                    return True
                else:
                    return None
            else:
                return None
        else:
            return None


def form_validation(request, step,):
    msg, flag, step = str(_('Alterações aplicadas com sucesso.')), True, int(step)
    step_name = 'step' + str(step)

    try:
        formdata = request.POST

        if step == 3:
            atmmodel = None
            if 'atmmodel' in formdata:
                # Reading Atm Model
                atmmodel = formdata['atmmodel']

                # Check if selected atmospheric forcing has BBox and if true get polygon
                bbox_atm = front_models.BBox.objects.filter(forcing_source__parent__reference=atmmodel)
                if bbox_atm:
                    bboxobj_atm = bbox_atm.values('polygon')[0]['polygon']
                    # Validate if Grid's Extent is inside atmospheric forcing bbox
                    ext = request.session['step2']['extent']  # Grid's extent
                    extent_obj = Polygon(((ext[0], ext[1]), (ext[0], ext[3]), (ext[2], ext[3]), (ext[2], ext[1]),
                                          (ext[0], ext[1])))
                    if not extent_obj.within(bboxobj_atm):
                        flag = False
                        msg = str(_('A malha têm de estar dentro da zona do forçamento atmosférico selecionado (ver legenda do mapa).'))

            if flag:
                if not atmmodel:
                    atmmodel = 'noforcing'

                # Saving atmospheric forcing choice to session
                request.session[step_name] = {'atmmodel': atmmodel}

                if 'forcings' in formdata and 'oceanmodel' in formdata:
                    # Reading Forcings Table
                    forcings = json.loads(formdata['forcings'])

                    # Reading Ocean forcings
                    oceanmodel = formdata['oceanmodel']

                    oceanmodel_waves = None
                    if 'oceanmodel_waves' in formdata:
                        oceanmodel_waves = formdata['oceanmodel_waves']

                    oceanmodel_temp_salt = None
                    if 'oceanmodel_temp_salt' in formdata:
                        oceanmodel_temp_salt = formdata['oceanmodel_temp_salt']

                    # Ocean - run_type == basic3d
                    # Commented separate forcings for Temperature ans Salinity, for this version use only one forcing for both
                    # oceanmodel_temp, oceanmodel_salt = None, None
                    # if 'oceanmodel_temp' in formdata:
                    #     oceanmodel_temp = formdata['oceanmodel_temp']
                    #
                    # if 'oceanmodel_salt' in formdata:
                    #     oceanmodel_salt = formdata['oceanmodel_salt']


                    bboxobj, bboxpols = None, []
                    # Check if selected ocean forcing has BBox and if true get polygon
                    bboxobj_forcings = front_models.BBox.objects.exclude(forcing_source__isnull=True)
                    for obj in bboxobj_forcings:
                        ref = obj.forcing_source.parent_reference()
                        # if ref == oceanmodel or ref == oceanmodel_temp or ref == oceanmodel_salt:
                        if ref == oceanmodel or ref == oceanmodel_temp_salt or ref == oceanmodel_waves:
                            if not bboxobj or ref == oceanmodel_waves:
                                bboxobj = obj.polygon
                            else:
                                # Get intersection area between polygons
                                bboxobj_inter = obj.polygon.intersection(bboxobj)
                                if bboxobj_inter:
                                    bboxobj = bboxobj_inter

                    # Get Boundaries
                    boundary_objs = json.loads(request.session['step2']['boundaries']['geojson'])
                    for n, item in enumerate(forcings):
                        if item['ftype'] == 'ocean':
                            # Validate if ocean boundary inside ocean forcing bbox
                            if bboxobj:
                                for obj in boundary_objs['features']:
                                    if obj['properties']['code'] == item['bid']:
                                        line = LineString(obj['geometry']['coordinates'])
                                        if not line.within(bboxobj):
                                            # If waves forcing no need to invalidate, just change forcing to 'noforcing'
                                            if "waves" in item['fmodel']:
                                                item['fmodel']['waves']['value'] = "No forcing"
                                            else:
                                                flag = False

                        if flag and isinstance(item['fmodel'], dict):
                            # Forcings defined on version schism_3d
                            new_dic = {}
                            for key, value in item['fmodel'].items():
                                # replace translated text to code string
                                new_key = str(get_item_code(FORCING_TYPE, _(key)))
                                new_dic[new_key] = value

                            forcings[n]['fmodel'] = new_dic

                    if not flag:
                        # if not oceanmodel_temp and not oceanmodel_salt:
                        if not oceanmodel_temp_salt:
                            msg = str(_('As fronteiras definidas como Oceânicas têm de estar dentro da zona do forçamento selecionado (ver legenda do mapa).'))
                        else:
                            msg = str(_('As fronteiras definidas como Oceânicas têm de estar dentro das zonas dos forçamentos selecionados (ver legenda do mapa).'))

                    # Check if all Open Boundaries have conditions set to move to next step
                    if flag and forcings:
                        # Saving oceanic forcing choices to session
                        request.session[step_name]['forcings'] = forcings

                        if oceanmodel:
                            request.session[step_name]['oceanmodel'] = oceanmodel

                        # Commented separate forcings for Temperature ans Salinity, for this version use only one forcing for both
                        # if oceanmodel_temp:
                        #     request.session[step_name]['oceanmodel_temp'] = oceanmodel_temp
                        #
                        # if oceanmodel_salt:
                        #     request.session[step_name]['oceanmodel_salt'] = oceanmodel_salt

                        if oceanmodel_temp_salt:
                            request.session[step_name]['oceanmodel_temp_salt'] = oceanmodel_temp_salt
                else:
                    # No oceanmodel defined - use 'noforcing' reference
                    request.session[step_name]['oceanmodel'] = 'noforcing'

                if flag and 'forcings' in formdata and 'oceanmodel_waves' in formdata:
                    request.session[step_name]['oceanmodel_waves'] = formdata['oceanmodel_waves']

        elif step == 4:
            # Reading Stations
            stations = json.loads(formdata['stations'])
            # Saving session
            request.session[step_name] = {'stations': stations}

        elif step == 5:
            # Reading Parameters
            if step_name in request.session:
                params = request.session[step_name]['params']
                step5_dic = request.session[step_name]
            else:
                params = request.session['step2']['params']
                step5_dic = {}
            parchoice = formdata['parchoice']

            if 'wwmchoice' in formdata:
                params_wwm = {}
                if step_name in request.session and 'wwm' in request.session[step_name]:
                    params_wwm = request.session[step_name]['wwm']['params']

                wwmchoice = formdata['wwmchoice']
                if wwmchoice == 'C':
                    for item in formdata:
                        if 'cst_wwm-' in item:
                            params_wwm[item.replace('cst_wwm-', '')] = formdata[item]

                    step5_dic['wwm'] = {'choice': wwmchoice, 'params': params_wwm}
                elif wwmchoice == 'D':
                    if parchoice == 'D':
                        params.pop('nstep_wwm', None)

                    step5_dic['wwm'] = {'choice': wwmchoice, 'params': params_wwm}

                elif wwmchoice == 'U':
                    # Get param.in File
                    formfiles = request.FILES
                    tmp_file = formfiles['cst_wwm-file']
                    filename = tmp_file.name

                    # Save param.in as sessin file
                    session = Session.objects.get(pk=request.session.session_key)
                    file_type = dm_models.FileType.objects.get(name='wwminput', format='nml')
                    session_file, created = front_models.SessionFile.objects.get_or_create(file_type=file_type, session=session)
                    session_file.name = filename
                    session_file.file = file2bytes(tmp_file)
                    session_file.save()

                    step5_dic['wwm'] = {'choice': wwmchoice, 'params': params_wwm, 'file': filename}

            if parchoice == 'C':
                for item in formdata:
                    if 'cst-' in item:
                        params[item.replace('cst-', '')] = formdata[item]

                # Changes in dt value reflect on the following params
                params['nspool'] = round(3600 / float(params['dt']))
                params['ihfskip'] = round(24 * 3600 / float(params['dt']))
                params['hotout_write'] = round(24 * 3600 / float(params['dt']))

                step5_dic['choice'] = parchoice
                step5_dic['params'] = params

            elif parchoice == 'D':
                # Remove dt and related parameters if they were set before in order to use default parameters
                params.pop('dt', None)
                params.pop('nspool', None)
                params.pop('ihfskip', None)
                params.pop('hotout_write', None)

                if 'wwmchoice' in formdata and wwmchoice == 'C' and 'cst-nstep_wwm' in formdata:
                    # In this case we also need to pass a param.in parameter that depends on wwminput.nml
                    params['nstep_wwm'] = formdata['cst-nstep_wwm']

                step5_dic['choice'] = parchoice
                step5_dic['params'] = params

            elif parchoice == 'U':
                # Get param.in File
                formfiles = request.FILES
                tmp_file = formfiles['cst-file']
                filename = tmp_file.name

                # Save param.in as sessin file
                session = Session.objects.get(pk=request.session.session_key)
                file_type = dm_models.FileType.objects.get(name='param', format='in')
                session_file, created = front_models.SessionFile.objects.get_or_create(file_type=file_type, session=session)
                session_file.name = filename
                session_file.file = file2bytes(tmp_file)
                session_file.save()

                step5_dic['choice'] = parchoice
                step5_dic['params'] = params
                step5_dic['file'] = filename

            request.session[step_name] = step5_dic

        elif step == 6:
            # Reading Aux Files
            formfiles = request.FILES

            jsondata = []
            formscount = int(formdata['form-TOTAL_FORMS'])

            # Read hgrid's nrelems and nrnodes for matching purposes
            nrelems = request.session['step2']['grid_info']['elems']
            nrnodes = request.session['step2']['grid_info']['nodes']

            # Commenting following code: no need to save aux files, only save constant or array
            '''
            session = Session.objects.get(pk=request.session.session_key)
            # Get hgrid.ll file in order to use it as base for auxfile construction
            if 'deploy_id' not in request.session:
                # Get file from SessionFile model
                file_type_hgrid = dm_models.FileType.objects.get(name='hgrid', format='ll')
                session_file_hgrid = front_models.SessionFile.objects.get(file_type=file_type_hgrid,
                                                                          session=session)
                hgrid_lines = bytes(session_file_hgrid.file).decode().split('\n')
            else:
                # Get file from DeploymentFile model
                try:
                    dep_id = request.session['deploy_id'][0]
                    deploy = front_models.Deployment.objects.get(id=dep_id)
                    file_type_hgrid = dm_models.FileType.objects.get(name='hgrid', format='ll')
                    deploy_file_hgrid = dm_models.DeploymentFile.objects.get(file_type=file_type_hgrid,
                                                                             deployment=deploy)
                    hgrid_lines = bytes(deploy_file_hgrid.file).decode().split('\n')
                except Exception:
                    # if there is a deployment id but not a deployment file an error will occur
                    # this happens when user has restarted step Domain of the wizard and hasn't saved changes
                    file_type_hgrid = dm_models.FileType.objects.get(name='hgrid', format='ll')
                    session_file_hgrid = front_models.SessionFile.objects.get(file_type=file_type_hgrid,
                                                                              session=session)
                    hgrid_lines = bytes(session_file_hgrid.file).decode().split('\n')
            '''

            for i in range(formscount):
                choice, constant, name, label, fileobj = None, None, None, None, None
                if 'form-' + str(i) + '-choice' in formdata:
                    choice = formdata['form-' + str(i) + '-choice']
                if 'form-' + str(i) + '-constant' in formdata:
                    constant = formdata['form-' + str(i) + '-constant']
                if 'form-' + str(i) + '-constant_hdn' in formdata:
                    # Constant is in hidden field because editable field is disabled and is not passed on POST
                    constant = formdata['form-' + str(i) + '-constant_hdn']
                    choice = "C"
                if 'form-' + str(i) + '-name' in formdata:
                    name = formdata['form-' + str(i) + '-name']
                    # lower_name = str(name).lower()
                if 'form-' + str(i) + '-label' in formdata:
                    label = formdata['form-' + str(i) + '-label']
                    if 'form-' + str(i) + '-constant_hdn' in formdata:
                        # Remove note from label
                        label = label.split('(')[0]
                if formfiles and 'form-' + str(i) + '-file' in formfiles:
                    fileobj = 'form-' + str(i) + '-file'

                if choice == 'C' and constant:
                    jsondata.append({'name': name, 'choice': choice, 'constant': constant, 'label': label})

                    # Commenting following code: no need to save aux files, only save constant or array
                    '''
                    # Build and, if exists, update auxfile with constant as value
                    newfile = schism.write_auxfile_fromhgrid(hgrid_lines, constant, None, lower_name)
                    # Get or Create file - no duplicates this way
                    file_type = dm_models.FileType.objects.get(name=lower_name, format='ll')
                    session_file, created = front_models.SessionFile.objects.get_or_create(file_type=file_type, session=session)
                    session_file.file = newfile
                    session_file.save()
                    '''
                elif choice == 'U' and fileobj:
                    file_array = []
                    if fileobj in formfiles:
                        tmp_file = formfiles[fileobj]
                        filename = tmp_file.name

                        # Validate if nrelems and nrnodes matches the grid file
                        filematch, mustconvert = valid_aux_file(tmp_file, nrelems, nrnodes)
                        if filematch:
                            # Commenting following code: no need to save aux files, only save constant or array
                            '''
                            if mustconvert:
                                # Commenting following code: no need to save all files in the original format, only need original hgrid
                                # Save file to SessionFile as gr3
                                # file_type = dm_models.FileType.objects.get(name=lower_name, format='gr3')
                                # # Get or Create - no duplicates this way
                                # session_file, created = front_models.SessionFile.objects.get_or_create(file_type=file_type, session=session)
                                # session_file.file = file2bytes(tmp_file)
                                # session_file.name = filename
                                # session_file.save()

                                # Save version of gr3 (original) file to hgrid ll
                                tmp_file.file.seek(0)  # Reposition file to start before reading lines
                                new_lines = TextIOWrapper(tmp_file.file, encoding='utf-8').readlines()
                                fileaux = schism.write_auxfile_fromhgrid(hgrid_lines, None, new_lines, lower_name)
                            else:
                                fileaux = file2bytes(tmp_file)

                            # Save file to SessionFile only once as ll - already in WGS84
                            file_type = dm_models.FileType.objects.get(name=lower_name, format='ll')
                            # Get or Create - no duplicates this way
                            session_file, created = front_models.SessionFile.objects.get_or_create(file_type=file_type, session=session)
                            session_file.file = fileaux
                            session_file.name = filename
                            session_file.save()
                            '''

                            tmp_file.file.seek(0)
                            file_lines = TextIOWrapper(tmp_file.file, encoding='utf-8').readlines()
                            file_array = schism.get_auxfile_array(file_lines)
                        else:
                            msg = str(_('Malha não compatível com a fornecida no Passo Domínio')) + ': ' + label
                            flag = False
                            break
                    else:
                        filename = formdata['form-' + str(i) + '-file_aux']

                    jsondata.append({'name': name, 'choice': choice, 'array': file_array, 'file': filename, 'label': label})

            # Saving session
            if flag:
                request.session[step_name] = {'auxfilesset': jsondata}

    except Exception:
        logger.exception('Error on form validation step %s' % step)
        flag = False
        msg = str(_('Não foi possível aplicar as alterações. Tente mais tarde, por favor!'))

    return flag, msg


def activate_deploy(request):
    # Activates a deploy from the Wizard
    if 'last_step' in request.session and 'step1' in request.session and 'step2' in request.session \
            and 'step3' in request.session and 'step4' in request.session and 'step5' in request.session \
            and 'step6' in request.session and 'step7' in request.session:
        try:
            user = request.user
            deploy_period = request.user.profile.deploy_period

            # Validate if user can create new systems
            if not new_deploy_possible(request.user):
                return False, str(_('Lamentamos mas atingiu o seu limite de Sistemas de Previsão ativos.'))

            mv_runperiod = dm_models.ModelVersionRunPeriod.objects.get(id=request.session['step1']['run_period_pk'])

            reference = request.session['wizard_key']
            now = timezone.now()

            name = request.session['step7']['name']

            deploy, created = front_models.Deployment.objects.get_or_create(reference=reference,
                                                                            defaults={'creation_datetime': now,
                                                                                      'user': user,
                                                                                      'name': name, 'active': False,
                                                                                      'model_version_period': mv_runperiod})

            # Update model version run period
            deploy.model_version_period = mv_runperiod

            # Update name
            deploy.name = name

            deploy.step = 7
            deploy.step1 = request.session['step1']
            deploy.step2 = request.session['step2']
            deploy.step3 = request.session['step3']
            deploy.step4 = request.session['step4']
            deploy.step5 = request.session['step5']
            deploy.step6 = request.session['step6']
            deploy.step7 = request.session['step7']

            deploy.description = request.session['step7']['description']
            deploy.active = True
            deploy.begin_date = now
            deploy.end_date = now.now() + timedelta(days=deploy_period) # + timedelta(hours=mv_runperiod.run_period)

            deploy_id = 'ID:' + str(deploy.id)

            try:
                deploy.save()
            except Exception:
                analyze_deploy = front_models.Deployment.objects.filter(reference=reference)
                flag_pass = False
                if analyze_deploy:
                    if analyze_deploy[0].active and analyze_deploy[0].step == 7:
                        # Error no post_save signal, ignore
                        flag_pass = True

                if flag_pass:
                    # Notify Admin
                    notify_error(user, "On PostSave Signal - Activate Deploy: {{0}}".format(deploy_id))
                else:
                    logger.exception('Error activating deploy - on save')
                    return False, str(_('Ocorreu um erro! Por favor tente mais tarde.'))

            # Save all Sessionfile(s) to DeploymentFile(s)
            session = Session.objects.get(pk=request.session.session_key)
            session_files = front_models.SessionFile.objects.filter(session=session)
            if session_files:
                for session_file in session_files:
                    # Create or replace
                    deploy_file, created = dm_models.DeploymentFile.objects.get_or_create(file_type=session_file.file_type, deployment=deploy)
                    deploy_file.file = session_file.file
                    deploy_file.name = session_file.name
                    deploy_file.save()

                    # Delete already saved file from SessionFile
                    session_file.delete()

            run_type = "basic2d"
            if 'run_type' in deploy.step1:
                run_type = deploy.step1["run_type"]

            # Backend needs parameter dt, so if it was not set (customized) we add it
            if 'params' in deploy.step5 and 'dt' not in deploy.step5["params"]:
                # TODO: get default values from template files
                
                dt_val = {'basic2d': 100., 'basic3d': 30., 'waves2d': 30.}
                dramp_val = {'basic2d': 1., 'basic3d': 2., 'waves2d': 1.}
                nstep_wwm_val = {'basic2d': 1, 'basic3d': 1, 'waves2d': 10}

                dt = dt_val.get(run_type)
                dramp = dramp_val.get(run_type)
                nstep_wwm = nstep_wwm_val.get(run_type)
                # OLD CODE
                #if '3d' in run_type:
                #    dt = 30.
                #else:
                #    dt = 100. 
                
                # Add dt to parameters
                deploy.step5["params"]['dt'] = dt
                
                # modified to put all variables in PARAMS in case user leave everything default             
                deploy.step5["params"]['nspool'] = round(3600 / dt)
                deploy.step5["params"]['ihfskip'] = round(24 * 3600 / dt)
                deploy.step5["params"]['hotout_write'] = round(24 * 3600 / dt)
                deploy.step5["params"]['nstep_wwm'] = nstep_wwm
                deploy.step5["params"]['dramp'] = dramp
                deploy.save()
            
            # deltc is calculated based on the values of dt and nstep_wwm
            if 'waves' in run_type and len(deploy.step5["wwm"]["params"]) == 0:        
                deploy.step5["wwm"]["params"]['ENGS_BRHD'] = 0.78
                deploy.step5["wwm"]["params"]['PROC_DELTC'] = float(deploy.step5["params"]['dt']) * float(deploy.step5["params"]['nstep_wwm'])
                deploy.save()
            
                

            # Finally prepare message to user
            request.session['msg_str'] = str(string_concat(_('Sistema de Previsão '), deploy_id, _(' ativado com sucesso.'),
                                                           _(' Os primeiros resultados serão gerados nas próximas 24 horas, podera consultá-los acedendo ao Visualizador a partir do menu.')))

            return True, None

        except Exception:
            logger.exception('Error activating deploy')
            return False, str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    else:
        request.session['msg_str'] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))
        return False, str(_('Ocorreu um erro! Por favor tente mais tarde.'))


def delete_files(request, exclude):
    # Delete related instances in SessionFile if exist
    session = Session.objects.get(pk=request.session.session_key)
    if exclude:
        front_models.SessionFile.objects.filter(session=session).exclude(file_type__name__in=exclude).delete()
    else:
        front_models.SessionFile.objects.filter(session=session).delete()

    # Delete related instances in DeploymentFile if exist
    if 'deploy_id' in request.session:
        dep_id = request.session['deploy_id'][0]
        try:
            deploy = front_models.Deployment.objects.get(id=dep_id)
            if exclude:
                dm_models.DeploymentFile.objects.filter(deployment=deploy).exclude(file_type__name__in=exclude).delete()
            else:
                dm_models.DeploymentFile.objects.filter(deployment=deploy).delete()
        except Exception:
            pass


def fill_sessions(request, deploy):
    # Fill request session from Deploy instance
    dep_id = [deploy.id] if not deploy.clone else [deploy.id, deploy.clone]
    request.session['deploy_id'] = dep_id

    if deploy.step:
        request.session['last_step'] = deploy.step

    if deploy.step1:
        request.session['step1'] = deploy.step1
        request.session['wizard_key'] = deploy.reference

    if deploy.step2:
        request.session['step2'] = deploy.step2
    else:
        if 'step2' in request.session:
            request.session.pop('step2')

    if deploy.step3:
        request.session['step3'] = deploy.step3
    else:
        if 'step3' in request.session:
            request.session.pop('step3')

    if deploy.step4:
        request.session['step4'] = deploy.step4
    else:
        if 'step4' in request.session:
            request.session.pop('step4')

    if deploy.step5:
        request.session['step5'] = deploy.step5
    else:
        if 'step5' in request.session:
            request.session.pop('step5')

    if deploy.step6:
        request.session['step6'] = deploy.step6
    else:
        if 'step6' in request.session:
            request.session.pop('step6')

    if deploy.step7:
        request.session['step7'] = deploy.step7
    else:
        if 'step7' in request.session:
            request.session.pop('step7')

    request.session.modified = True


def compute_extent(boundaries):
    flag, extent, msg = False, [0, 0, 0, 0], None
    try:
        allgeoms, geojson_items = [], json.loads(boundaries)
        for feature in geojson_items['features']:
            b_coords = feature['geometry']['coordinates']
            allgeoms.append(LineString(b_coords))

        extent = list(GeometryCollection(allgeoms, srid=4326).extent)

    except Exception:
        logger.exception('Error computing boundaries extent')

    if extent == [0, 0, 0, 0]:
        msg = _('Erro ao processar ficheiro! Confirme se o formato é o correto.')
    elif extent_not_wgs84(extent):
        msg = _('Não foi possível a conversão de coordenadas! Verifique se selecionou o Sistema de Referência de Coordenadas correto.')
    else:
        flag = True

    return flag, extent, msg


def extent_not_wgs84(extent):
    xmin, ymin, xmax, ymax = extent
    # Since extent might exceed wgs84 boundaries [-180,-90,180,90] we validate 360
    if xmin < -360 or xmax > 360 or ymin < -90 or ymax > 90:
        return True
    return False


def create_new_file(deploy, name, constant):
    try:
        # Get deploy's hgrid ll file
        file_type_hgrid = dm_models.FileType.objects.get(name='hgrid', format='ll')
        deploy_file_hgrid = dm_models.DeploymentFile.objects.get(file_type=file_type_hgrid, deployment=deploy)
        hgrid_lines = bytes(deploy_file_hgrid.file).decode().split('\n')

        # Build new file
        new_file_content = schism.write_auxfile_fromhgrid(hgrid_lines, constant, None, name)

        # Get or Create file - no duplicates this way
        new_file_type = dm_models.FileType.objects.get(name=name, format='ll')
        new_file, created = dm_models.DeploymentFile.objects.get_or_create(file_type=new_file_type, deployment=deploy)
        new_file.file = new_file_content
        new_file.save()
    except Exception:
        logger.exception('Error creating new auxiliary file from hgrid')


def member_run_3d(user):
    # Return if User belongs to run_3d group
    return user.groups.filter(name='run_3d').exists()


def deploy_run_3d(deploy):
    # Return if deploy is 3D
    return 'run_type' in deploy.step1 and '3d' in deploy.step1['run_type']


def build_stations(boundaries, step_stations_items, run_type):
    geos_poly, shell_coords, islands_coords, lines, islands = Polygon(), [], [], [], []
    deploy_stations, stations_items, cstations, stations = [], [], (), None

    try:
        # Build Polygon from Boundaries
        boundary_objs = json.loads(boundaries)
        for obj in boundary_objs['features']:
            line = LineString(obj['geometry']['coordinates'])
            btype = obj['properties']['type']
            # If not closed or closed but of type Land
            if not line.closed or btype == 'Land':
                lines.append(line)
            else:
                islands.append(line)

        if len(lines) > 0:
            # TODO: check if lines is clock or counter clock wise
            # if counter clock add merged lines as island
            mergedlines = linemerge(lines)
            mergedlines_coords = [[elem[1], elem[0]] for elem in list(mergedlines.coords)]

            shell_coords.append(mergedlines_coords)
            geos_poly.exterior_ring = LinearRing(tuple(mergedlines.coords))
        else:
            # No outer boundaries, create multipolygon with islands and check if stations are inside of each one
            shell_coords.append([[-180, -90], [-180, 90], [180, 90], [180, -90], [-180, -90]])
            geos_poly = MultiPolygon()

            # No outer boundaries - use World to create Shell (must check if Holes crosses Shell, too long)
            # shell_coords.append([[-180, -90], [-180, 90], [180, 90], [180, -90], [-180, -90]])
            # geos_poly.exterior_ring = LinearRing((-195, -90), (-195, 90), (195, 90), (195, -90), (-195, -90)) # counting with crossing antimeridian objects

        for obj in islands:
            islands_coords_aux = [[elem[1], elem[0]] for elem in list(obj.coords)]
            # LinearRing must have at least 4 points
            # So, if only 3 exist, close using first coordinate
            if len(obj.coords) == 3:
                islands_coords_aux.append([obj.coords[0][1], obj.coords[0][0]])

            if len(islands_coords_aux) >= 4:
                try:
                    # Check if Hole crosses exterior ring (can't happen)
                    ##### Trying to avoid this check ..
                    ##### it takes ages but works by not appending islands that cross the shell geometry
                    # hole = LinearRing(tuple(obj.coords))
                    # if not hole.crosses(geos_poly):
                    ## Append as Hole
                    # geos_poly.append(hole)

                    if geos_poly.geom_type == "Polygon":
                        # Append as Hole because Shell exists
                        geos_poly.append(LinearRing(tuple(obj.coords)))
                    else:
                        # Append as Polygon when no Shell exists
                        geos_poly.append(Polygon(tuple(obj.coords)))

                    # Append to geojson returned to the template
                    islands_coords.append(islands_coords_aux)
                except Exception:
                    pass

        # Load Stations from DB that are active and inside Global BBox
        # Commented on 26/09/2018 - Global BBox no longer used
        # bboxobj = front_models.BBox.objects.filter(bbox_type=front_models.BBox.BOXTYPE_GLOBAL).values('polygon')[0]['polygon']
        # stations_queryset = front_models.Station.objects.filter(active=True).filter(point__within=bboxobj)

        # Commented on 11/01/2019 - no more using database Station Table
        #stations_queryset = front_models.Station.objects.filter(active=True)
        #stations = json.loads(serialize('geojson', stations_queryset))

        # Get today minus 1 week
        today = timezone.now().date()
        lastweek = today - timedelta(days=7)

        params = "(ParametersCodeDescr LIKE '%SLEV%'"
        if '3d' in run_type:
            params += " OR ParametersCodeDescr LIKE '%TEMP%' OR ParametersCodeDescr LIKE '%PSAL%'"
        if 'waves' in run_type:
            params += " OR ParametersCodeDescr LIKE '%SWHT%' OR ParametersCodeDescr LIKE '%SWPR%'"
        params += ")"

        # Get stations from EMODnet - Build request url
        req_link = 'http://geoserver.emodnet-physics.eu/geoserver/emodnet/ows'
        query = "{0} AND LastDataMeasured > '{1}'".format(params, lastweek.strftime("%Y/%m/%d"))
        spatial_query = ""
        # Add spatial filter if outer shell Polygon exists
        if geos_poly.geom_type == "Polygon":
            # TODO: fixing distance to grid in the future should be customizable
            # Geoserver ignores unit and uses data projection (e.g. WGS84 degrees), ignore kilometers
            #spatial_query = " AND DWITHIN(position, {0}, {1}, {2})".format(geos_poly.ogr, 0.08, 'kilometers')
            # Better to use BBOX instead of DWITHIN - quicker and bigger chance to catch more stations
            spatial_query = " AND BBOX(position, {0})".format(str(geos_poly.extent).strip().replace("(", "").replace(")", ""))

        parms = {
            'version': "1.0.0",
            'service': "wfs",
            'request': "GetFeature",
            'typeName': "emodnet:EP_PLATFORMS",
            'outputFormat': "application/json",
            'CQL_FILTER': query + spatial_query
        }
        # If we use geos_poly.ogr instead of geos_poly.extent we need to use 'post'
        # instead of 'get' (get returns 'Request-URI Too Long') - too many coordinates
        try:
            #stations = requests.post(req_link, params=parms, timeout=15).json()["features"]
            # Using BBOX Spatial filter we can use the get method instead
            stations = requests.get(req_link, params=parms, timeout=15).json()["features"]
        except Exception:
            stations = []
            logger.debug('Error or Timeout on EMODnet Geoserver GetFeature request')
            pass

        # Process each station
        for station in stations:
            # Create point geometry
            lat = round(station['geometry']['coordinates'][1], 5)
            lon = round(station['geometry']['coordinates'][0], 5)
            point = Point(x=lon, y=lat, srid=4326)

            # Read relevant information
            strlat = "%.5f" % lat
            strlon = "%.5f" % lon
            id = str(station['properties']['PlatformID'])
            name = station['properties']['PlatformCode']
            cstation = str(string_concat(name, ' (', strlat, ', ', strlon, ')'))
            source = 'EMODnet'
            owner = station['properties']['DataOwner']
            params = station['properties']['ParametersCodeDescr']
            paramsgroup = station['properties']['ParametersGroupDescr']
            lastdate = station['properties']['LastDataMeasured']

            # Reduce coordinates precision
            station['geometry']['coordinates'][1] = strlat
            station['geometry']['coordinates'][0] = strlon

            # Delete irrelevant info
            del station["id"]

            # Replace properties
            station['properties'] = {
                'type': "O",
                'code': "O_" + id,
                'source': "EMODnet",
                'owner': owner,
                'name': name,
                'params': params,
                'paramsgroup': paramsgroup,
                'lastdate': lastdate
            }

            # If Shell exists and station inside geos_poly or if no Shell and station outside geos_poly
            if (len(lines) > 0 and geos_poly.contains(point)) or (
                    len(lines) == 0 and not point_in_geom(geos_poly, point)):
                # Mark station as being inside grid
                station['properties']['inbbox'] = 1

                if not step_stations_items:
                    # Step Stations not defined yet
                    stations_items.append({
                        "chk": False,
                        "code": 'C_' + id,
                        "type": 'C',
                        "name": name,
                        "lat": strlat,
                        "lon": strlon,
                        "cstation": cstation
                    })

                    # Duplicate observation station as comparison station
                    pnt = geojson_Point((lon, lat))
                    properties = {
                        "chk": 0,
                        "code": 'C_' + id,
                        "type": 'C',
                        "name": name,
                        "cstation": cstation,
                        "source": source,
                        "owner": owner,
                        "params": params,
                        "paramsgroup": paramsgroup,
                        "lastdate": lastdate
                    }
                    deploy_stations.append(Feature(geometry=pnt, properties=properties))
                else:
                    # Check if this station already in the table
                    saved_station = False
                    for item in step_stations_items:
                        if item['code'] == 'C_' + id:
                            saved_station = True

                    if not saved_station:
                        stations_items.append({
                            "chk": False,
                            "code": 'C_' + id,
                            "type": 'C',
                            "name": name,
                            "lat": strlat,
                            "lon": strlon,
                            "cstation": cstation
                        })
            else:
                station['properties']['inbbox'] = 0

            # Build tuple choices of observation stations for form select field
            cstations = (('O_' + id, cstation),) + cstations

        # Add no value to tuple choices
        cstations = (('', '-----'),) + cstations

        # Finally add saved stations to Stations geojson and table
        if step_stations_items:
            stations_items = step_stations_items + stations_items

            for item in step_stations_items:
                pnt = geojson_Point((float(item['lon']), float(item['lat'])))
                properties = {
                    "chk": item['chk'],
                    "code": item['code'],
                    'type': item['type'],
                    "name": item['name'],
                    "cstation": item['cstation']
                }

                if item['type'] == 'C':
                    properties['source'] = item['source']
                    properties['owner'] = item['owner']
                    properties['params'] = item['params']
                    # These properties were added latter on
                    if 'paramsgroup' in item:
                        properties['paramsgroup'] = item['paramsgroup']
                    if 'lastdate' in item:
                        properties['lastdate'] = item['lastdate']

                deploy_stations.append(Feature(geometry=pnt, properties=properties))

        # Concatenate observation stations with other types of stations
        stations += deploy_stations

        return True, stations, cstations, shell_coords, islands_coords, stations_items

    except Exception:
        logger.exception('Error building stations elements for rendering')
        return False, stations, cstations, shell_coords, islands_coords, stations_items


def point_in_geom(geom, point):
    # Returns True or False point inside geometry
    for obj in geom:
        if obj.contains(point):
            return True
    return False


def get_auxfile_bk(deploy_id, file_id):
    # Returns auxiliary file content - built specifically for backend
    content = None

    try:
        # Get Deployment
        deploy = front_models.Deployment.objects.get(id=deploy_id)

        # Get hgrid file content
        file_type = dm_models.FileType.objects.get(name='hgrid', format='ll')
        hgrid_file = dm_models.DeploymentFile.objects.get(file_type=file_type, deployment=deploy)
        hgrid_content = bytes(hgrid_file.file)

        # Get constant or array from step6
        for item in deploy.step6["auxfilesset"]:
            if item["name"].lower() == file_id:
                # Replace content
                if item["choice"] == "C":
                    content = schism.write_auxfile_fromhgrid(hgrid_content, item["constant"], file_id, True)
                elif item["choice"] == "U":
                    content = schism.write_auxfile_fromhgrid(hgrid_content, item["array"], file_id, True)

        # Windrot is not added to the auxfileset, so when called pass grid with zeros
        if file_id == 'windrot':
            content = schism.write_auxfile_fromhgrid(hgrid_content, 0, file_id, True)

    except Exception:
        logger.exception('Error getting auxiliary file for backend')
        content = None

    return content


def get_item_code(tuple, label):
    # Returns code of tuple from label

    for item in tuple:
        if item[1] == label:
            return item[0]
    return label


def row_with_comment(text):
    row_text = str(text.split("!")[0]).rstrip().rstrip(',')
    row_comment = str(text.split("!")[1])
    if len(row_comment) > 1:
        row = "{} <span class='label label-comment'>{}</span>".format(row_text, row_comment)
    else:
        row = row_text
    return row


def parse_param(template_name, params):
    par_template = get_template(template_name)
    paramin = par_template.render(params)

    # Split file by new lines and add as table rows
    # Main comments are used to group rows
    parfile = paramin.split('\n')
    pars, comment, new_comment = [], '', True
    for row in parfile:
        if len(row) > 0:
            if '!------' in row and new_comment:
                comment = ''
                new_comment = False
            elif '!------' in row and not new_comment:
                new_comment = True
            else:
                if '!' not in row[:3]:
                    if '!' in row:
                        row = row_with_comment(row)

                    pars.append({'comment': mark_safe(comment), 'row': mark_safe(row)})
                    new_comment = True
                else:
                    if not new_comment:
                        comment += row[2:] + '<br/>'
    return pars


def parse_wwminput(template_name, params_wwm):
    wwminput_template = get_template(template_name)
    wwminput = wwminput_template.render(params_wwm)

    # Split file by new lines and add as table rows
    # Main comments are used to group rows
    wwminputfile = wwminput.split('\n')
    pars, comment, row_comment, new_comment = [], '', '', True
    for i, row in enumerate(wwminputfile):
        if len(row) > 0 and i > 3:
            row = row.lstrip()
            if row[:1] == '&' and new_comment:
                if '!' in row:
                    row = row_with_comment(row)

                comment += row[1:] + '<br/>'
            elif row[:1] == '/':
                comment = ''
                new_comment = True
            else:
                if row[:1] != '!':
                    if len(row_comment) > 0:
                        # This file has comments for rows after the row
                        # So we compile them and add them to the last added row
                        last_row = pars[-1]["row"]
                        last_row_comment = pars[-1]["comment"]
                        if 'span' in last_row:
                            # Append to comment
                            last_row = last_row.replace('</span>', "{}</span>".format(row_comment))
                        else:
                            # Add a comment
                            last_row = "{} <span class='label label-comment'>{}</span>".format(last_row, row_comment)
                        pars[-1] = {'comment': mark_safe(last_row_comment), 'row': mark_safe(last_row)}
                        # Clear row comment
                        row_comment = ''

                    if '!' in row:
                        row = row_with_comment(row)
                    else:
                        row = row.rstrip().rstrip(',')

                    pars.append({'comment': mark_safe(comment), 'row': mark_safe(row)})
                    new_comment = False
                else:
                    # Comments to append to last element
                    row_comment += ' ' + row[1:]
    return pars


'''
def geojson2boundaries(geojson_items):
    # Convert geojson to Boundaries
    boundaries = []
    for feature in geojson_items['features']:
        b_type = feature['properties']['type']
        b_code = feature['properties']['code']
        b_coords = feature['geometry']['coordinates']
        boundaries.append({'type': b_type, 'code': b_code, 'line': b_coords})

    return boundaries
    
    
def boundaries2geojson(objs, open=False):
    # Convert Boundaries to geojson
    features = []
    for obj in objs:
        line = geojson_LineString(obj.line)
        properties = {
            "bid": obj.code,
            "btype": obj.type,
        }
        if not open or (open and obj.type == 'Open'):
            features.append(Feature(geometry=line, properties=properties))

    if len(features) == 0:
        return None

    feature_collection = FeatureCollection(features)
    # Create geojson
    geojson_obj = geojson_dumps(feature_collection)
    return geojson_obj
'''