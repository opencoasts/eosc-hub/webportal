import argparse
import gdal
import json
import logging
import os
import shutil
import subprocess
import traceback
from datetime import date, timedelta
from multiprocessing import Pool, cpu_count
from pathlib import Path
from PIL import Image
from osgeo import gdal

import django
django.setup()

from django.core.files import File

import front.models
import raster.models
import satellite.models

LOGGING_FILE_LEVEL = logging.DEBUG
LOGGING_CONSOLE_LEVEL = logging.DEBUG

LOGGING_FMT = '%(asctime)s %(levelname)s' \
                  ' %(name)s # %(module)s.%(funcName)s:%(lineno)s "%(message)s"'

logger = logging.getLogger(__name__)

SOURCE_FOLDER_PATH = ''
BASE_IMAGE_PATH = ''
IMAGE_FOLDER_PATH = ''

#source_dir = '/home/morocha/OPENCoastS'
#base_dir = /home/morocha/OPENCoastS/wizard/utilities/
#IMAGE_FOLDER_PATH = '/home/morocha/OPENCoastS/wizard/utilities/satellite'

def generate_bbox_from_wkt(wkt_str):
    #bbox = -9.28 38.68 -9.17 38.57
    bbox = [None,None,None,None]
    wkt = wkt_str.replace('POLYGON','').replace(' ((','').replace(')) ','').replace('((','').replace('))','') #even is there's a space between polygon and parethesis
    wkt_split = wkt.split(',')
    print(wkt_split)
    for w in wkt_split:
        latlon = w.split(' ')
        if bbox[0] is None and bbox[2] is None:
            bbox[0] = bbox[2] = float(latlon[0])
            bbox[1] = bbox[3] = float(latlon[1])
        else:
            bbox[0] = min(bbox[0],float(latlon[0]))
            bbox[1] = max(bbox[1],float(latlon[1]))
            bbox[2] = max(bbox[2],float(latlon[0]))
            bbox[3] = min(bbox[3],float(latlon[1]))
    print(bbox)
    return bbox

def worsicaResampling(procDict):
    try:
        imgZip = procDict['imgZip']
        print(imgZip)
        projWinArgs = procDict['projWinArgs']

        #ACTUAL_PATH = os.getcwd()
        if('path' in procDict):
            ACTUAL_PATH = procDict['path']
        else:
            ACTUAL_PATH = os.getcwd()

        #print('ACTUAL_PATH')
        print(ACTUAL_PATH)
        #print(imgZip)
        #print(projWinArgs)

        imgZip = ACTUAL_PATH + imgZip

        print("Sou o zip:" + imgZip)

        #print('worsicaResampling')
        os.system(f"unzip -u {imgZip} -d {ACTUAL_PATH}")
        fileLista = []
        for root, dirs, files in os.walk(f"{imgZip[:-4]}.SAFE", topdown=True):
            for name in files:
                fileLista.append(os.path.join(root, name))
            for name in dirs:
                fileLista.append(os.path.join(root, name))

        l10JP2 = [i for i in fileLista if (i.endswith("B02_10m.jp2") or i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2") or i.endswith("B08_10m.jp2"))]
        # l10JP2 = [i for i in fileLista if (i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2") or i.endswith("B08_10m.jp2"))]
        l20JP2 = [i for i in fileLista if i.endswith("B11_20m.jp2") or i.endswith("B12_20m.jp2")]
        l1020 = l10JP2 + l20JP2

        BandsRGB = {}
        foutFiles = []
        for img in l1020:
            #fout = "".join((img.split("SAFE")[0][:-1], img.split("/")[-1][-12:-4], ".tif"))
            fout = "".join((ACTUAL_PATH + "auxResample/", img.split("SAFE")[0][:-1].split("/")[-1], img.split("/")[-1][-12:-4], ".tif"))
            #fout2 = "resampled/" + "_".join((img.split("SAFE")[0][:-1], "resampled.tif"))
            fout2 = ACTUAL_PATH + "resampled/" + "_".join((img.split("SAFE")[0][:-1].split("/")[-1], "resampled.tif"))
            print("FOUT2: ", fout2)
            foutFiles.append(fout)

            ds = gdal.Open(img)
            band = ds.GetRasterBand(1)
            arr = band.ReadAsArray()
            [cols, rows] = arr.shape
            # ncols, nrows = a.XSize, a.YSize
            Metadata = ds.GetMetadata()
            GCPs = ds.GetGCPs()
            GCPsProj = ds.GetGCPProjection()
            geotransform = ds.GetGeoTransform()
            Description = ds.GetDescription()

            gdal.Translate(fout, ds, format="GTiff", resampleAlg='cubic', xRes=10., yRes=10., outputType=gdal.GDT_Float32, projWinSRS = projWinArgs['projWinSRS'], projWin = projWinArgs['projWin'])
            ds = None

            if "B02_10m" in fout:
                BandsRGB["B"] = fout
            elif "B03_10m" in fout:
                BandsRGB["G"] = fout
            elif "B04_10m" in fout:
                BandsRGB["R"] = fout
            elif "B08_10m" in fout:
                BandsRGB["NIR"] = fout
            elif "B11_20m" in fout:
                BandsRGB["SWIR1"] = fout
            elif "B12_20m" in fout:
                BandsRGB["SWIR2"] = fout

        vrt_options = gdal.BuildVRTOptions(separate=True, hideNodata=True)
        gdal.BuildVRT(ACTUAL_PATH+'auxResample/aux.vrt', [BandsRGB['B'], BandsRGB['G'], BandsRGB['R'], BandsRGB['NIR'], BandsRGB['SWIR1'], BandsRGB['SWIR2']], options=vrt_options)
        ds2 = gdal.Open(ACTUAL_PATH+'auxResample/aux.vrt')
        ds2 = gdal.Translate(fout2, ds2)
        ds2 = None

        dsOut = gdal.Open(fout2)
        dsOut.SetMetadata(Metadata)
        dsOut.SetDescription(Description)
        dsOut.GetRasterBand(1).SetDescription('Blue')
        dsOut.GetRasterBand(2).SetDescription('Green')
        dsOut.GetRasterBand(3).SetDescription('Red')
        dsOut.GetRasterBand(4).SetDescription('NIR')
        dsOut.GetRasterBand(5).SetDescription('SWIR1')
        dsOut.GetRasterBand(6).SetDescription('SWIR2')
        dsOut = None

        worsicaRGB(fileLista, ACTUAL_PATH)

        foutFiles.append(ACTUAL_PATH+"auxResample/aux.vrt")
        for trash in foutFiles:
            os.remove(trash)
        fDelete = imgZip.replace(".zip", ".SAFE")
        shutil.rmtree(f"{fDelete}")
        print(f"{fout2} :: Done!!!")
        return None
    except:
        traceback.print_exc()


def worsicaRGB(fileLista, actual_path):
    try:
        l10JP2 = [i for i in fileLista if (i.endswith("B02_10m.jp2") or i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2"))]
        # l10JP2 = [i for i in fileLista if (i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2") or i.endswith("B08_10m.jp2"))]
        l1020 = l10JP2

        BandsRGB = {}
        pathAux = Path(actual_path) / 'auxResample'
        pathAux.mkdir(exist_ok=True)
        for img in l1020:
            fout = "".join((str(pathAux),"/", img.split("SAFE")[0][:-1].split("/")[-1], img.split("/")[-1][-12:-4], ".tif"))
            fout2 = actual_path+"/" + "_".join((img.split("SAFE")[0][:-1].split("/")[-1], "RGB.tif"))

            #that comes from the zip, we are not using from the zip
            #ds = gdal.Open(imgZip)
            #a = ds.GetRasterBand(1)
            #Metadata = ds.GetMetadata()
            #ncols, nrows = a.XSize, a.YSize
            #GCPs = ds.GetGCPs()
            #GCPsProj = ds.GetGCPProjection()
            #geotransform = ds.GetGeoTransform()
            #Description = ds.GetDescription()

            #gdal.Translate(fout, ds, format="GTiff", resampleAlg='cubic', xRes=10., yRes=10., outputType=gdal.GDT_Float32)
            #ds = None

            if "B02_10m" in fout:
                BandsRGB["B"] = fout
            elif "B03_10m" in fout:
                BandsRGB["G"] = fout
            elif "B04_10m" in fout:
                BandsRGB["R"] = fout

        vrt_options = gdal.BuildVRTOptions(separate=True, hideNodata=True)
        gdal.BuildVRT(actual_path+'auxRGB/auxRGB.vrt', [BandsRGB['R'], BandsRGB['G'], BandsRGB['B']], options=vrt_options)
        ds2 = gdal.Open(actual_path+'auxRGB/auxRGB.vrt')
        ds2 = gdal.Translate(fout2, ds2, outputType=gdal.GDT_Float32)
        ds2 = None

        # dsOut = gdal.Open(fout2)
        # dsOut.SetMetadata(Metadata)
        # dsOut.SetDescription(Description)
        # dsOut.GetRasterBand(1).SetDescription('B2')
        # dsOut.GetRasterBand(2).SetDescription('B3')
        # dsOut.GetRasterBand(3).SetDescription('B4')
        # dsOut = None

        #fDelete = imgZip.replace(".zip", ".SAFE")
        #shutil.rmtree(pathAux)
        #shutil.rmtree(f"{fDelete}")
        print(f"{fout2} :: Done!!!")
        #return None
    except:
        traceback.print_exc()

def run_resample_rgb(procDicts):
    try:
        print('run_resample_rgb')

        for procDict in procDicts:
            if('path' in procDict):
                ACTUAL_PATH = procDict['path']
            else:
                ACTUAL_PATH = os.getcwd()

            print('ACTUAL_PATH')
            print(ACTUAL_PATH)

            if not os.path.exists(ACTUAL_PATH+'resampled'):
                os.makedirs(ACTUAL_PATH+'resampled', exist_ok=True)
            if not os.path.exists(ACTUAL_PATH+'auxResample'):
                os.makedirs(ACTUAL_PATH+'auxResample', exist_ok=True)
            if not os.path.exists(ACTUAL_PATH+'auxRGB'):
                os.makedirs(ACTUAL_PATH+'auxRGB', exist_ok=True)

        #print(procDict)
        pool = Pool(cpu_count())
        results = pool.map_async(worsicaResampling, procDicts)
        pool.close()
        pool.join()
        return None
    except:
        traceback.print_exc()

def tifToJPG():
    for infile in os.listdir("./"):
        print("file : " + infile)
        if infile[-3:] == "tif" or infile[-3:] == "bmp" :
            #    print "is tif or bmp"
            outfile = infile[:-3] + "jpeg"
            im = Image.open(infile)
            print("new filename : " + outfile)
            out = im.convert("RGB")
            out.save(outfile, "JPEG", quality=90)

def loadSatelliteRasters(deploy_id):
    try:
        print("ENTREI")
        count = 0
        file_names = os.listdir(IMAGE_FOLDER_PATH)
        for file_name in file_names:
            if ".tif" in file_name:
                print(file_name)
                correct_name = file_name.split(".")[0] + "_" + str(count) + ".tif"
                nameRl = correct_name
                count += 1
                wrapped_file = open(IMAGE_FOLDER_PATH + "/" + file_name, 'rb')
                print('[startProcessing _store_raster] File '+ file_name +' found, store it')
                try:
                    print('[startProcessing _store_raster] check Rasterlayer ' + nameRl)
                    rl = raster.models.RasterLayer.objects.get(name=nameRl)
                    print('[startProcessing _store_raster] found it, delete')
                    rl.delete()
                    print('[startProcessing _store_raster] create again')
                    rl = raster.models.RasterLayer.objects.create(name=nameRl)
                except Exception as e:
                    print('[startProcessing _store_raster] not found, create')
                    rl = raster.models.RasterLayer.objects.create(name=nameRl)

                rl.rasterfile.save('new', File(wrapped_file))
                rl.save()
                wrapped_file.close()
                wrapped_file = None

                print('[startProcessing _store_raster] storing done (id='+str(rl.id)+')')
                file_name_split = file_name.split("_")
                date_split = file_name_split[6]

                year = date_split[0:4]
                month = date_split[4:6]
                day = date_split[6:8]

                date = year + "-" + month + "-" + day
                print(date)

                threshold_file = IMAGE_FOLDER_PATH + "/" + file_name.split(".")[0] + ".txt"
                f = open(threshold_file, "r")
                lines = f.read().splitlines()

                #img_name = correct_name.split(".")[0]
                #img_name = img_name.split("_")[0:7]
                #img_name_correct = ""
                #for k in range(0,7):
                #    img_name_correct += img_name[k] + "_"
                #print(img_name_correct)

                #img = '/satellites/' + img_name_correct + str(count) + ".png"

                deploy = front.models.Deployment.objects.get(pk=deploy_id)
                satRaster, _ = satellite.models.SatelliteRaster.objects.get_or_create(raster_name = nameRl, raster_date = date, raster_threshold = lines[0], raster_deployment=deploy)

                satRaster.rasterLayer = rl
                satRaster.save()

                f.close()

                del satRaster, rl, wrapped_file

                print('Done!')

    except Exception as e:
        print('Stop! Errors found!')
        print(e.with_traceback)

def processImages(xmin, ymax, xmax, ymin, deploy_id):

    print("entrei aqui")

    #base_dir = '/srv/www/mosaic/web-portal/'
    #base_dir = '/home/morocha/OPENCoastS/'

    #source_dir = '/srv/www/mosaic/web-portal/utils/satellite/'
    #source_dir = base_dir + 'wizard/utilities/'
    #coordinates_dir = source_dir + "satellite/"

    #target_dir = base_dir + '/media/viewer/satellites/'

    today = date.today()
    yesterday = today - timedelta(days=5)
    print(yesterday)
    correct_date = yesterday.strftime("%Y%m%d")

    #-8.77 40.625
    #-8.74 40.625
    #-8.74 40.59
    #-8.77 40.59

    line1 = str(xmin) + " " + str(ymax) + "\n"
    line2 = str(xmax) + " " + str(ymax) + "\n"
    line3 = str(xmax) + " " + str(ymin) + "\n"
    line4 = str(xmin) + " " + str(ymin) + "\n"
    f = open(IMAGE_FOLDER_PATH +  "Coordinates.txt", "w+")
    f.write(str(line1))
    f.write(str(line2))
    f.write(str(line3))
    f.write(str(line4))
    f.close()

    #create file
    coord_path = IMAGE_FOLDER_PATH + "Coordinates.txt"
    #print(coord_path)
    subprocess.run(["python", "-m", "satellite.utilities.worsica_ImgDownloads", "-i" , correct_date, "-f", "NOW", "-s", "2", "-l", "2" , "-p", coord_path , "-c", "10", "-d", "-rgb", "-sou", SOURCE_FOLDER_PATH, "-b", BASE_IMAGE_PATH], stdout=subprocess.DEVNULL)

    #POLYGON ((-8.8806 40.1529,-8.8541 40.1475,-8.8597 40.1182,-8.8795 40.1213,-8.8806 40.1529))

    #polygon = f'POLYGON (({xmin} {ymax}, {xmax} {ymax}, {xmax} {ymin}, {xmin} {ymin}))'
    polygon = 'POLYGON ((' + str(xmin) + " " + str(ymax) + "," + str(xmax) + " " + str(ymax) + "," + str(xmax) + " " + str(ymin) + "," + str(xmin) + " " + str(ymin) + "))"

    file_names = os.listdir(BASE_IMAGE_PATH)
    #wkt_path = path_to_wkts + observatories[i] + ".wkt"
    #file_wkt = open(wkt_path, "r")
    #polygon = file_wkt.readline()
    #file_wkt.close()
    bbox = generate_bbox_from_wkt(polygon)

    #print(file_names)

    for file_name in file_names:
        print(file_name)
        if ".zip" in file_name:
            print("entrei no if")
            print(file_name)
            print(bbox)
            procDict = [{'imgZip': file_name, 'path': BASE_IMAGE_PATH, 'projWinArgs': {'projWinSRS': 'EPSG:4326', 'projWin': bbox} }]
            run_resample_rgb(procDict)
            images = os.listdir(BASE_IMAGE_PATH + "resampled")

            for img in images:
                if ".tif" in img and ".xml" not in img:
                    print(img)
                    shutil.move(os.path.join(BASE_IMAGE_PATH + "resampled/" + img), BASE_IMAGE_PATH)

            #print("cheguei aqui")

            image_file = BASE_IMAGE_PATH + file_name.split(".")[0] + "_resampled.tif"
            rgb_image_file = BASE_IMAGE_PATH + file_name.split(".")[0] + "_RGB.tif"
            print(image_file)
            subprocess.run(["python", "-m", "satellite.utilities.WaterIndex_AA", "--image", image_file, "-i", BASE_IMAGE_PATH], stdout=subprocess.DEVNULL)
            ndwi_image_file = BASE_IMAGE_PATH + file_name.split(".")[0] + "_resampled_NDWI.tif"

            txt_file = BASE_IMAGE_PATH + file_name.split(".")[0] + "_resampled_NDWI.txt"
            #print("TESTE: " + source_dir + observatories[i])
            if os.path.exists(ndwi_image_file):
                shutil.move(os.path.join(ndwi_image_file), IMAGE_FOLDER_PATH)
                shutil.move(os.path.join(txt_file), IMAGE_FOLDER_PATH)

            #options_list = [
            #    '-ot Byte',
            #    '-of PNG',
            #    '-b 1',
            #    '-scale'
            #]

            #options_string = " ".join(options_list)

            #gdal.Translate(
            #    target_dir + file_name.split(".")[0] + "_" + observatories[i] + ".png",
            #    rgb_image_file,
            #    options=options_string
            #)

            os.remove(BASE_IMAGE_PATH + file_name)
            os.remove(rgb_image_file)
            os.remove(image_file)
            shutil.rmtree(BASE_IMAGE_PATH + "auxResample")
            shutil.rmtree(BASE_IMAGE_PATH + "auxRGB")
            shutil.rmtree(BASE_IMAGE_PATH + "resampled")

    #subprocess.run(["python", source_dir + "create_satellite_rasters.py", "-id", deploy_id], stdout=subprocess.DEVNULL)
    loadSatelliteRasters(deploy_id)

def getARGScmd():
    parser = argparse.ArgumentParser(description='worsica_ImgDownloads: Script to search and download data from satellites sentinel 1/2')
    parser.add_argument('-s', '--source', help='Source image folder.', required=True)
    parser.add_argument('-b', '--base', help='Base image folder.', required=True)
    parser.add_argument('-i', '--path', help='Final path of the input image.', required=True)
    # parser.add_argument('-t', '--threads', help='Thread count.')
    args = parser.parse_args()
    return args

if __name__ == '__main__':

    args = getARGScmd()

    SOURCE_FOLDER_PATH = args.source
    BASE_IMAGE_PATH = args.base
    IMAGE_FOLDER_PATH = args.path

    print(SOURCE_FOLDER_PATH)
    print(BASE_IMAGE_PATH)
    print(IMAGE_FOLDER_PATH)

    #sys.path.append(OPENCOASTS_FOLDER_PATH+"/ProjOpenCoastS")
    #os.environ['DJANGO_SETTINGS_MODULE'] = 'ProjOpenCoastS.settings_local'
    #os.environ['DJANGO_SETTINGS_MODULE'] = 'ProjOpenCoastS.settings'
    #print(os.environ['DJANGO_SETTINGS_MODULE'])
    #os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ProjOpenCoastS.settings")
    #os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ProjOpenCoastS.settings")
    #print(os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ProjOpenCoastS.settings"))
    #print(os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ProjOpenCoastS.settings"))

    #logger.debug('run date: %s', run_date)

    deployment_set = (
        front.models.Deployment.objects
        .filter(deleted = False)
        .filter(active = True)
        .order_by('id')
    )
    logger.info('deployment set: %s', deployment_set)

    for deployment in deployment_set:
        step2_inter = json.dumps(deployment.step2)
        step2_json = json.loads(step2_inter)

        try:
            processImages(step2_json['xmin'], step2_json['ymax'],
                step2_json['xmax'], step2_json['ymin'], deployment.id)
        except KeyError:
            pass
