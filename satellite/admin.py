from django.contrib import admin

from .models import *


class SatelliteRasterAdmin(admin.ModelAdmin):
    list_display = (
		'raster',
		'raster_name',
        'raster_date',
        'raster_observatory',
        'raster_threshold',
	)

admin.site.register(SatelliteRaster, SatelliteRasterAdmin)

