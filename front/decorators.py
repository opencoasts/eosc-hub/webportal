from django.utils.functional import wraps
from front.models import Deployment
from django.shortcuts import get_object_or_404
from django.http import Http404


def deploy_permission(view):
    # Custom decorator that checks user's permission to access the view with a certain deployment as parameter
    @wraps(view)
    def inner(request, deploy_id, *args, **kwargs):
        # deploy_id == 0 means use request.sessions (not error or lack of permissions)
        if int(deploy_id) == 0:
            return view(request, deploy_id, *args, **kwargs)
        else:
            deploy = get_object_or_404(Deployment, id=deploy_id)

            # For now only deployment owner or superuser can access view
            if deploy.user == request.user or request.user.is_staff:
                return view(request, deploy_id, *args, **kwargs)
            else:
                raise Http404()

    return inner
