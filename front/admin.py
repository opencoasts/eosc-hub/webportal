from django.contrib import admin
from .models import *
from front.forms import StationForm, BBoxForm

from django.contrib.postgres import fields
from django_json_widget.widgets import JSONEditorWidget

from django.core.urlresolvers import reverse


class SessionFileAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'session',
        'owner',
        'file_type',
        'file_size_mb',
        'file_link',
    )
    raw_id_fields = ('session', 'file_type')
    list_per_page = 10

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super(SessionFileAdmin, self).get_queryset(request)
        qs = qs.defer('file')
        return qs

    def owner(self, obj):
        # Get session's username
        session = Session.objects.get(session_key=obj.session.session_key)
        user = User.objects.get(pk=session.get_decoded().get('_auth_user_id'))
        return user.username

    def file_link(self, obj):
        # Return file as download link
        url = reverse('front:download_file', kwargs={'file_id': obj.id, 'file_cat': "session"})
        return u'<a href="%s">download</a>' % url

    file_link.allow_tags = True


class DeploymentAdmin(admin.ModelAdmin):
    list_editable = (
     )
    list_display = (
        'id',
        'clone',
        'user',
        'model_version_period',
        'creation_datetime',
        'step',
        'active',
        'begin_date',
        'end_date',
        'deleted',
        'name',
    ) + list_editable
    list_per_page = 10
    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget},
    }


class ParameterAdmin(admin.ModelAdmin):
    list_editable = (
        'active',
        'predefined',
        'readonly',
     )
    list_display = (
        'name',
        'description',
        'category',
        'default',
        'order_by',
    ) + list_editable
    list_filter = (
        'category',
    )
    ordering = ('order_by',)
    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget},
    }


class ModelVersionAdmin(admin.ModelAdmin):
    list_editable = (
        'public',
     )
    list_display = (
        'parent_model',
        'parent_version',
    ) + list_editable


class ForcingSourceAdmin(admin.ModelAdmin):
    list_editable = (
        'active',
    )

    list_display = (
        'parent_name',
        'forcing_kind',
    ) + list_editable


class ExtendDeployRequestAdmin(admin.ModelAdmin):
    list_editable = (
        'accepted',
        'closed',
    )
    list_display = (
        'deployment',
        'creation_datetime',
        'reply_date',
    ) + list_editable


class StationFormAdmin(admin.ModelAdmin):
    form = StationForm

    list_editable = (
        'active',
    )
    list_display = (
        'code',
        'source',
        'owner',
        'name'
    ) + list_editable


class BBoxFormAdmin(admin.ModelAdmin):
    form = BBoxForm

    list_display = (
        'bbox_type',
        'forcing_source',
    )


class ProfileAdmin(admin.ModelAdmin):
    change_list_template = 'front/user_profile.html'

    list_editable = (
        'deploy_period',
        'deploy_maxnr',
        'station_maxnr',
        'node_maxnr',
    )
    list_display = (
        'username',
        'first_name',
        'last_name',
        'affiliation',
        'affiliation_country',
        'confirm_registration',
        'is_active'
    ) + list_editable

    def username(self, obj):
        # Truncating long emails (e.g. EGI users)
        max_length = 20
        if len(obj.user.username) > max_length:
            return "%s..." % obj.user.username[:max_length]
        return obj.user.username

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}

        if 'activate_new_user' in request.session:
            extra_context['new_user'] = request.session['activate_new_user']
            request.session.pop('activate_new_user')

        if 'activate_msg' in request.session:
            extra_context['msg'] = request.session['activate_msg']
            request.session.pop('activate_msg')

        return super().changelist_view(
            request,
            extra_context=extra_context
        )


admin.site.register(UserProfile, ProfileAdmin)
admin.site.register(Station, StationFormAdmin)
admin.site.register(BBox, BBoxFormAdmin)

admin.site.register(Deployment, DeploymentAdmin)
admin.site.register(ModelParameter, ParameterAdmin)
admin.site.register(ModelVersionParameter, ParameterAdmin)
admin.site.register(ModelVersionParameter3D, ParameterAdmin)
admin.site.register(ModelVersionParameterWave, ParameterAdmin)
admin.site.register(ModelVersion, ModelVersionAdmin)
admin.site.register(ForcingSource, ForcingSourceAdmin)
admin.site.register(ExtendDeployRequest, ExtendDeployRequestAdmin)
admin.site.register(SessionFile, SessionFileAdmin)
admin.site.register(CRS)
admin.site.register(CRSV)
admin.site.register(BoundaryKind)
admin.site.register(UserProfileRating)
