from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.formats import date_format
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail

from django.contrib.gis.gdal import OGRGeometry

from .models import UserProfileRating
from .forms import RATE_TYPE

logger = settings.LOGGER.getChild('front')


# Send email notifications
def notify_users(send_to, subject, dic_pars, plain_template, html_template):
    # send_to: array of emails
    # subject: email subject
    # dic_pars: passes dictionary to template
    # plain_template: link to plain text template
    # html_template: link to html template

    try:
        # Creates dic if it's None
        if dic_pars == None:
            dic_pars = dict()

        # Add time to dic_pars
        dic_pars.update(datetime_now=timezone.now())

        # Add domain to dic_pars
        dic_pars.update(website_name=settings.WEBSITE_NAME)

        # Send email
        plain_content = render_to_string(plain_template, dic_pars)
        html_content = render_to_string(html_template, dic_pars)

        if settings.NOTIFICATIONS_ENABLED:
            send_mail(
                '[OPENCoastS] ' + subject,
                plain_content,
                '',
                send_to,
                html_message=html_content,
                fail_silently=False
            )
        else:
            logger.info('EMAIL NOT SENT: (notifications disabled in settings): %s %s \n %s', str(send_to), subject, plain_content)

    except Exception:
        logger.exception('Error sending email')

# Send email notification to User on OPENCoastS registration with confirmation link
def notify_registration(new_user, new_profile, confirmation_link):
    # Prepare email parameters
    pars = {
        'affiliation': new_profile.affiliation,
        'affiliation_country': new_profile.affiliation_country,
        'first_name': new_user.first_name,
        'last_name': new_user.last_name,
        'email': new_user.email,
        'link': confirmation_link
    }

    # Send email to user
    subject = str(_('Confirmar Registo'))
    notify_users([new_user.email], subject, pars,
                 'front/emails/register_confirm.txt', 'front/emails/register_confirm.html')


# Send email notification to Managers and to User on user's EGI Check-in registration
def notify_registration_egi(new_user, new_profile, activation_link):
    # Prepare email to notify admin
    pars = {
        'affiliation': new_profile.affiliation,
        'affiliation_country': new_profile.affiliation_country,
        'first_name': new_user.first_name,
        'last_name': new_user.last_name,
        'email': new_user.email,
        'link': activation_link
    }

    # Send email to Managers
    subject = str(_('Registo'))
    notify_users(settings.MANAGERS, subject, pars,
                 'front/emails/register_followup.txt', 'front/emails/register_followup.html')

    # Send email to user
    notify_users([new_user.email], subject, pars,
                 'front/emails/register.txt', 'front/emails/register.html')


# Send email notification to Managers on user's OPENCoastS registration
def notify_confirm_registration(new_user, new_profile, activation_link):
    # Prepare email parameters
    pars = {
        'affiliation': new_profile.affiliation,
        'affiliation_country': new_profile.affiliation_country,
        'first_name': new_user.first_name,
        'last_name': new_user.last_name,
        'email': new_user.email,
        'link': activation_link
    }

    # Send email to user
    subject = str(_('Registo'))
    notify_users(settings.MANAGERS, subject, pars,
                 'front/emails/register_followup.txt', 'front/emails/register_followup.html')


# Send email notification to User on user's OPENCoastS activation
def notify_set_active(new_user, active):
    # Prepare email parameters
    pars = {
        'username': new_user.username,
        'default_email': settings.DEFAULT_FROM_EMAIL
    }

    # Send email to user
    if active:
        subject = str(_('Conta ativada'))
        notify_users([new_user.email], subject, pars,
                     'front/emails/user_activation.txt', 'front/emails/user_activation.html')
    else:
        subject = str(_('Conta desativada'))
        notify_users([new_user.email], subject, pars,
                     'front/emails/user_deactivation.txt', 'front/emails/user_deactivation.html')


# Send email notification to User on user's password recover
def notify_user_password_recover(user, password_recover_link):
    # Prepare email parameters
    pars = {
        'username': user.username,
        'link': password_recover_link
    }

    subject = str(_('Recuperar Palavra-passe'))
    notify_users([user.email], subject, pars, 'front/emails/password_recover.txt', 'front/emails/password_recover.html')


# Send email notification to Managers and User on deployment extend request
def notify_extend_deploy(record, current_site):
    # Prepare email parameters
    pars = {
        'user': None,
        'link': None,
        'deploy_id': record.deployment.id,
        'motive': record.motive,
        'to_date': date_format(record.to_date, format='SHORT_DATE_FORMAT'),
    }

    subject = str(_('Pedido de Extensão'))
    # Notify User
    notify_users([record.deployment.user.email], subject, pars, 'front/emails/extenddeploy.txt', 'front/emails/extenddeploy.html')

    # Notify Managers
    pars['user'] = record.deployment.user.email
    pars['link'] = '{0}/admin/front/extenddeployrequest/{1}/change/'.format(current_site.domain, record.id)
    notify_users(settings.MANAGERS, subject, pars, 'front/emails/extenddeploy.txt', 'front/emails/extenddeploy.html')


# Send email notification to User on deployment extend request Managers follow up
def notify_extend_deploy_response(record):
    # Prepare email parameters
    pars = {
        'deploy_id': record.deployment.id,
        'motive': record.motive,
        'to_date': date_format(record.to_date, format='SHORT_DATE_FORMAT'),
        'accepted': record.accepted,
        'reply': record.reply
    }

    subject = '{0} [{1}]'.format(_('Pedido de Extensão'), _('Resposta'))
    # Notify User
    notify_users([record.deployment.user.email], subject, pars, 'front/emails/extenddeploy_followup.txt',
                 'front/emails/extenddeploy_followup.html')


# Send email notification to Admin of error
def notify_error(user, error):
    # First print to logger, in case email fails
    logger.error('USER %s - ERR: %s' % (user.username, error))

    # Prepare email parameters
    pars = {
        'username': user.username,
        'error': error
    }

    subject = str(_('Erro'))
    notify_users(settings.ADMINS, subject, pars, 'front/emails/front_error.txt', 'front/emails/front_error.html')


# Get and format user ratings
def get_user_ratings(userprofile):
    rating_queryset = UserProfileRating.objects.filter(userprofile=userprofile).order_by('-creation_datetime')
    if rating_queryset.count() == 0:
        return None
    else:
        ratings = []
        for item in rating_queryset:
            ratings.append({'date': item.creation_datetime, 'rate': str(dict(RATE_TYPE)[item.value]), 'comment': item.text})
        return ratings


def isWGS84(x, y):
    # Test if CRS is WGS84
    try:
        pnt = OGRGeometry('POINT(' + str(float(x)) + ' ' + str(float(y)) + ')')
        wgs84_bounding_box = OGRGeometry('POLYGON((-180 -90, 180 -90, 180 90, -180 90, -180 -90))')
        if not wgs84_bounding_box.contains(pnt):
            return False
    except:
        return False

    return True


def file2bytes(tmp_file):
    # Convert file content to bytesarray and return bytes
    bytesdata = bytes()
    try:
        for chunk in tmp_file.chunks():
            bytesdata += chunk
    except Exception:
        logger.exception('Error converting text to bytes')

    return bytesdata
