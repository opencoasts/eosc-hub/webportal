{% extends "__common__/emails/base.txt" %}
{% load i18n %}

{% block content %}

{% trans "REGISTO PENDENTE! Assim que o seu registo for validado com sucesso receberá um email da ativação da conta." %}

{% trans "Afiliação" %}
{{ affiliation }}

{% trans "País da Afiliação" %}
{{ affiliation_country }}

{% trans "Nome" %}
{{ first_name }} {{ last_name }}

Email
{{ email }}

{% endblock content %}
