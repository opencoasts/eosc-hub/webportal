FROM python:3.6-buster

RUN set -eux \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        gettext \
        libgdal-dev \
        less nano \
        postgresql-client \
    && apt-get autoremove && apt-get clean \
        && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /_project

COPY requirements.txt .
RUN set -eux \
    && python -m pip install --upgrade pip \
    && python -m pip install --requirement requirements.txt

COPY . .

ARG deployments_path=/_deployments
ARG postgis_host=postgis
ARG postgis_port=5432
ARG postgis_user=oc_user
ARG postgis_password=password
ARG postgis_database=opencoasts
ARG wms_host=ncwms
ARG wms_port=8000
ARG wms_path=/ncWMS
ARG wms_did_template='deployment_{id}'

RUN set -eux; \
    mkdir -vp _logs $deployments_path; \
    django-admin compilemessages; \
    python -m ProjOpenCoastS.setup \
        -POSTGIS_HOST=${postgis_host} \
        -POSTGIS_PORT=${postgis_port} \
        -POSTGIS_USER=${postgis_user} \
        -POSTGIS_PASSWORD=${postgis_password} \
        -POSTGIS_DATABASE=${postgis_database} \
        -WMS_HOST=${wms_host} \
        -WMS_PORT=${wms_port} \
        -WMS_PATH=${wms_path} \
        -DEPLOYMENTS_PATH=$deployments_path; \
    python -m manage collectstatic --clear --link

EXPOSE 8000
ENTRYPOINT [ "gunicorn", \
    "--config=python:gunicorn_config", \
    "--bind=0.0.0.0:8000", \
    "ProjOpenCoastS.wsgi" ]
