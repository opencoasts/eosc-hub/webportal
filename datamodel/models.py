from datetime import datetime, date

from django.db.models import (
    Model as dbModel, ForeignKey, IntegerField, CharField, SlugField, TextField,
    BooleanField, DateTimeField, DateField, BinaryField, FilePathField,
)
from django.contrib.postgres.fields import JSONField
from django.utils.timezone import now

from django.conf import settings


# TODO: convert default=datetime.now to auto_now_add on creation fields

class Name(dbModel):
    """
    Abstract Table - Naming info
    """
    name = CharField(max_length=100)
    reference = SlugField(max_length=50)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Info(dbModel):
    """
    Abstract Table - Description info
    """
    description = TextField(blank=True)
    notes = TextField(blank=True)

    class Meta:
        abstract = True


class Model(Name, Info):
    """
    Admin Table - references of Forecast Model Types
    """
    pass


class ModelVersion(dbModel):
    """
    Admin Table - references of versions of a Forecast Model Type
    """
    model = ForeignKey(Model)
    version = CharField(max_length=50)
    # executable = CharField(max_length = 50)

    def __str__(self):
        return '{0.model}-{0.version}'.format(self)
        # return f'{self.model}, {self.version}'


class ModelVersionRunPeriod(dbModel):
    """
    Admin Table - references of run periods of a Forecast Model
    """
    # Constants
    RUN_PERIOD_48H = 48
    RUN_PERIOD_72H = 72
    RUN_PERIOD_CHOICES = (
        (RUN_PERIOD_48H, '48h'),
        (RUN_PERIOD_72H, '72h'),
    )

    model_version = ForeignKey(ModelVersion)
    run_period = IntegerField(choices=RUN_PERIOD_CHOICES, default=RUN_PERIOD_48H)

    def __str__(self):
        return '%s (%s)' % (self.model_version, self.get_run_period_display())
        # return f'{self.model_version} ({self.get_run_period_display()})'


class Parameter(Name, Info):
    """
    Admin Table - references of Parameter Types
    """
    pass

    def __str__(self):
        return self.name


class AbstractEntityParameter(Info):

    parameter = ForeignKey(Parameter)

    default_value = TextField(blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.parameter


class ModelParameter(AbstractEntityParameter):
    """
    Admin Table - connects Parameters to a Forecast Model Type with a default value
    """
    model = ForeignKey(Model)

    def __str__(self):
        return '{0.model}: {0.parameter}'.format(self)
        # return f'{self.model}, {self.parameter}'


class ModelVersionParameter(AbstractEntityParameter):
    """
    Admin Table - connects Parameters to a specific version of a Forecast Model Type with a default value
    """
    model_version = ForeignKey(ModelVersion)

    def __str__(self):
        return '{0.model_version}: {0.parameter}'.format(self)
        # return f'{self.model_version}, {self.parameter}'

# Forcing: ForcingSource x Product (IBI, Global, etc...)
# ForcingKind: Forcing x BoundaryKind
class ForcingSource(Name, Info):
    """
    Admin Table - references of Forcing Source Provider
    """
    # forcing_kind = ForeignKey(BoundaryKind)

    def __str__(self):
        return self.name


class Requirement(Name, Info):
    """
    Admin Table - references of Requirements Types
    """
    # Constants
    pass

    def __str__(self):
        return self.name


class AbstractEntityRequirements(Info):

    requirement = ForeignKey(Requirement)

    # Note: move to wizard?
    ruleset = JSONField(blank=True, null=True)

    class Meta:
        abstract = True


class ModelRequirement(AbstractEntityRequirements):
    """
    Admin Table - connects Requirements to a Forecast Model Type with a default ruleset
    """
    model = ForeignKey(Model)

    def __str__(self):
        return '{0.model}: {0.requirement}'.format(self)
        # return f'{self.model}, {self.requirement}'


class ModelVersionRequirement(AbstractEntityRequirements):
    """
    Admin Table - connects Requirements to a specific version of a Forecast Model Type with a default ruleset
    """
    model_version = ForeignKey(ModelVersion)

    def __str__(self):
        return '{0.model_version}: {0.requirement}'.format(self)
        # return f'{self.model_version}, {self.requirement}'


class Deployment(Name, Info):
    """
    Table - information regarding Deployments
    """

    creation_datetime = DateTimeField(default=datetime.now)

    active = BooleanField(default=False)
    deleted = BooleanField(default=False)

    priority = IntegerField(blank=True, default=0)

    begin_run_date = DateField(blank=True, null=True)
    end_run_date = DateField(blank=True, null=True)

    last_run_date = DateField(blank=True, null=True)

    begin_date = DateField(blank=True, null=True)
    end_date = DateField(blank=True, null=True)

    model_version_period = ForeignKey(ModelVersionRunPeriod)

    def __str__(self):
        return 'ID:{0.id} {0.name} [{0.model_version_period}]'.format(self)
        # return f'ID:{self.id} {self.name} [{self.model_version_period}]'


class FileType(Name, Info):
    """
    Admin Table - references of FileTypes
    """
    # Constants
    CATEGORY_GR3 = 'gr3'
    CATEGORY_LL = 'll'              # TODO: eliminate this ??
    CATEGORY_NETCDF = 'netcdf'
    CATEGORY_IN = 'in'
    CATEGORY_NML = 'nml'
    CATEGORY = (
        (CATEGORY_GR3, 'GR3'),
        (CATEGORY_LL, 'LL'),
        (CATEGORY_NETCDF, 'netCDF'),
        (CATEGORY_IN, 'IN'),
        (CATEGORY_NML, 'NML'),
    )

    format = CharField(max_length=10, choices=CATEGORY, default='gr3')

    # TODO: Associate with ModelVersion

    def __str__(self):
        return '%s (%s)' % ( self.name, self.get_format_display() )
        # return f'{self.name} ({self.format})'


# TODO: remove Name trait ?
class FileBlobTrait(Name):
    """
    Abstract Table - File data
    """
    file_type = ForeignKey(FileType)
    file = BinaryField() # rename to file_blob
    # file_blob = BinaryField()
    # category = ...           # TODO: add category? distinguish category file: exº hgrid, manning, bctides.. ??

    @property
    def file_size_mb(self):
        #return round(len(self.file) / 1024 / 1024, 1)
        return 0

    class Meta:
        abstract = True


class DeploymentFile(FileBlobTrait):
    """
    Table - Binary Data and respective FileTypes connected to Deployments
    """
    deployment = ForeignKey(Deployment, related_name='files')
    # original = ...        # TODO: flag that indicates if this file has been changed from the uploaded version ??
                            # usefull for hgrid file that has xyz changed by the wizard app

    def __str__(self):
        return '{0.deployment}: {0.file_type} ({0.file_size_mb}MB)'.format(self)
        # return f'{self.deployment}: {self.file_type} ({self.file_size_mb}MB)'


class DeploymentParameter(dbModel):
    """
    Table - connects Parameters to Deployments with a specific value
    """
    deployment = ForeignKey(Deployment)
    parameter = ForeignKey(Parameter)

    value = TextField(blank=True)
    notes = TextField(blank=True)

    def __str__(self):
        return '{0.deployment}: {0.parameter} = {0.value}'.format(self)
        # return f'{self.deployment}: {self.parameter} = {self.value}'


class DeploymentBoundary(Name):
    """
    Table - connects Boundaries to Deployments
    """
    deployment = ForeignKey(Deployment)

    # kind = ForeignKey(BoundaryKind)

    def __str__(self):
        return '{0.deployment}: {0.name}'.format(self)
        # return f'{self.deployment}: {self.name}'


# rename to DeploymentBoundaryForcing ?
class DeploymentForcing(dbModel):
    """
    Table - connects Forcings to Deployments
    """
    deployment_boundary = ForeignKey(DeploymentBoundary)
    # bind to Forcing instead of ForcingSource
    forcing_source = ForeignKey(ForcingSource)
    priority = IntegerField(blank=True, default=0)

    def __str__(self):
        return '{0.deployment_boundary} forced by {0.forcing_source} ' \
                                                    '#{0.priority}'.format(self)
        # return f'{self.deployment_boundary} forced by {self.forcing_source} ' \
        #                                                      f'#{self.priority}'

class DeploymentOutputFilepath(dbModel):

    deployment = ForeignKey(Deployment)
    date = DateField(default=date.today)
    filepath = FilePathField(
        path=settings.DEPLOYMENTS_PATH,
        match=settings.OUTPUT_FILES_MATCH,
        recursive=True,
        max_length=254,
    )

    def __str__(self):
        return '{0.deployment} on {0.date}: {0.filepath}'.format(self)
        #return f'{self.deployment} on {self.date}: {self.filepath}'


class DeploymentRun(dbModel):

    class State:

        PENDING = 'pending'
        STARTED = 'started'
        FINISHED = 'finished'
        FAILED = 'failed'

        _CHOICES = (
            (PENDING, 'Pending'),
            (STARTED, 'Started'),
            (FINISHED, 'Finished'),
            (FAILED, 'Failed'),
        )

    deployment = ForeignKey(Deployment)
    run_datetime = DateTimeField()
    state = CharField(choices=State._CHOICES, max_length=8)
    begin = DateTimeField(default=now, null=True)
    end = DateTimeField(null=True, blank=True)

    @property
    def duration(self):
        return self.begin and ( ( self.end or now() ) - self.begin )

    def __str__(self):
        return '{0.deployment} on {0.run_datetime}: {1}'.format( self,
                                                      self.get_state_display() )
        # return f'{self.deployment} on {self.run_datetime}: ' \
        #                                                 self.get_state_display()


class DeploymentRunMessage(dbModel):

    class Kind:

        ERROR = 'error'
        TIMINGS = 'timings'

        _CHOICES = (
            (ERROR, 'Error'),
            (TIMINGS, 'Timings'),
        )

    deployment_run = ForeignKey(DeploymentRun)
    kind = CharField(choices=Kind._CHOICES, max_length=8)
    message = TextField(blank=True)

    def __str__(self):
        return '{0.deployment_run}; {0.kind}: {0.message}'.format(self)
        # return f'{self.deployment_run}; {self.kind}: {self.message}'


class DeploymentRunOutputFilepath(dbModel):

    class Kind:

        LOGS = 'logs'
        CONFS = 'confs'
        INPUTS = 'inputs'
        RESULTS = 'results'

        _CHOICES = (
            (LOGS, 'Logs'),
            (CONFS, 'Configurations'),
            (INPUTS, 'Inputs'),
            (RESULTS, 'Results'),
        )

    deployment_run = ForeignKey(DeploymentRun)
    kind = CharField(choices=Kind._CHOICES, max_length=8)
    filepath = FilePathField(
        path = settings.DEPLOYMENTS_PATH,
        match = r'^{}$'.format(settings.OUTPUT_FILES_MATCH),
        recursive = True,
        max_length = 254,
    )

    def __str__(self):
        return '{0.deployment_run}; {0.kind}: {0.filepath}'.format(self)
        # return f'{self.deployment_run}; {self.kind}: {self.filepath}'


'''
class BoundaryKind(dbModel):
    name = CharField(max_length=100, blank=True)
    reference = SlugField(max_length=50, blank=True)
    description = TextField(blank=True)
    notes = TextField(blank=True)

    model_version = ManyToManyField(ModelVersion)

    def __str__(self):
        return self.name
'''
