from django.contrib.admin import ModelAdmin, register
from front.models import Deployment as front_deployment

from .models import *
from django.core.urlresolvers import reverse


@register(Model)
class ModelAdmin_(ModelAdmin):
    ordering = ('name',)
    prepopulated_fields = dict( reference = ('name',) )


@register(ModelVersion)
class ModelVersionAdmin(ModelAdmin):
    ordering = ('model', 'version')


@register(ModelVersionRunPeriod)
class ModelVersionRunPeriodAdmin(ModelAdmin):
    ordering = ('model_version', 'run_period')


@register(Parameter)
class ParameterAdmin(ModelAdmin):
    ordering = ('name',)
    prepopulated_fields = dict( reference = ('name',) )


@register(ModelParameter)
class ModelParameterAdmin(ModelAdmin):
    ordering = ('model', 'parameter')


@register(ModelVersionParameter)
class ModelVersionParameterAdmin(ModelAdmin):
    ordering = ('model_version', 'parameter')


@register(ForcingSource)
class ForcingSourceAdmin(ModelAdmin):
    ordering = ('name',)
    prepopulated_fields = dict( reference = ('name',) )


@register(Requirement)
class RequirementAdmin(ModelAdmin):
    ordering = ('name',)
    prepopulated_fields = dict( reference = ('name',) )


@register(ModelRequirement)
class ModelRequirementAdmin(ModelAdmin):
    ordering = ('model', 'requirement')


@register(ModelVersionRequirement)
class ModelVersionRequirementAdmin(ModelAdmin):
    ordering = ('model_version', 'requirement')


@register(FileType)
class FileTypeAdmin(ModelAdmin):
    ordering = ('name',)
    prepopulated_fields = dict( reference = ('name',) )


@register(Deployment)
class DeploymentAdmin(ModelAdmin):
    list_editable = (
        'active',
    )
    list_display = (
        'id',
        'name',
        'model_version_period',
        'creation_datetime',
        'deleted',
        'active',
        'begin_date',
        'end_date',
        'begin_run_date',
        'end_run_date',
        'last_run_date',
    )
    ordering = ('-active', '-last_run_date', 'id')
    prepopulated_fields = dict( reference = ('name',) )
    list_per_page = 10


@register(DeploymentFile)
class DeploymentFileAdmin(ModelAdmin):
    list_display = (
        'id',
        'deployment',
        'owner',
        'file_type',
        'file_size_mb',
        'file_link',
    )
    ordering = ('deployment', 'name',)
    prepopulated_fields = dict( reference = ('name',) )
    raw_id_fields = ('deployment', 'file_type')
    list_per_page = 10
    list_filter = ('deployment',)

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super(DeploymentFileAdmin, self).get_queryset(request)
        qs = qs.defer('file')
        return qs

    def owner(self, obj):
        # Get deploy owner's username
        deploy = front_deployment.objects.get(id=obj.deployment.id)
        return deploy.user.username

    def file_link(self, obj):
        # Return file as download link
        url = reverse('front:download_file', kwargs={'file_id': obj.id, 'file_cat': "deploy"})
        return u'<a href="%s">download</a>' % url

    file_link.allow_tags = True


@register(DeploymentParameter)
class DeploymentParameterAdmin(ModelAdmin):
    ordering = ('deployment', 'parameter')


@register(DeploymentBoundary)
class DeploymentBoundaryAdmin(ModelAdmin):
    ordering = ('deployment', 'name',)
    prepopulated_fields = dict( reference = ('name',) )


@register(DeploymentForcing)
class DeploymentForcingAdmin(ModelAdmin):
    ordering = ('deployment_boundary', 'priority')


@register(DeploymentOutputFilepath)
class DeploymentOutputFilepathAdmin(ModelAdmin):
    ordering = ('-date', 'deployment')
    list_display = ('id', 'deployment', 'date', 'filepath')
    list_filter = ('deployment', )
    search_fields = ('deployment__name', )
    date_hierarchy = 'date'


@register(DeploymentRun)
class DeploymentRunAdmin(ModelAdmin):
    ordering = ('-run_datetime', '-deployment')
    list_display = ('id', 'deployment', 'run_datetime', 'state', 'begin', 'end', 'duration')
    list_filter = ('deployment', 'state')
    search_fields = ('deployment__name',)
    date_hierarchy = 'run_datetime'


class BaseDeploymentRunEntityAdmin:
    ordering = ('-deployment_run__begin', '-deployment_run__run_datetime', 'kind')
    list_display_ = ('id', 'deployment_run', 'kind')
    list_filter = ('deployment_run__deployment', 'kind')
    search_fields = ('deployment_run__deployment__name',)
    date_hierarchy = 'deployment_run__run_datetime'


@register(DeploymentRunMessage)
class DeploymentRunMessageAdmin(BaseDeploymentRunEntityAdmin, ModelAdmin):
    list_display = BaseDeploymentRunEntityAdmin.list_display_ + ('message',)


@register(DeploymentRunOutputFilepath)
class DeploymentRunOutputFilepathAdmin(BaseDeploymentRunEntityAdmin,
                                                                    ModelAdmin):
    list_display = BaseDeploymentRunEntityAdmin.list_display_ + ('filepath',)
