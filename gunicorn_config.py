import multiprocessing
from pathlib import path

# general
bind = '0.0.0.0'

_min_workers = min(multiprocessing.cpu_count()*2, 16) # at most 16 workers
workers = max(_min_workers, 4) # at least 4 workers

# timeout
timeout = 120

# logging
log_dir = Path('_logs/gunicorn')
log_dir = log_dir.mkdir(parents=True, exist_ok=True)

accesslog = log_dir / 'gunicorn_access'
#errorlog = log_dir / 'gunicorn_error'
#capture_output = True

pidfile = log_dir / 'gunicorn_pidfile'
