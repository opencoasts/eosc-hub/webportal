/*
   L.TimeDimension.Layer.WMS.cstm: check if current time is not available in loaded service
   if not: diverge from default behavior that is to load nearest time, loads time (empty layer)
   and pushes to array the reference to that reference
*/

L.TimeDimension.Layer.WMS.CSTM = L.TimeDimension.Layer.WMS.extend({
    _getLayerForTime: function(time) {
        if (time == 0 || time == this._defaultTime || time == null) {
            return this._baseLayer;
        }
        if (this._layers.hasOwnProperty(time)) {
            return this._layers[time];
        }

        // Changed the following statements
        if (this._availableTimes.length > 0 && this._availableTimes.indexOf(time) == -1) {
            var nearestTime = time;
            // Adding
            if (layers_notime.indexOf(this.options.layer_id) == -1) layers_notime.push(this.options.layer_id);
        } else {
            var nearestTime = this._getNearestTime(time);
            if (this._layers.hasOwnProperty(nearestTime)) {
                return this._layers[nearestTime];
            }
        }

        var newLayer = this._createLayerForTime(nearestTime);

        this._layers[time] = newLayer;

        newLayer.on('load', (function(layer, time) {
            layer.setLoaded(true);
            // this time entry should exists inside _layers
            // but it might be deleted by cache management
            if (!this._layers[time]) {
                this._layers[time] = layer;
            }
            if (this._timeDimension && time == this._timeDimension.getCurrentTime() && !this._timeDimension.isLoading()) {
                this._showLayer(layer, time);
            }

            this.fire('timeload', {
                time: time
            });
        }).bind(this, newLayer, time));

        // Hack to hide the layer when added to the map.
        // It will be shown when timeload event is fired from the map (after all layers are loaded)
        newLayer.onAdd = (function(map) {
            Object.getPrototypeOf(this).onAdd.call(this, map);
            this.hide();
        }).bind(newLayer);

        return newLayer;
    },

    _parseTimeDimensionFromCapabilities: function(xml) {
        var layers = xml.querySelectorAll('Layer[queryable="1"]');
        var layerName = this._baseLayer.wmsParams.layers;
        var layer = null;
        var times = null;

        layers.forEach(function(current) {
            if (current.querySelector("Name").innerHTML === layerName) {
                layer = current;
            }
        })
        if (layer) {
            times = this._getTimesFromLayerCapabilities(layer);
            if (!times) {
                times = this._getTimesFromLayerCapabilities(layer.parentNode);
            }
        }

        // Changed the following statements
        // Write service period, if empty and times exist
        if (times) {
            var lbl = $('#period_' + this.options.layer_id.split('-')[0]);
            lbl.html('[' + times.replace(/T/g, ' ').replace(/:00.000Z/g, '').slice(0, -5) + ']');
        }
        return times;
    },
});

L.timeDimension.layer.wms.cstm = function(layer, options) {
    return new L.TimeDimension.Layer.WMS.CSTM(layer, options);
};
