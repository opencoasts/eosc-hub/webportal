import sys
import os
import django

sys.path.append('/home/opencoasts/opencoasts_pt-dev/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ProjOpenCoastS.local_settings'
django.setup()

from front.models import Deployment, ForcingSource


def getkey(str_value):
    fmodels = ((x[0], x[1]) for x in ForcingSource.objects.values_list('parent__reference', 'parent__name'))
    for item in fmodels:
        (key, value) = item
        if str_value == value:
            return key
    return None


def save_deployments():
    deploys = Deployment.objects.all()
    for dep in deploys:
        dep.save()
        print(dep.id, 'Saved..')


def update_deployments():
    deploys = Deployment.objects.all()
    for dep in deploys:
        oceanmodel = None
        step3 = dep.step3

        if step3 and step3 != 'null':
            if 'forcings' in step3:
                forcings = step3['forcings']

                for n, item in enumerate(forcings):
                    ftype = item['ftype']

                    if ftype == 'ocean' or ftype == 'Oceânica' or ftype == 'Oceanic':
                        ftype = 'ocean'
                        # Get oceanmodel
                        oceanmodel = getkey(item['fmodel'])

                    if ftype == 'river' or ftype == 'Fluvial' or ftype == 'River':
                        ftype = 'river'
                        try:
                            series_in = item['fmodel'].strip('[]').split(',')
                            forcings[n]['fmodel'] = [float(obj[4:]) for obj in series_in]
                        except Exception:
                            pass

                    forcings[n]['ftype'] = ftype

                    if 'ftype_code' in forcings[n]:
                        del forcings[n]['ftype_code']

            if 'atmmodel' in step3:
                atmmodel = getkey(step3['atmmodel'])

                if atmmodel:
                    step3['atmmodel'] = atmmodel

            # Build new json
            if oceanmodel:
                step3['oceanmodel'] = oceanmodel

            # When all is ok save it
            dep.step3 = step3
            dep.save()
            print(dep.id, 'Saved..')

            # print(dep.id, step3, '\n')


if __name__ == '__main__':
    update_deployments()
