import requests
import os.path
import xml.etree.ElementTree as ET

from datetime import datetime
from django.shortcuts import render
#from django.core.serializers import serialize
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.utils.translation import ugettext_lazy

from django.conf import settings

from operator import itemgetter

import raster.models
import satellite.models
from front.models import Deployment #, BBox
from datamodel.models import DeploymentOutputFilepath, DeploymentRun, DeploymentRunOutputFilepath

from wizard.views import STATION_TYPE
from wizard.forms import RUN_TYPE, RUN_TYPE_map
from wizard.utils import compute_extent

logger = settings.LOGGER.getChild('viewer')


app_name = 'viewer'


# View for index
@login_required(login_url='/login/')
def home(request):
    template, bbox, bbox_types, deploys, msg_str = 'viewer/base.html', [], [], None, None

    # TODO: rethink this..
    request_host, _, request_port = request.get_host().partition(':')
    wms_host = settings.WMS_HOST or request_host
    wms_port = settings.WMS_PORT or request_port
    wms_path = settings.WMS_PATH

    wms_url = '{schema}://{host}{port}{path}/wms'.format(
        schema = 'https' if settings.WMS_TLS else 'http',
        host = wms_host,
        port = ':{}'.format(wms_port) if wms_port else '',
        path = '/{}'.format(wms_path) if wms_path else '',
    )

    # Just passing a message
    if 'msg_str' in request.session:
        msg_str = request.session['msg_str']
        request.session.pop('msg_str')

    # Return Global Bounding Box only
    # Commented on 26/09/2018 - Global BBox no longer used
    #bbox_queryset = BBox.objects.filter(bbox_type=BBox.BOXTYPE_GLOBAL)
    #if bbox_queryset:
    #    bbox = serialize('geojson', bbox_queryset)
    #    bbox_types = [{'type': item.bbox_type, 'label': str(dict(BBox.BOXTYPE)[item.bbox_type]),
    #                   'color': item.color}
    #                  for item in bbox_queryset]

    # Get active and non deleted Deployments to fill table
    deploy_queryset = Deployment.objects.select_related().filter(deleted=False, active=True).order_by('-id')
    # Add non expired deployments filter
    today = timezone.now().date()
    deploy_queryset = deploy_queryset.filter(end_date__gte=today)

    # TODO: remove this check when users can see more than their deployments
    if not request.user.is_staff:
        # Filter User's Deployments
        deploy_queryset = deploy_queryset.filter(user=request.user)

    if deploy_queryset:
        deploys, dep_files_old, dep_files = [], [], []
        for item in deploy_queryset:
            # Get output files list
            files_queryset_old = DeploymentOutputFilepath.objects.filter(deployment__id=item.id).values()
            dep_files_old = [{'date': datetime.strftime(obj['date'], '%Y-%m-%d'),
                              'name': os.path.basename(obj['filepath']),
                              'filepath': os.path.join('files', os.path.relpath(obj['filepath'],
                                                                                settings.DEPLOYMENTS_PATH))} for
                             obj in
                             files_queryset_old]

            query_runs = DeploymentRun.objects.filter(deployment__id=item.id)
            files_queryset = DeploymentRunOutputFilepath.objects.filter(deployment_run__in=query_runs).values(
                'deployment_run__run_datetime', 'filepath')
            dep_files_new = [{'date': datetime.strftime(obj['deployment_run__run_datetime'], '%Y-%m-%d'),
                              'name': os.path.basename(obj['filepath']),
                              'filepath': os.path.join('files', os.path.relpath(obj['filepath'],
                                                                                settings.DEPLOYMENTS_PATH))} for
                             obj in
                             files_queryset]

            dep_files = dep_files_old + dep_files_new

            # If superuser show deploy creator username
            if not request.user.is_staff:
                creator = None
            else:
                creator = item.user

            # Get BBox of the deployment
            if 'extent' in item.step2 and item.step2['extent'] != [0, 0, 0, 0]:
                extent = item.step2['extent']
            else:
                extent_ok, extent, msg = compute_extent(item.step2['boundaries']['geojson'])
                if extent_ok:
                    item.step2['extent'] = extent
                    item.save()

            # Get selected stations only
            stations = []
            for station in item.step4["stations"]:
                if station['chk'] == 1:
                    stations.append(station)

            run_type = 'basic2d'
            if 'run_type' in item.step1:
                run_type = item.step1['run_type']

            # TODO: In the future get Outputs from Requirements or Model Tables
            # Set deploy's outputs (SCHISM)
            outputset = []
            if 'SCHISM' in str(item.model_version_period.model_version):
                outputset.append({
                    'id': "elev",
                    'theme': "elev",
                    'name': ugettext_lazy('Elevação'),
                    'style': "default-scalar/div-BuRd-inv",
                    'unit': "m",
                    'min': -3.0,
                    'max': 3.0,
                    'log': "false",
                })

                outputset.append({
                    'id': "uvgroup",
                    'theme': "u:v-group",
                    'name': ugettext_lazy('Velocidade'),
                    'style': "colored_arrows/x-Rainbow",
                    'unit': "ms-1",
                    'min': 0.001,
                    'max': 3.0,
                    'log': "true"
                })

                if 'waves' in run_type:
                    outputset.append( dict(
                        id =  'hs',
                        theme = 'wsh_u:wsh_v-group',
                        name = 'Significant Wave Height',
                        style = 'colored_arrows/x-Ncview',
                        unit = 'm',
                        min = 0.01,
                        max = 15.,
                        log = 'false',
                    ) )

                    outputset.append( dict(
                        id = 'tp',
                        theme = 'tp',
                        name = 'Peak Period',
                        style = 'default-scalar/psu-inferno-inv',
                        unit = 's',
                        min = 2.,
                        max = 25.,
                        log = 'false',
                    ) )

                levels = 0
                if '3d' in run_type:
                    # Get number of vertical levels from step2
                    if 'vgrid_info' in item.step2 and 's_levels' in item.step2['vgrid_info'] and \
                                    'z_levels' in item.step2['vgrid_info']:
                        nrlev = len(item.step2['vgrid_info']['s_levels']) + \
                                    len(item.step2['vgrid_info']['z_levels'])
                        levels = list(range(1, nrlev))[::-1]

                    outputset.append({
                        'id': "temp",
                        'theme': "temp",
                        'name': ugettext_lazy('Temperatura'),
                        'style': "default-scalar/div-PuOr-inv",
                        'unit': "º",
                        'min': -5.0,
                        'max': 25.0,
                        'log': "false"
                    })

                    outputset.append({
                        'id': "salt",
                        'theme': "salt",
                        'name': ugettext_lazy('Salinidade'),
                        'style': "default-scalar/seq-Greens",
                        'unit': "PSU",
                        'min': 0.0,
                        'max': 40.0,
                        'log': "false"
                    })

                    outputset.append({
                        'id': "zcor",
                        'theme': "zcor",
                        'name': ugettext_lazy('Ref. Vert.'),
                        'style': "contours",
                        'unit': "m",
                        'log': "false"
                    })

            deploys.append({'id': item.id, 'name': item.name, 'modelversion': item.model_version_period,
                            'files': dep_files, 'stations': stations, 'runperiod': item.model_version_period.run_period,
                            'creator': creator, 'extent': extent, 'run_type_str': str(RUN_TYPE_map[run_type]),
                            'run_type': run_type, 'outputs': outputset, 'levels': levels})

    # Public - Stations Legend
    station_types = [{'type': 'O', 'label': str(dict(STATION_TYPE)['O']), 'color': 'rgb(0,102,255)'},
                     {'type': 'C', 'label': str(dict(STATION_TYPE)['C']), 'color': 'rgb(0,0,0,0)'},
                     {'type': 'V', 'label': str(dict(STATION_TYPE)['V']), 'color': 'rgb(204,51,255)'}]

    rasters = raster.models.RasterLayer.objects.all()
    satellite_rasters = satellite.models.SatelliteRaster.objects.all() #filtering by deployment_id
    n_rasters = rasters.count()
    n_satellite_rasters = satellite_rasters.count()

    #print(rasters)

    rasters_loaded = []

    for i in range(0, n_rasters):
        #print(rasters[i].name)
        for j in range(0, n_satellite_rasters):
            #print(satellite_rasters[j].raster_name)
            if(satellite_rasters[j].raster_name == rasters[i].name):
                satellite_raster = {
                    "deploy_id": satellite_rasters[j].raster_deployment.id,
                    "properties": {
                        "id": rasters[i].id,
                        "name": rasters[i].name,
                        "date": satellite_rasters[j].raster_date.strftime('%d/%m/%Y'),
                    }
				}

                rasters_loaded.append(satellite_raster)

    #print(rasters_loaded)

    return render(request, template, {
        'msg_str': msg_str,
        'bbox': bbox,
        'bbox_types': bbox_types,
        'deploys': deploys,
        'rasters_loaded': rasters_loaded,
        'station_types': station_types,
        'wms_host': wms_url,
        'wms_did': settings.WMS_DID_TEMPLATE
    })


@login_required(login_url='/login/')
def get_station_data(request, station_id, param, startdate, enddate):
    # Get and parse observation data from EMODNet webservices for selected period
    data, parameter = [], ''

    try:
        # Convert SCHISM output codes to Emodnet codes
        param = param.upper()
        if param == 'ELEV':
            parameter = 'SLEV'
        elif param == 'SALT':
            parameter = 'PSAL'
        elif param == 'HS':
            parameter = 'SWH'

        parse_item = lambda item_str: item_str.split('=', 1)


        if parameter:
            # Build request url
            req_link = 'http://www.emodnet-physics.eu/Map/service/WSEmodnet2.aspx' \
                       '?q=GetAllLatestDataFromTo&PlatformID={}&StartDate={}&' \
                       'EndDate={}'.format(station_id, startdate.replace("-", "/"), enddate.replace("-", "/"))

            # Parse returned xml and build JSON data response
            xml_response = ET.fromstring(requests.get(req_link).content.decode('latin1'))
            for item in xml_response:
                date_value = int(datetime.strptime(item.find('Date').text, '%Y/%m/%d %H:%M:%S').timestamp() * 1000) # timestamp in seconds

                param_items = item.find('ParamValue').text.split(";")
                # Build dictionary of parameters
                params_dict = dict( map(parse_item, param_items) )

                # If parameter exists in dict
                param_value_str = params_dict.get(parameter)
                if param_value_str:

                    param_quality = int(params_dict[f'{parameter}_QC'])
                    if param_quality <= 3: # quality is nonexistent or good

                        param_value = float( param_value_str.replace(',', '.') )
                        data.append( (date_value, param_value) )

            if len(data) > 0:
                # Sort array of arrays by date (Highcharts can't handle unsorted data)
                data = sorted(data, key=itemgetter(0) )

    except Exception:
        logger.exception('Error getting station data')
        data = []

    return JsonResponse({'parameter': parameter, 'data': data})
