# Deploy with Docker

> **Warning**: default passwords SHOULD be changed in production systems!

## Setup
The following commands must be executed inside the folder containing the
`docker-compose.yml` file or specify its location by including the `-f` or
`--file` argument (use `-h` or `--help` flag to know how):

### Deploy

Build images:

    docker-compose build

Start services in the background (`-d`):

    docker-compose up -d

Check containers status and logs:

    docker-compose ps
    docker-compose logs

### Configure

Configuration commands are described in the [setup](SETUP.md#Configuration)
document.

> They must be prefixed by `docker-compose exec webportal` to be executed inside
> the container.

### Manage

Reload web portal (served by gunicorn) settings:

    docker-compose kill -s HUP webportal

Stop and remove services, including volumes (`-v`), but not the network:

    docker-compose rm --stop -v


## Dockerfiles

**Base** and **Web Portal** hold the web application code base.  
**PostGIS** and **pgAdmin4** add customizations by wrapping well established
Docker Hub images.

### Base (`Dockerfile`)
Based on the _python:3.6-buster_ image, this file defines an image providing the
software platform (dependencies) and the web application (at `/project`) without configurations.

### Web Portal (`Dockerfile-webportal`)
Based on _opencoasts/django:0_ image, built from the base file, provides
the missing configurations and an entry point to serve the web application on
port 8000. 
_gunicorn_ is the web server and since its execution is defined via
an `ENTRYPOINT` it is possible to pass it extra arguments (eg: `--workers=2`) in
`docker run` and `docker create`.  
The following arguments (with defaults) allow some build customization:
 - `postgis_host = postgis`
 - `postgis_port = 5432`
 - `postgis_user = oc_user`
 - `postgis_password = password`
 - `postgis_database = opencoasts`
 - `deployments_path = /__deployments__`

### PostGIS (`Dockerfile-postgis`)
Based on _postgis/postgis:13-3.0_ image, this file defines init scripts and
defaults.

### pgAdmin4 (`Dockerfile-pgadmin`)
Based on the _dpage/pgadmin4:4_ image, this file just defines the `postgis`
server to be included in pgAdmin's list of servers.


## Docker Compose (`docker-compose.yml`)

### Services

#### `webportal`:
    build:
      context: .
      dockerfile: Dockerfile-webportal
    command: --workers=2
    # environment:
    #   - GUNICORN_CMD_ARGS=--workers=2
    ports:
      - 8000:8000
    restart: always
    depends_on:
      - postgis

#### `postgis`:
    build:
      context: .
      dockerfile: Dockerfile-postgis
    # volumes:
    #   - data:/var/lib/postgresql/data
    restart: always

#### `pgadmin`:
    build:
      context: .
      dockerfile: Dockerfile-pgadmin
    environment:
      PGADMIN_DEFAULT_EMAIL: admin@local
      PGADMIN_DEFAULT_PASSWORD: password
    ports:
      - 10080:80
    # volumes:
    #   - data:/var/lib/pgadmin
    restart: always
    depends_on:
      - postgis

### Networks
Currently no custom configurations.

### Volumes
Currently no custom configurations.